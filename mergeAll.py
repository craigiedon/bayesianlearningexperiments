import mergeResults as mrg
import sys
import accumulateRewards as ac
from os.path import join
import os

if len(sys.argv) != 2:
    print("Usage: python mergeResults.py <results_folder>")
    sys.exit(1)

results_folder = sys.argv[1]

# Accumulate rewards in each file
print("Accumulating results")
ac.accumulate_results(results_folder, 0.99)


# Merge Together All Results
merged_folder = join(results_folder, os.pardir, "merged")

print("Merging experiment repetitions")
for metric in ["discountedCumReward", "Rewards", "ActionsSize", "ChanceVarsSize", "TimeSecs"]:
    mrg.merge_results(results_folder, metric)

print("Adding Action and Chance Size Results")
mrg.add_action_and_chance(merged_folder)
