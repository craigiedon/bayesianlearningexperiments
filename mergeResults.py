import chartResults as ch
import os
from os.path import join
import sys
import re


def add_action_and_chance(folder_path):
    task_pattern = r'(.*)-ActionsSize\.csv'
    task_matches = [re.match(task_pattern, f) for f in os.listdir(folder_path)]
    task_names = [t.group(1) for t in task_matches if t is not None]
    for task_name in task_names:
        action_path = join(folder_path, '{0}-ActionsSize.csv'.format(task_name))
        chance_path = join(folder_path, '{0}-ChanceVarsSize.csv'.format(task_name))
        save_path = join(folder_path, '{0}-VocabSize.csv'.format(task_name))

        ch.merge_and_save([action_path, chance_path], save_path, ch.add_results)


def merge_results(results_folder, metric_name):

    merge_folder = join(results_folder, os.pardir, "merged")
    if not os.path.exists(merge_folder):
        os.mkdir(merge_folder)

    for experiment_folder in os.listdir(results_folder):
        abs_exp_folder = join(results_folder, experiment_folder)
        experiment_files = os.listdir(abs_exp_folder)

        file_pattern = r'.*-{0}.txt'.format(metric_name)
        metric_files = [join(abs_exp_folder, f) for f in experiment_files if re.match(file_pattern, f)]

        experiment_name = os.path.basename(experiment_folder)
        print(experiment_name)
        if metric_files != []:
            ch.merge_and_save(metric_files, os.path.join(merge_folder, "{0}-{1}.csv".format(experiment_name, metric_name)), ch.combine_results)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python mergeResults.py <results_folder> <metric-name>")
        sys.exit(1)

    results_folder = sys.argv[1]
    metric_name = sys.argv[2]

    merge_results(results_folder, metric_name)
