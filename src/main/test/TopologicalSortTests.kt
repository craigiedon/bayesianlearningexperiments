import org.junit.Assert
import org.junit.Test
import java.lang.IllegalStateException

class TopologicalSortTests{
    @Test
    fun topologicalSort_emptyList_returnEmpty(){
        val result = topologicalSort(emptyList<BNode>())
        Assert.assertEquals(emptyList<BNode>(), result)
    }

    @Test
    fun topologicalSort_singleItem_returnSingleItem(){
        val node = BNode(B, emptyList(), emptyList())
        val result = topologicalSort(listOf(node))
        Assert.assertEquals(listOf(node), result)
    }

    @Test
    fun topologicalSort_singleItemActionParents_returnSingleItem(){
        val actions = listOf(RandomVariable("A1", 2, VarType.ACTION))
        val node = BNode(B, actions, emptyList())
        val result = topologicalSort(listOf(node))
        Assert.assertEquals(listOf(node), result)
    }

    @Test
    fun topologicalSort_multipleItemsNoParents_returnItemsAnyOrder(){
        val nodeA = BNode(A, emptyList(), emptyList())
        val nodeB = BNode(B, emptyList(), emptyList())
        val result = topologicalSort(listOf(nodeA, nodeB))
        Assert.assertEquals(setOf(nodeA, nodeB), result.toSet())
    }

    @Test(expected = IllegalStateException::class)
    fun topologicalsort_cyclicDependency_throwException(){
        val nodeA = BNode(A, listOf(B), emptyList())
        val nodeB = BNode(B, listOf(A), emptyList())
        val result = topologicalSort(listOf(nodeA, nodeB))
        //Assert.assertEquals(setOf(nodeA, nodeB), result.toSet())
    }

    @Test
    fun topologicalSort_multipleVarsExplicitOrder_returnOrdered(){
        val nodeA = BNode(A, emptyList(), emptyList())
        val nodeB = BNode(B, listOf(A), emptyList())
        val nodeC = BNode(C, listOf(B), emptyList())
        val result = topologicalSort(listOf(nodeC, nodeA, nodeB))
        Assert.assertEquals(listOf(nodeA, nodeB, nodeC), result)
    }

    @Test
    fun topologicalSort_multipleVarsExplicitOrderRedundantVars_returnOnlyRelevant(){
        val nodeA = BNode(A, listOf(RandomVariable("A1", 2, VarType.ACTION)), emptyList())
        val nodeB = BNode(B, listOf(A, D), emptyList())
        val nodeC = BNode(C, listOf(B, E), emptyList())
        val result = topologicalSort(listOf(nodeC, nodeA, nodeB))
        Assert.assertEquals(listOf(nodeA, nodeB, nodeC), result)
    }
}