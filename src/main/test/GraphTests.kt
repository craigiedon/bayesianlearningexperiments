import org.junit.Assert
import org.junit.Test
import java.util.*

class GraphTests{
    val A = RandomVariable("A", 2)
    val B = RandomVariable("B", 2)
    val C = RandomVariable("C", 2)
    val D = RandomVariable("D", 2)

    @Test
    fun shortestPath_singleNodeEmptyScope_returnsNull(){
        val result = shortestPathContainingScope(listOf(A,B,C), CliqueNode(emptySet(), ArrayList<CliqueNode>()))
        Assert.assertNull(result)
    }

    @Test
    fun shortestPath_singleNodeFullScope_returnsSingleNodePath(){
        val c1 = CliqueNode(setOf(A,B,C,D), ArrayList())
        val result = shortestPathContainingScope(listOf(A,B,C), c1)
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertEquals(c1, result[0])
    }

    @Test
    fun shortestPath_onePathTwoStops_returnsListSizeTwo(){
        val c1 = CliqueNode(setOf(A,B), ArrayList())
        val c2 = CliqueNode(setOf(B,C), ArrayList())
        c1.neighbors.add(c2)
        c2.neighbors.add(c1)

        val result = shortestPathContainingScope(listOf(A,B,C), c1)
        Assert.assertNotNull(result)
        Assert.assertEquals(2, result!!.size)
        Assert.assertEquals(c1, result[0])
        Assert.assertEquals(c2, result[1])
    }

    @Test
    fun shortestPath_multiPathOneCorrect_returnsCorrectPath(){
        val c1 = CliqueNode(setOf(A,B), ArrayList())
        val c2 = CliqueNode(setOf(B), ArrayList())
        val c3 = CliqueNode(setOf(C,D), ArrayList())
        c1.neighbors.addAll(listOf(c2,c3))
        c2.neighbors.add(c1)
        c3.neighbors.add(c1)

        val result = shortestPathContainingScope(listOf(A,B,C), c1)
        Assert.assertNotNull(result)
        Assert.assertEquals(2, result!!.size)
        Assert.assertEquals(c1, result[0])
        Assert.assertEquals(c3, result[1])
    }

    @Test
    fun shortestPath_extraLongerPath_returnsShorterPath(){
        val c1 = CliqueNode(setOf(A,B), ArrayList())
        val c2 = CliqueNode(setOf(C), ArrayList())
        val c3 = CliqueNode(setOf(C,D), ArrayList())
        c1.neighbors.addAll(listOf(c2))
        c2.neighbors.addAll(listOf(c1, c3))
        c3.neighbors.add(c2)

        val result = shortestPathContainingScope(listOf(A,B,C), c1)
        Assert.assertNotNull(result)
        Assert.assertEquals(2, result!!.size)
        Assert.assertEquals(c1, result[0])
        Assert.assertEquals(c2, result[1])
    }

    @Test
    fun shortestPath_twoPaths_returnsShorterPath(){
        val c1 = CliqueNode(setOf(A), ArrayList())
        val c2 = CliqueNode(setOf(B, C), ArrayList())
        val c3 = CliqueNode(setOf(B), ArrayList())
        val c4 = CliqueNode(setOf(B,C), ArrayList())
        c1.neighbors.addAll(listOf(c2, c3))
        c2.neighbors.addAll(listOf(c1))
        c3.neighbors.addAll(listOf(c1, c4))

        val result = shortestPathContainingScope(listOf(A,B,C), c1)
        Assert.assertNotNull(result)
        Assert.assertEquals(2, result!!.size)
        Assert.assertEquals(c1, result[0])
        Assert.assertEquals(c2, result[1])
    }
}