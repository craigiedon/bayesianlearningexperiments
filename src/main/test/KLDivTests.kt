import Utils.randomParams
import org.junit.Assert
import org.junit.Test

class KLDivTests{
    @Test
    fun empiricalAndCachedIdentical_fullObservedVocab(){
        val trueBN = loadJsonBN("graphs/asia.json")
        val otherBN = randomParams(initialStructure(trueBN.vocab), 2)

        val testData = generateSamples(trueBN, 100)
        val cachedLnProbs = testData.associate { Pair(it, Math.log(variableEliminationQuery(it, trueBN))) }


        val resultCached = KLDivCached(trueBN.observableVars, cachedLnProbs, otherBN, testData)
        val resultEmpirical = KLDivEmpirical(trueBN.observableVars, trueBN, otherBN, testData)

        Assert.assertEquals(resultEmpirical, resultCached, 1E-10)
    }

    @Test
    fun empiricalAndCachedIdentical_fullVocabHiddenVars(){
        val loadedBN = loadJsonBN("graphs/asia.json")
        val trueBN = hideNodes(loadedBN, chooseHiddenVarsProportion(loadedBN, 0.25))
        val otherBN = randomParams(initialStructure(trueBN.vocab), 2)

        val testData = hideVars(generateSamples(trueBN, 100), trueBN.hiddenVars)
        val cachedLnProbs = testData.associate { Pair(it, Math.log(variableEliminationQuery(it, trueBN))) }


        val resultCached = KLDivCached(trueBN.observableVars, cachedLnProbs, otherBN, testData)
        val resultEmpirical = KLDivEmpirical(trueBN.observableVars, trueBN, otherBN, testData)

        Assert.assertEquals(resultEmpirical, resultCached, 1E-10)
    }

    @Test
    fun empiricalAndCachedIdentical_partialVocabHiddenVars(){
        val loadedBN = loadJsonBN("graphs/asia.json")
        val trueBN = hideNodes(loadedBN, chooseHiddenVarsProportion(loadedBN, 0.25))
        val otherBN = randomParams(initialStructure(trueBN.vocab.take(3)), 2)

        val testData = hideVars(generateSamples(trueBN, 100), trueBN.hiddenVars)
        val cachedLnProbs = testData.associate { Pair(it, Math.log(variableEliminationQuery(it, trueBN))) }

        val resultCached = KLDivCached(trueBN.observableVars, cachedLnProbs, otherBN, testData)
        val resultEmpirical = KLDivEmpirical(trueBN.observableVars, trueBN, otherBN, testData)

        Assert.assertEquals(resultEmpirical, resultCached, 1E-10)
    }
}