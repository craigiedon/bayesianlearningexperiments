import Utils.randomParams
import Utils.shuffle
import java.util.*

fun main(args : Array<String>){
    // Get a true BN (Asia network with ``either'' hidden?
    val trueBN = loadJsonBN("graphGeneration/asia.json")
    val observableVars = trueBN.vocab.filter { it.name != "either" }

    // generate a bunch of samples, hiding one variable
    val data = generateSamples(trueBN, 500)
    val observableData = hideVars(data, trueBN.vocab.filter { it.name == "either" })

    val startingBN = randomParams(rawStructure(trueBN), 2)
    val numIterations = 25

    var currentStepwiseBN = startingBN
    var currentBatchBN = startingBN

    val stepwiseKLs = ArrayList<Double>()
    val batchKLs = ArrayList<Double>()

    var stepwiseSufficientStats = EStep(currentStepwiseBN, listOf(data[0]))
    for(iteration in 0..numIterations){
        val shuffledData = shuffle(observableData)
        for(i in shuffledData.indices){
            println("Iteration $iteration, step $i")
            // Run stepwise EM
            stepwiseSufficientStats = stepWiseE(currentStepwiseBN, stepwiseSufficientStats, listOf(shuffledData[i]), 0.4, iteration * observableData.size + i, 0.7)
            currentStepwiseBN = MStep(rawStructure(trueBN), stepwiseSufficientStats, 1.0)
            stepwiseKLs.add(KLDivExact(observableVars, trueBN, currentStepwiseBN))
        }
        // Run batch EM
        currentBatchBN = batchEM(currentBatchBN, observableData, 1)
        batchKLs.add(KLDivExact(observableVars, trueBN, currentBatchBN))
    }

    println(stepwiseKLs)
    println(batchKLs)
    // Save to JSON File and plot
    saveToJson(listOf(stepwiseKLs, batchKLs), "stepwiseVsBatchExperiment", "result")
}