fun <B> timeFunc(func : () -> B) : Pair<B, Double>{
    val startTime = System.currentTimeMillis()
    val result = func()
    return Pair(result, (System.currentTimeMillis() - startTime) / 1000.0)
}
