import DNUnawareness.*
import com.google.ortools.constraintsolver.Decision
import org.junit.Assert
import org.junit.Test

class ExpertTests{
    @Test
    fun subOptimalBehaviour_noActions_notSuboptimal(){
        val optimalActionEUs = emptyList<Double>()
        val agentActionEUs = emptyList<Double>()
        val thresh = 0.3
        val result = subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, -1, -1)
        Assert.assertFalse(result)
    }

    @Test(expected=IllegalArgumentException::class)
    fun subOptimalBehaviour_optimalAsAndAgentActionsDifferentSizes_throwException(){
        val optimalActionEUs = listOf(0.5, 0.6)
        val agentActionEUs = listOf(0.5)
        val thresh = 0.3
        subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, -1, 1)
    }

    @Test(expected=IllegalArgumentException::class)
    fun subOptimalBehaviour_timeSinceLastAdviceGreaterThanActions_throwException(){
        val optimalActionEUs = listOf(0.5,0.6)
        val agentActionEUs = listOf(0.5,0.6)
        val thresh = 0.3
        val result = subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, -1, 20)
        Assert.assertFalse(result)
    }

    @Test
    fun subOptimalBehaviour_actingPoorlyOverThreshold_returnTrue(){
        val optimalActionEUs = listOf(5.0,10.0)
        val agentActionEUs = listOf(0.0,0.0)
        val thresh = 0.3
        val result = subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, -1, 1)
        Assert.assertTrue(result)
    }

    @Test
    fun subOptimalBehaviour_actingUnderThreshold_returnFalse(){
        val optimalActionEUs = listOf(5.0,10.0)
        val agentActionEUs = listOf(0.0,0.0)
        val thresh = 100.0
        val result = subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, -1, 1)
        Assert.assertFalse(result)
    }

    @Test
    fun subOptimalBehaviour_poorlyOverallButNotInInterval_returnFalse(){
        val optimalActionEUs = listOf(1000.0, 5.0, 10.0)
        val agentActionEUs = listOf(0.0, 0.0,0.0)
        val thresh = 100.0
        val result = subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, 0, 2)
        Assert.assertFalse(result)
    }

    @Test
    fun subOptimalBehaviour_goodOverallButNotInInterval_returnTrue(){
        val optimalActionEUs = listOf(0.0, 0.0, 110.0)
        val agentActionEUs = listOf(0.0, 0.0,0.0)
        val thresh = 100.0
        val result = subOptimalBehaviour(optimalActionEUs, agentActionEUs, thresh, 1, 2)
        Assert.assertTrue(result)
    }

    @Test(expected = IllegalArgumentException::class)
    fun betterActionAdvice_bestActionTaken_throwException(){
        val actions = listOf("A1", "A2").map { RandomVariable(it, 2, VarType.ACTION) }
        val o1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val chanceNodes = listOf(BNode(o1, actions,
            listOf(
                0.0,1.0,
                1.0,0.0,
                1.0,0.0,
                1.0,0.0
            ))
        )

        val rewardFunc = RewardTable(listOf(o1), listOf(0.0, 10.0))

        val dn = DecisionNetwork(rewardFunc, chanceNodes, actions)
        val expert = Expert(dn, 1, 0.0)

        val trial = Trial(mapOf(actions[0] to 0, actions[1] to 0, o1 to 1), 10.0)
        val result = betterActionAdvice(expert, trial, 0)
    }

    @Test
    fun expertWantsToInterject_currentActionOptimal_returnFalse(){
        val A1 = RandomVariable("A1", 2, VarType.ACTION)
        val dn = DecisionNetwork(RewardTable(listOf(A), listOf(0.5,0.5)),
            listOf(BNode(A, emptyList(), listOf(0.5,0.5))),
            listOf(A1))

        val newTrial = Trial(mapOf(A to 0, A1 to 0), 10.0)
        val optActEUs = listOf(10.0,10.0,10.0)
        val agentActEUs = listOf(0.0,0.0, newTrial.reward)

        val expert = Expert(dn, 1, 0.0)
        val result = expertWantsToInterject(expert, optActEUs, agentActEUs, 2, newTrial, dn)
        Assert.assertFalse(result)
    }

    @Test
    fun betterActionAdvice_bestActionReqsNoNewVocab_returnBestAction(){
        val actions = listOf("A1", "A2").map { RandomVariable(it, 2, VarType.ACTION) }
        val o1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val chanceNodes = listOf(BNode(o1, actions,
            listOf(
                0.0,1.0, // ¬a1, ¬a2
                1.0,0.0, // a1, ¬a2
                1.0,0.0, // ¬a1, a2
                1.0,0.0 // a1, a2
            ))
        )

        val rewardFunc = RewardTable(listOf(o1), listOf(0.0, 10.0))

        val dn = DecisionNetwork(rewardFunc, chanceNodes, actions)
        val expert = Expert(dn, 1, 0.0)
        expert.knownAgentVocab += actions[0]

        val trial = Trial(mapOf(actions[0] to 1, actions[1] to 0, o1 to 0), 0.0)
        val result = betterActionAdvice(expert, trial, 0)
        Assert.assertEquals(mapOf(actions[0] to 0), result)
    }

    @Test
    fun betterActionAdvice_bestActionReqsOneNewVocab_returnBestAction(){
        val actions = listOf("A1", "A2").map { RandomVariable(it, 2, VarType.ACTION) }
        val o1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val chanceNodes = listOf(BNode(o1, actions,
            listOf(
                1.0,0.0, // ¬a1, ¬a2
                0.0,1.0, // a1, ¬a2
                1.0,0.0, // ¬a1, a2
                1.0,0.0 // a1, a2
            ))
        )

        val rewardFunc = RewardTable(listOf(o1), listOf(0.0, 10.0))

        val dn = DecisionNetwork(rewardFunc, chanceNodes, actions)
        val expert = Expert(dn, 1, 0.0)

        val trial = Trial(mapOf(actions[0] to 0, actions[1] to 0, o1 to 0), 0.0)
        val result = betterActionAdvice(expert, trial, 0)
        Assert.assertEquals(mapOf(actions[0] to 1), result)
    }

    @Test
    fun betterActionAdvice_bestActionReqsMultipleButBetterActionWithZero_returnJustBetter(){
        val actions = listOf("A1", "A2").map { RandomVariable(it, 2, VarType.ACTION) }
        val o1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val chanceNodes = listOf(BNode(o1, actions,
            listOf(
                1.0,0.0, // ¬a1, ¬a2
                0.5,0.5, // a1, ¬a2
                1.0,0.0, // ¬a1, a2
                0.0,1.0 // a1, a2
            ))
        )

        val rewardFunc = RewardTable(listOf(o1), listOf(0.0, 10.0))

        val dn = DecisionNetwork(rewardFunc, chanceNodes, actions)
        val expert = Expert(dn, 1, 0.0)
        expert.knownAgentVocab += actions[0]

        val trial = Trial(mapOf(actions[0] to 0, actions[1] to 0, o1 to 0), 0.0)
        val result = betterActionAdvice(expert, trial, 0)
        Assert.assertEquals(mapOf(actions[0] to 1), result)
    }

    @Test
    fun betterActionAdvice_onlyBetterActionReqsAllVocab_returnBestAction(){
        val actions = listOf("A1", "A2").map { RandomVariable(it, 2, VarType.ACTION) }
        val o1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val chanceNodes = listOf(BNode(o1, actions,
            listOf(
                1.0,0.0, // ¬a1, ¬a2
                1.0,0.0, // a1, ¬a2
                1.0,0.0, // ¬a1, a2
                0.0,1.0 // a1, a2
            ))
        )

        val rewardFunc = RewardTable(listOf(o1), listOf(0.0, 10.0))

        val dn = DecisionNetwork(rewardFunc, chanceNodes, actions)
        val expert = Expert(dn, 1, 0.0)

        val trial = Trial(mapOf(actions[0] to 0, actions[1] to 0, o1 to 0), 0.0)
        val result = betterActionAdvice(expert, trial, 0)
        Assert.assertEquals(mapOf(actions[0] to 1, actions[1] to 1), result)
    }

    @Test(expected = IllegalArgumentException::class)
    fun resolveMisunderstanding_sameTime_throwException(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A), listOf(0.0, 0.1)),
            listOf(BNode(A, emptyList(), listOf(0.5,0.5))),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        expert.trialHistory.addAll(listOf(
            Pair(Trial(mapOf(A to 0), 10.0), 0),
            Pair(Trial(mapOf(A to 1), 5.0), 1),
            Pair(Trial(mapOf(A to 0), 10.0), 2))
        )
        val result = resolveMisunderstanding(expert, 2, 2, 5)
    }

    @Test(expected=IllegalArgumentException::class)
    fun resolveMisunderstanding_sameTrialDiffTimes_throwException(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A), listOf(0.0, 0.1)),
            listOf(BNode(A, emptyList(), listOf(0.5,0.5))),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        expert.trialHistory.addAll(listOf(
            Pair(Trial(mapOf(A to 0), 10.0), 0),
            Pair(Trial(mapOf(A to 1), 5.0), 1),
            Pair(Trial(mapOf(A to 0), 10.0), 2))
        )
        val result = resolveMisunderstanding(expert, 0, 2, 5)
    }

    @Test
    fun resolveMisunderstanding_singleDifference_returnDifference(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A), listOf(0.0, 0.1)),
            listOf(BNode(A, emptyList(), listOf(0.5,0.5))),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        expert.trialHistory.addAll(listOf(
            Pair(Trial(mapOf(A to 0), 10.0), 0),
            Pair(Trial(mapOf(A to 1), 5.0), 1),
            Pair(Trial(mapOf(A to 0), 10.0), 2))
        )
        val result = resolveMisunderstanding(expert, 0, 1, 5)
        Assert.assertEquals(ResolveMisunderstanding(A, 0, 1), result)
    }

    @Test
    fun resolveMisunderstanding_multipleDifferences_returnOneOfDifferences(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A, B), listOf(0.0, 0.1, 0.5, 1.0)),
            listOf(
                BNode(A, emptyList(), listOf(0.5,0.5)),
                BNode(B, emptyList(), listOf(0.5,0.5))
            ),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        expert.trialHistory.addAll(listOf(
            Pair(Trial(mapOf(A to 0, B to 0), 10.0), 0),
            Pair(Trial(mapOf(A to 1, B to 1), 5.0), 1),
            Pair(Trial(mapOf(A to 0, B to 1), 10.0), 2))
        )
        val result = resolveMisunderstanding(expert, 0, 1, 5)

        Assert.assertTrue(result.beliefVariable == A || result.beliefVariable == B)
    }

    @Test(expected = IllegalArgumentException::class)
    fun askForRDomVar_allRDomVarsAlreadyRevealed_throwException(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A, B), listOf(0.0, 0.1, 0.5, 1.0)),
            listOf(
                BNode(A, emptyList(), listOf(0.5,0.5)),
                BNode(B, emptyList(), listOf(0.5,0.5))
            ),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        val result = askForRDomVar(expert, setOf(A, B), 0)
    }

    @Test
    fun askForRDomVar_oneRemaining_revealRemaining(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A, B), listOf(0.0, 0.1, 0.5, 1.0)),
            listOf(
                BNode(A, emptyList(), listOf(0.5,0.5)),
                BNode(B, emptyList(), listOf(0.5,0.5))
            ),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        val result = askForRDomVar(expert, setOf(A), 0)
        Assert.assertEquals(B, result)
    }

    @Test(expected=IllegalArgumentException::class)
    fun whatDoesVarDo_varHasNoChildren_throwException(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A, B), listOf(0.0, 0.1, 0.5, 1.0)),
            listOf(
                BNode(A, emptyList(), listOf(0.5,0.5)),
                BNode(B, emptyList(), listOf(0.5,0.5))
            ),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        val result = whatDoesVarDo(expert, A, 0)
    }

    @Test
    fun whatDoesVarDo_varHasChildInExistingVocabAndUnknownVocab_revealExisting(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A, B), listOf(0.0, 0.1, 0.5, 1.0)),
            listOf(
                BNode(A, emptyList(), listOf(0.5,0.5)),
                BNode(B, listOf(A), listOf(0.5,0.5, 0.5, 0.5)),
                BNode(C, listOf(A), listOf(0.5,0.5,0.5,0.5))
            ),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        expert.knownAgentVocab.addAll(listOf(A,B))
        val result = whatDoesVarDo(expert, A, 0)
        Assert.assertEquals(Pair(A, B), result)
    }

    @Test
    fun whatDoesVarDo_varHasNewVocabChild_revealNewVocab(){
        val dn = DecisionNetwork(
            RewardTable(listOf(A, B), listOf(0.0, 0.1, 0.5, 1.0)),
            listOf(
                BNode(A, emptyList(), listOf(0.5,0.5)),
                BNode(B, listOf(), listOf(0.5,0.5)),
                BNode(C, listOf(A), listOf(0.5,0.5,0.5,0.5))
            ),
            emptyList()
        )

        val expert = Expert(dn, 1, 0.0)
        expert.knownAgentVocab.addAll(listOf(A,B))
        val result = whatDoesVarDo(expert, A, 0)
        Assert.assertEquals(Pair(A, C), result)
    }
}