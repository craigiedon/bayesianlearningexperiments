import Utils.random
import Utils.shuffle
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class ExactInferenceTests {
    val A = RandomVariable("A", 2)
    val B = RandomVariable("B", 2)
    val C = RandomVariable("C", 2)
    val D = RandomVariable("D", 2)
    val E = RandomVariable("E", 2)

    val c1 = CliqueNode(setOf(A,B), ArrayList())
    val f1 = Factor(listOf(A,B), listOf(0.2, 0.2, 0.1, 0.5))

    val c2 = CliqueNode(setOf(C,B), ArrayList())
    val f2 = Factor(listOf(C,B), listOf(0.3, 0.1, 0.2, 0.4))

    val c3 = CliqueNode(setOf(D), ArrayList())
    val f3 = Factor(listOf(D), listOf(0.5,0.5))
    lateinit var basicForest : CalibratedCliqueForest

    @Before
    fun setup(){

        c1.neighbors.clear()
        c2.neighbors.clear()

        c1.neighbors.add(c2)
        c2.neighbors.addAll(listOf(c1))

        val cliqueBeliefs1 = mapOf(c1 to f1, c2 to f2)
        val f1_2 = Factor(listOf(B), listOf(0.4, 0.6))
        val sepsetBeliefs1 = mapOf(setOf(c1, c2) to f1_2)
        val tree1 = CalibratedCliqueTree(cliqueBeliefs1, sepsetBeliefs1)

        val tree2 = CalibratedCliqueTree(mapOf(c3 to f3), emptyMap())

        basicForest = CalibratedCliqueForest(listOf(tree1, tree2))
    }

    @Test
    fun varElim_allVars_IdenticalToSimpleQuery(){
        val bn = loadJsonBN("graphs/asia.json")
        // Warning: Not a great unit test: Randomness involved
        val queryVars = bn.vocab.toList()
        val VEDist = variableEliminationQuery(queryVars, emptyMap(), bn)

        for(assignment in allAssignments(queryVars)){
            val simpleResult = simpleLnQuery(assignment, bn)
            val VEResult = Math.log(VEDist.values[assignmentToIndex(assignment, VEDist.scope)])
            Assert.assertEquals(simpleResult, VEResult, 1E-5)
        }
    }

    @Test
    fun varElim_missingVars_IdenticalToSimpleQuery(){
        val bn = loadJsonBN("graphs/asia.json")
        // Warning: Not a great unit test: Randomness involved
        val queryVars = shuffle(bn.vocab.toList()).take(3)
        val VEDist = variableEliminationQuery(queryVars, emptyMap(), bn)

        for(assignment in allAssignments(queryVars)){
            val simpleResult = simpleLnQuery(assignment, bn)
            val VEResult = Math.log(VEDist.values[assignmentToIndex(assignment, VEDist.scope)])
            Assert.assertEquals(simpleResult, VEResult, 1E-5)
        }
    }

    @Test
    fun factorFilterQuery_fullAssignment_IdenticalToSimpleQuery(){
        val bn = loadJsonBN("graphs/insurance.json")
        val trial = generateSample(bn)
        val simpleResult = simpleLnQuery(trial, bn)
        val ffResult = Math.log(variableEliminationQuery(trial, bn))

        Assert.assertEquals(simpleResult, ffResult, 1E-5)
    }

    @Test
    fun factorFilterQuery_hiddenVariables_IdenticalToSimpleQuery(){
        val bn = loadJsonBN("graphs/insurance.json")
        val hiddenVars = chooseHiddenVarsProportion(bn, 0.25)
        val trials = hideVars(generateSamples(bn, 100), hiddenVars)
        var simpleTotalTime = 0.0
        var ffTotalTime = 0.0
        for(trial in trials){
            val (simpleResult, simpleTime) = timeFunc({simpleLnQuery(trial, bn)})
            val (ffResult, ffTime) = timeFunc({Math.log(variableEliminationQuery(trial, bn))})
            simpleTotalTime += simpleTime
            ffTotalTime += ffTime
            Assert.assertEquals(simpleResult, ffResult, 1E-5)
        }

        //println("Simple Time : $simpleTotalTime, FF Time : $ffTotalTime")
    }

    @Test
    fun relevantBeliefFactors_singleInCliqueMatch_returnSummedOutFactor(){
        val result = relevantBeliefFactors(listOf(A, B), basicForest)
        Assert.assertEquals(1, result.size)
        Assert.assertEquals(f1.scope, result[0].scope)
        Assert.assertEquals(f1.values.toList(), result[0].values.toList())
    }

    @Test
    fun relevantBeliefFactors_multipleInCliqueMatch_returnSingleFactor(){
        val result = relevantBeliefFactors(listOf(B), basicForest)
        Assert.assertEquals(1, result.size)
        Assert.assertEquals(f1.scope, result[0].scope)
        Assert.assertEquals(f1.values.toList(), result[0].values.toList())
    }

    @Test
    fun relevantBeliefFactors_insidePathMatch_returnFactorDividedPath(){
        val result = relevantBeliefFactors(listOf(A, C), basicForest)
        println(result)
        Assert.assertEquals(2, result.size)
        Assert.assertEquals(f1.scope, result[0].scope)
        val expectedProbTable1 = listOf(1.0 / 2, 1.0 / 2, 1.0 / 6, 5.0 / 6)
        for((actual, expected) in result[0].values.zip(expectedProbTable1)){
            Assert.assertEquals(expected, actual, 1E-10)
        }

        Assert.assertEquals(f2.scope, result[1].scope)
        for((actual, expected) in result[1].values.zip(f2.values)){
            Assert.assertEquals(expected, actual, 1E-10)
        }
    }

    @Test
    fun relevantBeliefFactors_acrossGroupsMatch_returnNormedFactorProduct(){
        val result = relevantBeliefFactors(listOf(A, D), basicForest)
        Assert.assertEquals(2, result.size)
        Assert.assertEquals(f1, result[0])
        Assert.assertEquals(f3, result[1])
    }

    /*
    Test is empirical and takes too long to run! Not really a quick unit test
    @Test
    fun outOfCliqueQuery_identicalAnswersToVE(){
        val bn = loadJsonBN("graphs/insurance.json")
        val vocab = bn.vocab.toList()
        val fullFactors = extractFactors(bn)

        val cliqueForest = convertToCliqueForest(fullFactors)
        val potentialAssignments = assignPotentials(cliqueForest, fullFactors)
        val calibratedCliqueForest = CalibratedCliqueForest(cliqueForest.map { cliqueTree -> sumProductCalibrate(cliqueTree, potentialAssignments) })

        for(i in 1..100){
            val randomFamily = shuffle(fullFactors).first().scope
            val randomFamilyWithAddition = randomFamily + vocab.filter { !randomFamily.contains(it) }.random()

            val outOfCliqueResult = outOfCliqueQuery(randomFamilyWithAddition, calibratedCliqueForest)
            val varElimResult = variableEliminationQuery(randomFamilyWithAddition, emptyMap(), bn)

            Assert.assertEquals(varElimResult.scope, outOfCliqueResult.scope)
            for(pIndex in varElimResult.values.indices){
                Assert.assertEquals(varElimResult.values[pIndex], outOfCliqueResult.values[pIndex], 1E-10)
            }
        }
    }
    */
}

