import DNUnawareness.*
import org.junit.Assert
import org.junit.Test

fun dummyPInfo(rv : RandomVariable, pSet : PSet, logProb : Double = -0.5) =
    ParentSetInfo(rv, pSet, logProb, CountTable(listOf(rv) + pSet), Factor(listOf(rv) + pSet, uniformCPT(rv, pSet.toList())))

class BuntineUpdateTests{
    val A1 = RandomVariable("A1", 2, VarType.ACTION)
    val B1 = RandomVariable("B1", 2, VarType.BELIEF)
    val B2 = RandomVariable("B2", 2, VarType.BELIEF)
    val O1 = RandomVariable("O1", 2, VarType.OUTCOME)
    val O2 = RandomVariable("O2", 2, VarType.OUTCOME)

    @Test
    fun structuralUpdate_action_emptyParentSetOnly(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = { Math.log(1.0) }
        val paramPrior : List<Factor> = listOf(
            Factor(listOf(A1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(O1), listOf(0.5,0.5))
        )

        val trials : List<Trial> = listOf(
            //Trial(mapOf(A1 to 0, B1 to 0, O1 to 0), 5.0),
            //Trial(mapOf(A1 to 1, B1 to 1, O1 to 1), 10.0)
        )
        val result = structUpdate(A1, logPrior, setOf(A1,B1,O1), trials, listOf(), paramPrior, config)

        Assert.assertEquals(1, result.size)
        Assert.assertTrue(result[0].parentSet.isEmpty())
    }

    @Test
    fun structuralUpdate_multipleReasonableParents_resultsNormalized(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = { Math.log(1.0) }
        val paramPrior : List<Factor> = listOf(

            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf(
            //Trial(mapOf(A1 to 0, B1 to 0, O1 to 0), 5.0),
            //Trial(mapOf(A1 to 1, B1 to 1, O1 to 1), 10.0)
        )
        val result = structUpdate(O1, logPrior, setOf(A1, B1, O1), trials, listOf(), paramPrior, config)

        Assert.assertEquals(4, result.size)
        Assert.assertEquals(1.0, result.sumByDouble { Math.exp(it.logProbability) }, 1E-7)
    }

    @Test
    fun structuralUpdate_noOtherVocab_singleEmptyParents(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = { Math.log(1.0) }
        val paramPrior : List<Factor> = listOf(
            Factor(listOf(O1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf(
            //Trial(mapOf(A1 to 0, B1 to 0, O1 to 0), 5.0),
            //Trial(mapOf(A1 to 1, B1 to 1, O1 to 1), 10.0)
        )
        val result = structUpdate(O1, logPrior, setOf(O1), trials, listOf(), paramPrior, config)

        Assert.assertEquals(1, result.size)
        Assert.assertTrue(result[0].parentSet.isEmpty())
    }

    @Test
    fun structuralUpdate_maxParentsLimit_onlyParentSetsUnderLimit(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 1)
        val logPrior  : LogPrior<PSet> = { Math.log(1.0) }
        val paramPrior : List<Factor> = listOf(

            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf(
            //Trial(mapOf(A1 to 0, B1 to 0, O1 to 0), 5.0),
            //Trial(mapOf(A1 to 1, B1 to 1, O1 to 1), 10.0)
        )
        val result = structUpdate(O1, logPrior, setOf(A1, B1, O1), trials, listOf(), paramPrior, config)

        Assert.assertEquals(
            setOf(setOf(B1), setOf(A1), emptySet()),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test
    fun structuralUpdate_minParents_allReasonableHaveMin(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = { Math.log(1.0) }
        val paramPrior : List<Factor> = listOf(

            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf(
            //Trial(mapOf(A1 to 0, B1 to 0, O1 to 0), 5.0),
            //Trial(mapOf(A1 to 1, B1 to 1, O1 to 1), 10.0)
        )
        val result = structUpdate(O1, logPrior, setOf(A1, B1, O1), trials, listOf(Pair(A1, O1)), paramPrior, config)

        Assert.assertEquals(
            setOf(setOf(A1), setOf(A1, B1)),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test(expected=IllegalArgumentException::class)
    fun structuralUpdate_minParentsConflictsWithMax_throwException(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 1)
        val logPrior  : LogPrior<PSet> = { Math.log(1.0) }
        val paramPrior : List<Factor> = listOf()
        val trials : List<Trial> = listOf(
            //Trial(mapOf(A1 to 0, B1 to 0, O1 to 0), 5.0),
            //Trial(mapOf(A1 to 1, B1 to 1, O1 to 1), 10.0)
        )
        val result = structUpdate(O1, logPrior, setOf(A1, B1, O1), trials, listOf(Pair(A1, O1), Pair(B1, O1)), paramPrior, config)
    }

    @Test
    fun structuralUpdate_pSetsBelowThreshold_pSetsNotReasonable(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = {
            val probMap = mapOf(emptySet<RandomVariable>() to 0.33,
                                setOf(B1) to 0.33,
                                setOf(A1) to 0.34,
                                setOf(B1, A1) to 0.0
            )
            Math.log(probMap[it]!!)
        }
        val paramPrior : List<Factor> = listOf(
            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf()
        val result = structUpdate(O1, logPrior, setOf(B1, O1, A1), trials, listOf(), paramPrior, config)
        Assert.assertEquals(
            setOf(setOf(B1), setOf(A1), emptySet()),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test
    fun structuralUpdate_aboveThresholdButCantBeReached_notIncludedInReasonable(){
        val config = StructLearnConfig(0.001, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = {
            val probMap = mapOf(emptySet<RandomVariable>() to 0.1,
                setOf(B1) to 0.0,
                setOf(A1) to 0.0,
                setOf(B1, A1) to 0.9
            )
            Math.log(probMap[it]!!)
        }
        val paramPrior : List<Factor> = listOf(

            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf()
        val result = structUpdate(O1, logPrior, setOf(B1, O1, A1), trials, listOf(), paramPrior, config)
        Assert.assertEquals(
            setOf(emptySet<RandomVariable>()),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test
    fun structuralUpdate_bestParentSetUpdatesMidWay_updateAliveList(){
        val config = StructLearnConfig(0.5, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = {
            val probMap = mapOf(emptySet<RandomVariable>() to 0.0001,
                setOf(B1) to 0.1,
                setOf(A1) to 0.9,
                setOf(B1, A1) to 0.0
            )
            Math.log(probMap[it]!!)
        }
        val paramPrior : List<Factor> = listOf(

            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf()
        val result = structUpdate(O1, logPrior, setOf(B1, A1, O1), trials, listOf(), paramPrior, config)
        Assert.assertEquals(
            setOf(setOf(A1), emptySet()),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test
    fun structuralUpdate_pSetBelowThreshButSupersetAbove_keepSubset(){
        val config = StructLearnConfig(0.5, 1.0, 0.1, 4)
        val logPrior  : LogPrior<PSet> = {
            val probMap = mapOf(emptySet<RandomVariable>() to 0.0001,
                setOf(B1) to 0.0001,
                setOf(A1) to 0.0001,
                setOf(B1, A1) to 0.9
            )
            Math.log(probMap[it]!!)
        }
        val paramPrior : List<Factor> = listOf(

            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5,0.5)),
            Factor(listOf(A1), listOf(0.5,0.5))
        )
        val trials : List<Trial> = listOf()
        val result = structUpdate(O1, logPrior, setOf(B1, A1, O1), trials, listOf(), paramPrior, config)
        Assert.assertEquals(
            setOf(setOf(A1), setOf(B1), emptySet(), setOf(B1,A1)),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test
    fun parameterUpdate_newParentEv_filterInvalidPSets(){
        val reasonableParents = listOf(
            dummyPInfo(O1, emptySet()),
            dummyPInfo(O1, setOf(B1)),
            dummyPInfo(O1, setOf(A1)),
            dummyPInfo(O1, setOf(B1, A1))
        )

        val result = parameterUpdate(O1, reasonableParents, 1.0, null, listOf(Pair(A1, O1)))
        Assert.assertEquals(
            setOf(setOf(A1), setOf(B1, A1)),
            result.map { it.parentSet }.toSet()
        )
    }

    @Test
    fun paramterUpdate_equivalentToStructuralUpdate(){
        val config = StructLearnConfig(0.0, 1.0, 0.1, 4)
        val logPrior : LogPrior<PSet> = {
            val priorMap = mapOf(
                setOf(B1) to 0.5,
                emptySet<RandomVariable>() to 0.5
            )
            priorMap[it]!!
        }

        val paramPrior = listOf(
            Factor(listOf(O1), listOf(0.5,0.5)),
            Factor(listOf(B1), listOf(0.5, 0.5))
        )


        val reasonableParents = listOf(
            createPSetInfo(O1, emptySet(), emptyList(), logPrior, paramPrior, config.alphaStrength, emptyList()),
            createPSetInfo(O1, setOf(B1), emptyList(), logPrior, paramPrior, config.alphaStrength, emptyList())
        )

        val trials = listOf(
            Trial(mapOf(B1 to 0, O1 to 0), 5.0),
            Trial(mapOf(B1 to 1, O1 to 1), 10.0)
        )

        val result1 = parameterUpdate(O1, reasonableParents, 1.0, trials[0], emptyList())
        val result2 = parameterUpdate(O1, result1, 1.0, trials[1], emptyList())

        val batchResult = structUpdate(O1, logPrior, setOf(B1, O1), trials, emptyList(), paramPrior, config)

        // Empty set probability the same
        Assert.assertEquals(
            batchResult.find { it.parentSet.isEmpty() }!!.logProbability,
            result2.find { it.parentSet.isEmpty() }!!.logProbability,
            1E-7
        )

        // Single member probability the same
        Assert.assertEquals(
            batchResult.find { it.parentSet == setOf(B1) }!!.logProbability,
            result2.find { it.parentSet == setOf(B1) }!!.logProbability,
            1E-7
        )
    }

    @Test
    fun validOrderExists_success_returnTrue(){
        val possibleParents = mapOf(B1 to emptySet(), O1 to setOf(B1))
        val possibleChildren = mapOf(B1 to setOf(O1), O1 to emptySet())
        val parentEvidence = emptyList<DirEdge>()
        val rewardDomain = setOf(O1)

        val result = validOrderExists(possibleChildren, rewardDomain)
        Assert.assertTrue(result is SOSuccess)
    }

    @Test
    fun validOrderExists_multiplePossible_returnTrue(){
        val possibleParents = mapOf(B1 to emptySet(), B2 to emptySet(), O1 to setOf(B1, B2))
        val possibleChildren = mapOf(B1 to setOf(O1), B2 to setOf(O1), O1 to emptySet())
        val parentEvidence = emptyList<DirEdge>()
        val rewardDomain = setOf(O1)

        val result = validOrderExists(possibleChildren,  rewardDomain)
        Assert.assertTrue(result is SOSuccess)
    }

    @Test
    fun validOrderExists_beliefFailure_returnCulprits(){
        val possibleParents : Map<RandomVariable, Set<RandomVariable>> = mapOf(B1 to emptySet(), O1 to emptySet())
        val possibleChildren : Map<RandomVariable, Set<RandomVariable>> = mapOf(B1 to emptySet(), O1 to emptySet())
        val parentEvidence = emptyList<DirEdge>()
        val rewardDomain = setOf(O1)

        val result = validOrderExists(possibleChildren, rewardDomain)
        Assert.assertEquals(SOFail(listOf(B1)), result)
    }

    @Test
    fun validOrderExists_outcomeFailure_returnCulprits(){
        val possibleParents = mapOf(B1 to emptySet(), O1 to setOf(B1))
        val possibleChildren = mapOf(B1 to setOf(O1), O1 to emptySet())
        val parentEvidence = emptyList<DirEdge>()
        val rewardDomain = emptySet<RandomVariable>()

        val result = validOrderExists(possibleChildren, rewardDomain)
        Assert.assertEquals(SOFail(listOf(O1)), result)
    }

    @Test
    fun addNewVar_newAction_returnsBestDN(){
        val vocab = setOf(B1, O1)
        val config = StructLearnConfig(0.01, 1.0, 0.1, 4)
        val parentExtStrat = Conservative(config, 0.99)
        val rewardTable = RewardTable(listOf(O1), listOf(5.0, 10.0))


        val updater = BuntineUpdate(vocab, parentExtStrat, 100, config, true)
        val result = updater.addNewVars(DecisionNetwork(rewardTable, emptyList(), emptyList()), setOf(A1), rewardTable, emptyList(), emptyList())
        val expectedStruct = mapOf(O1 to setOf(B1, A1), B1 to emptySet())

        Assert.assertTrue(result is UpdateResult.Success)
        if(result is UpdateResult.Success){
            Assert.assertEquals(setOf(A1), result.result.actions.toSet())
            Assert.assertEquals(expectedStruct, result.result.sortedChanceNodes.associate { Pair(it.rv, it.parents.toSet()) })
        }
    }

    @Test
    fun addNewVar_newBelief_returnsBestDN(){
        val vocab = setOf(B1, O1)
        val config = StructLearnConfig(0.01, 1.0, 0.1, 4)
        val parentExtStrat = Conservative(config, 0.99)
        val rewardTable = RewardTable(listOf(O1), listOf(5.0, 10.0))


        val updater = BuntineUpdate(vocab, parentExtStrat, 100, config, true)
        val result = updater.addNewVars(DecisionNetwork(rewardTable, emptyList(), emptyList()), setOf(B2), rewardTable, emptyList(), listOf(Pair(B1,B2)))
        val expectedStruct = mapOf(O1 to setOf(B2), B1 to emptySet(), B2 to setOf(B1))

        Assert.assertTrue(result is UpdateResult.Success)
        if(result is UpdateResult.Success){
            Assert.assertEquals(expectedStruct, result.result.sortedChanceNodes.associate { Pair(it.rv, it.parents.toSet()) })
        }
    }

    @Test
    fun addNewVar_newOutcome_returnsBestDN(){
        val vocab = setOf(B1, O1)
        val config = StructLearnConfig(0.01, 1.0, 0.1, 4)
        val parentExtStrat = Conservative(config, 0.99)
        val rewardTable = RewardTable(listOf(O1), listOf(5.0, 10.0))


        val updater = BuntineUpdate(vocab, parentExtStrat, 100, config, true)
        val result = updater.addNewVars(DecisionNetwork(rewardTable, emptyList(), emptyList()), setOf(O2), rewardTable, emptyList(), listOf(Pair(B1, O1), Pair(O2, O1)))
        val expectedStruct = mapOf(O1 to setOf(B1, O2), B1 to emptySet(), O2 to emptySet())

        Assert.assertTrue(result is UpdateResult.Success)
        if(result is UpdateResult.Success){
            Assert.assertEquals(expectedStruct, result.result.sortedChanceNodes.associate { Pair(it.rv, it.parents.toSet()) })
        }
    }
}