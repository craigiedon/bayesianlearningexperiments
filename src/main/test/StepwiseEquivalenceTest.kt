import Utils.randomParams
import org.junit.Test

/*
class StepwiseTests{
    @Test
    fun stepwiseEquivalenceTest() {
        val trueBN = loadJsonBN("graphGeneration/insurance.json")
        val data = generateSamples(trueBN, 1000)
        val testData = generateSamples(trueBN, 5000)
        val randomParams = randomParams(rawStructure(trueBN))

        val batchEMParams = batchEM(randomParams, data, 50)
        val stepwiseParams = stepWiseEM(randomParams, data, 1, 10, 1.0)
        val fullyObservedParams = fullyObservedParamlearning(rawStructure(trueBN), data)

        val klDivBatchEM = KLDivEmpirical(trueBN, batchEMParams, testData)
        val klDivStep = KLDivEmpirical(trueBN, stepwiseParams, testData)
        val klDivObs = KLDivEmpirical(trueBN, fullyObservedParams, testData)

        println("Step $klDivStep, Obs $klDivObs, Batch Sanity Check : $klDivBatchEM")
    }

    @Test
    fun batchEMBetterThanNoHiddenVariablesTrueStruct(){
        // Explicitly sketch out which variables you want to hide in each case. 1, 2 ,3, 4 etc. With names
        val loadedBN = loadJsonBN("graphGeneration/insurance.json")
        val trueBN = hideNodes(loadedBN, listOf("Accident", "VehicleYear", "MakeModel", "RiskAversion"))
        println("Hidden Vars: ${trueBN.hiddenVars}")
        val data = hideVars(generateSamples(trueBN, 1000), trueBN.hiddenVars)
        val testData = hideVars(generateSamples(trueBN, 5000), trueBN.hiddenVars)

        val randomParams = randomParams(rawStructure(trueBN))

        val batchEMBN = batchEM(randomParams, data, 50)
        val stepwiseBN = stepWiseEM(randomParams, data, 50, 10, 0.9)
        val adTree = buildADTree(trueBN.observableVars, data, ADBuildParams(10, 200, 100, 0.6, 6))
        val ignoreHiddenBNLearnedStruct = fullyObservedParamlearning(greedyStructureSearch(initialStructure(trueBN.observableVars), adTree, 1.0, parentSizeRestriction = 5), data)

        val klDivBatchEM = KLDivEmpirical(trueBN.observableVars, trueBN, batchEMBN, testData)
        val klDivIgnoreHiddenBNStructLearn = KLDivEmpirical(trueBN.observableVars, trueBN, ignoreHiddenBNLearnedStruct, testData)
        val klDivStepwise = KLDivEmpirical(trueBN.observableVars, trueBN, stepwiseBN, testData)

        println("Batch EM: $klDivBatchEM, Ignore Hidden Learn Struct: $klDivIgnoreHiddenBNStructLearn, Stepwise: $klDivStepwise")
    }

    @Test
    fun structEMBetterThanNoHiddenVarsHiddenVarParentChildrenKnown(){
        // Explicitly sketch out which variables you want to hide in each case. 1, 2 ,3, 4 etc. With names
        val loadedBN = loadJsonBN("graphGeneration/insurance.json")
        val trueBN = hideNodes(loadedBN, listOf("Accident", "RiskAversion"))
        println("Hidden Vars: ${trueBN.hiddenVars}")
        val fullData = generateSamples(trueBN, 100)
        val data = hideVars(fullData, trueBN.hiddenVars)
        val testData = hideVars(generateSamples(trueBN, 5000), trueBN.hiddenVars)

        val hiddenVarParentChildren = parentChildSubstructure(rawStructure(trueBN), trueBN.hiddenVars)
        val initialStruct = initialStructure(trueBN.vocab) //, hiddenVarParentChildren)


        val paramEM = batchEM(randomParams(rawStructure(trueBN)), data, 50)

        val adTree = buildADTree(trueBN.observableVars, data, ADBuildParams(25, 500, 250, 0.5, 6))
        val completeADTree = buildADTree(trueBN.vocab.toList(), completeData(trueBN, data), ADBuildParams(25, 500, 250, 0.5, 6))

        val ignoreHiddenStruct = greedyStructureSearch(initialStructure(trueBN.observableVars), adTree, 1.0, parentSizeRestriction = 5)
        val ignoreHiddenBN = fullyObservedParamlearning(ignoreHiddenStruct, data)


        /*
        val statsFromAD = sufficientStats(ignoreHiddenStruct, adTree, emptyMap())
        val statsFromCompletedAD =sufficientStats(ignoreHiddenStruct, completeADTree, emptyMap())

        for((family, adCountTable) in statsFromAD){
            println("Family: $family")
            val completedADCountTable = statsFromCompletedAD[family]!!
            for(assignment in allAssignments(adCountTable.scope)){
                val adCount = adCountTable.counts[assignmentToIndex(assignment, adCountTable.scope)]
                val completedCount = completedADCountTable.counts[assignmentToIndex(assignment, completedADCountTable.scope)]
                val difference = adCount - completedCount
                println("adCount: $adCount, completedCount: $completedCount, difference: $difference")
            }
            println()
        }
        */

        val structEM = structuralEMNoAD(randomParams(initialStruct), data, 50, emptyMap(), 5)


        val klStructEM = KLDivEmpirical(trueBN.observableVars, trueBN, structEM, testData)
        val klIgnoreHiddenBNStructLearn = KLDivEmpirical(trueBN.observableVars, trueBN, ignoreHiddenBN, testData)
        val klParamEM = KLDivEmpirical(trueBN.observableVars, trueBN, paramEM, testData)

        println("Struct EM: $klStructEM, Ignore Hidden Learn Struct: $klIgnoreHiddenBNStructLearn, True struct Known: $klParamEM")
        //println(parentChildSubstructure(rawStructure(structEM), trueBN.hiddenVars))

        val SEMWithoutHidden = rawStructure(structEM).filterKeys { !trueBN.hiddenVars.contains(it)}
        val ignoreHiddenTestScore = BDeuScore(ignoreHiddenStruct, emptyMap(), sufficientStats(listOf(ignoreHiddenStruct), testData), 1.0)
        val ignoreHiddenTrainScore = BDeuScore(ignoreHiddenStruct, emptyMap(), sufficientStats(listOf(ignoreHiddenStruct), data), 1.0)

        val structEMTestScore = BDeuScore(SEMWithoutHidden, emptyMap(), sufficientStats(listOf(SEMWithoutHidden), testData), 1.0)
        val structEMTrainScore = BDeuScore(SEMWithoutHidden, emptyMap(), sufficientStats(listOf(SEMWithoutHidden), data), 1.0)

        println("Ignore Score (Test) : $ignoreHiddenTestScore, SEM Score (Test): $structEMTestScore")
        println("Ignore Score (Training) : $ignoreHiddenTrainScore, SEM Score (Train): $structEMTrainScore")

        for((rv, parents) in rawStructure(structEM)){
            if(ignoreHiddenStruct[rv] != parents){
                println("Different parents for $rv: SEM: $parents, Ignored: ${ignoreHiddenStruct[rv]}")
            }
        }
    }

    @Test
    fun noADStructLearnVsADStructLearn(){
        // Explicitly sketch out which variables you want to hide in each case. 1, 2 ,3, 4 etc. With names
        val loadedBN = loadJsonBN("graphGeneration/insurance.json")
        val trueBN = hideNodes(loadedBN, listOf("Accident", "RiskAversion"))
        println("Hidden Vars: ${trueBN.hiddenVars}")
        val fullData = generateSamples(trueBN, 1000)
        val data = hideVars(fullData, trueBN.hiddenVars)
        val testData = hideVars(generateSamples(trueBN, 5000), trueBN.hiddenVars)

        val initialStruct = initialStructure(trueBN.vocab) //, hiddenVarParentChildren)
        val initialBN = randomParams(initialStruct)

        val structEM = structuralEM(initialBN, data, 50, emptyMap(), 5)
        val structEMNoAD = structuralEMNoAD(initialBN, data, 50, emptyMap(), 5)


        val klStructEM = KLDivEmpirical(trueBN.observableVars, trueBN, structEM, testData)
        val klStructEMNoAD = KLDivEmpirical(trueBN.observableVars, trueBN, structEMNoAD, testData)

        println("Struct EM: $klStructEM, No AD: $klStructEMNoAD")

        val SEMWithoutHidden = rawStructure(structEM).filterKeys { !trueBN.hiddenVars.contains(it)}
        val SEMNoADWithoutHidden = rawStructure(structEMNoAD).filterKeys { !trueBN.hiddenVars.contains(it)}

        val structEMTestScore = BDeuScore(SEMWithoutHidden, emptyMap(), sufficientStats(listOf(SEMWithoutHidden), testData), 1.0)
        val structEMTrainScore = BDeuScore(SEMWithoutHidden, emptyMap(), sufficientStats(listOf(SEMWithoutHidden), data), 1.0)

        val structEMNoADTestScore = BDeuScore(SEMNoADWithoutHidden, emptyMap(), sufficientStats(listOf(SEMNoADWithoutHidden), testData), 1.0)
        val structEMNoADTrainScore = BDeuScore(SEMNoADWithoutHidden, emptyMap(), sufficientStats(listOf(SEMNoADWithoutHidden), data), 1.0)

        println("No AD Score (Test) : $structEMNoADTestScore, AD Score (Test): $structEMTestScore")
        println("No AD Score (Training) : $structEMNoADTrainScore, AD Score (Train): $structEMTrainScore")
    }

    @Test
    fun structEMBetterThanNoHiddenVariables(){
        // Explicitly sketch out which variables you want to hide in each case. 1, 2 ,3, 4 etc. With names
        val loadedBN = loadJsonBN("graphGeneration/insurance.json")
        val trueBN = hideNodes(loadedBN, listOf("Accident", "RiskAversion"))
        println("Hidden Vars: ${trueBN.hiddenVars}")
        val data = hideVars(generateSamples(trueBN, 5000), trueBN.hiddenVars)
        val testData = hideVars(generateSamples(trueBN, 5000), trueBN.hiddenVars)

        val hiddenVarParentChildren = parentChildSubstructure(rawStructure(trueBN), trueBN.hiddenVars)
        val initialStruct = initialStructure(trueBN.vocab, hiddenVarParentChildren)

        val structEM = structuralEM(randomParams(initialStruct), data, 50, emptyMap(), 5)
        val adTree = buildADTree(trueBN.observableVars, data, ADBuildParams(10, 200, 100, 0.6, 6))

        val ignoreHiddenStruct = greedyStructureSearch(initialStructure(trueBN.observableVars), adTree, 1.0, parentSizeRestriction = 5)
        val ignoreHiddenBNLearnedStruct = fullyObservedParamlearning(ignoreHiddenStruct, data)

        val klDivStructEM = KLDivEmpirical(trueBN.observableVars, trueBN, structEM, testData)
        val klDivIgnoreHiddenBNStructLearn = KLDivEmpirical(trueBN.observableVars, trueBN, ignoreHiddenBNLearnedStruct, testData)

        println("No Monotonic Info Given. Struct EM: $klDivStructEM, Ignore Hidden Learn Struct: $klDivIgnoreHiddenBNStructLearn")
    }

    @Test
    fun structEMEquivToStepwiseWithIncHeuristic(){

    }
}
*/
