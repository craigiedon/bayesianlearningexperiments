import org.junit.Assert
import org.junit.Test

class GraphGenerationTests{
    @Test
    fun utilityBNStruct_noParentBeliefsIncluded(){
        val actions = (1..2).map { RandomVariable("A$it", 2) }
        val B1 = RandomVariable("B1", 2, VarType.BELIEF)
        val beliefs = listOf(B1)

        val O1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val outcomes = listOf(O1)

        val result = utilityBNStruct(actions, beliefs, outcomes, outcomes, 3,3)
        Assert.assertEquals(setOf(B1, O1), result.keys)
        Assert.assertEquals(emptySet<RandomVariable>(), result[B1]!!)
    }
}
