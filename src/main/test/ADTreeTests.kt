import org.junit.*

class ADTreeTests{
    val A = RandomVariable("A", 2)
    val B = RandomVariable("B", 2)
    val C = RandomVariable("C", 2)
    val noBufferOrLeafConfig = ADBuildParams(-1, 0, 0, 0.0,5)

    @Test
    fun noData_ZeroCounts(){
        val data = emptyList<RVAssignment>()
        val adNode = buildADTree(listOf(A,B,C), data, noBufferOrLeafConfig)

        Assert.assertEquals(0, adNode.rootNode.unweightedCount)
    }

    @Test
    fun noDataNoLeafList_VaryNodesStillExist(){
        val data = emptyList<RVAssignment>()
        val adNode = buildADTree(listOf(A,B,C), data, noBufferOrLeafConfig)
        val root = adNode.rootNode

        if(root is ADNode.SubTree){
            Assert.assertEquals(3, root.varyNodes.size)
        }
        else{
            Assert.fail("Should not create a leaf list when leaf threshold is 0")
        }
    }

    @Test
    fun noDataNoLeafList_countTableZero(){
        val data = emptyList<RVAssignment>()
        val adNode = buildADTree(listOf(A,B,C), data, noBufferOrLeafConfig)
        val countTable = makeWeightedCountTable(listOf(A,B,C), adNode)

        Assert.assertEquals(8, countTable.counts.size)
        Assert.assertTrue(countTable.counts.all { it == 0.0 })
    }

    @Test(expected = IllegalArgumentException :: class)
    fun noScope_throwError(){
        val data = emptyList<RVAssignment>()
        buildADTree(emptyList(), data, noBufferOrLeafConfig)
    }

    @Test
    fun singleVarSomeData_returnAccurateCount(){
        val data = listOf(0,0,1,1,0,0).map{mapOf(A to it)}
        val adTree = buildADTree(listOf(A), data, noBufferOrLeafConfig)
        val countTable = makeWeightedCountTable(listOf(A), adTree)
        Assert.assertEquals(4.0, countTable.counts[0], 1E-10)
        Assert.assertEquals(2.0, countTable.counts[1], 1E-10)
    }

    @Test
    fun doubleVarSomeData_accurateCount(){
        val data = listOf(
                listOf(0,0),
                listOf(1,1),
                listOf(1,1),
                listOf(1,1),
                listOf(0,1),
                listOf(0,1)
        ).map { mapOf(A to it[0], B to it[1]) }

        val adTree = buildADTree(listOf(A, B), data, noBufferOrLeafConfig)
        val countTable = makeWeightedCountTable(listOf(A, B), adTree)

        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(2.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(3.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1), countTable.scope)], 1E-10)
    }

    @Test
    fun doubleVarReversedCTConstruction_noErrorsAccurateCount(){
        val data = listOf(
                listOf(0,0),
                listOf(1,1),
                listOf(1,1),
                listOf(1,1),
                listOf(0,1),
                listOf(0,1)
        ).map { mapOf(A to it[0], B to it[1]) }

        val adTree = buildADTree(listOf(A, B), data, noBufferOrLeafConfig)
        val countTable = makeWeightedCountTable(listOf(B, A), adTree)

        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(2.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(3.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1), countTable.scope)], 1E-10)
    }

    @Test
    fun tripleVar_accurateCount(){
        val data = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,0,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A, B, C), data, noBufferOrLeafConfig)
        val countTable = makeWeightedCountTable(listOf(A, B, C), adTree)

        Assert.assertEquals(2.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(3.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(4.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(5.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 1), countTable.scope)], 1E-10)
    }

    @Test
    fun tripleVarReorderedCTConstruction_accurateCount(){
        val data = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,0,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A, B, C), data, noBufferOrLeafConfig)
        val countTable = makeWeightedCountTable(listOf(B, C, A), adTree)

        Assert.assertEquals(2.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(3.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(4.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(5.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 1), countTable.scope)], 1E-10)
    }

    // For sequential tests, want to test what happens when I add to an empty AD Node
    @Test
    fun emptyADNode_AddData_ReturnsNewADNode(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val adTree = buildADTree(listOf(A,B,C), emptyList<RVAssignment>(), buildParams)
        Assert.assertTrue(adTree.rootNode is ADNode.LeafList)

        val additionalData = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        updateADTree(adTree, additionalData)
        Assert.assertTrue(adTree.rootNode is ADNode.SubTree)
    }
    @Test
    fun leafList_BelowLeafThreshold_StaysLeafList(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val adTree = buildADTree(listOf(A,B,C), emptyList<RVAssignment>(), buildParams)

        val additionalData = listOf(
                listOf(0,0,0),
                listOf(1,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        updateADTree(adTree,additionalData)
        Assert.assertTrue(adTree.rootNode is ADNode.LeafList)
    }

    // What if I'm above the leaf list but below buffer amounts
    @Test
    fun varyNodeLeafLists_AboveLeafThreshBelowLowerBuffThresh_StayAsUnexpandedVaryNode(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val initialData = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A,B,C), initialData, buildParams)
        val root = adTree.rootNode
        Assert.assertTrue(root is ADNode.SubTree && root.varyNodes[A]!! is VaryNode.BufferedVaryNode)
    }

    @Test
    fun varyNodeLeafLists_AboveLowerBuffThreshRatioUnsatisfied_StayAsUnexpandedVaryNode(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val initialData = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(1,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A,B,C), initialData, buildParams)
        val root = adTree.rootNode
        Assert.assertTrue(root is ADNode.SubTree && root.varyNodes[A]!! is VaryNode.BufferedVaryNode)
    }

    @Test
    fun varyNodeLeafLists_AboveLowerBuffThreshRatioSatisfied_ExpandSubADNodes(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val initialData = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A,B,C), initialData, buildParams)
        val root = adTree.rootNode
        Assert.assertTrue(root is ADNode.SubTree && root.varyNodes[A]!! is VaryNode.ExpandedVaryNode)
    }

    @Test
    fun varyNodeLeafLists_AboveUpperBuffThreshRatioUnsatisfied_ExpandSubADNodes(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val initialData = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(1,0,0),
                listOf(1,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A,B,C), initialData, buildParams)
        val root = adTree.rootNode
        Assert.assertTrue(root is ADNode.SubTree && root.varyNodes[A]!! is VaryNode.ExpandedVaryNode)
    }

    @Test
    fun varyNodeLeafLists_AboveUpperBuffThreshRatioSatisfied_ExpandSubADNodes(){
        val buildParams = ADBuildParams(2, 5, 4, 0.6,5)
        val initialData = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(0,0,0)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val adTree = buildADTree(listOf(A,B,C), initialData, buildParams)
        val root = adTree.rootNode
        Assert.assertTrue(root is ADNode.SubTree && root.varyNodes[A]!! is VaryNode.ExpandedVaryNode)
    }

    @Test
    fun queryOnBatchAndIncrementalADTreeEquivalent(){
        val buildParams = ADBuildParams(1, 3, 2, 0.6,5)
        val data = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,0,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }

        val vocab = listOf(A,B,C)

        val batchADTree = buildADTree(vocab, data, buildParams)
        val sequentialADTree = buildADTree(vocab, emptyList<RVAssignment>(), buildParams)
        for(sample in data){
            updateADTree(sequentialADTree, listOf(sample))
        }

        val batchCT = makeWeightedCountTable(vocab, batchADTree)
        val sequentialCT = makeWeightedCountTable(vocab, sequentialADTree)
        Assert.assertEquals(batchCT.scope, sequentialCT.scope)
        Assert.assertEquals(batchCT.counts.toList(), sequentialCT.counts.toList())
    }

    @Test
    fun hugeData_batchAndSeqQueryEquivalent(){
        val vocab = ExampleBNs.studentBN.nodes.keys.toList()
        val buildParams = ADBuildParams(10, 200, 100, 0.6, vocab.size)
        val data = generateSamples(ExampleBNs.studentBN, 5000)

        val batchADTree = buildADTree(vocab, data, buildParams)
        val sequentialADTree = buildADTree(vocab, emptyList<RVAssignment>(), buildParams)
        for(i in 0..((data.size - 1) / 100)){
            updateADTree(sequentialADTree, data.subList(i * 100, (i+1) * 100))
        }

        val batchCT = makeWeightedCountTable(vocab, batchADTree)
        val sequentialCT = makeWeightedCountTable(vocab, sequentialADTree)
        Assert.assertEquals(batchCT.scope, sequentialCT.scope)
        Assert.assertEquals(batchCT.counts.toList(), sequentialCT.counts.toList())
    }

    @Test
    fun multADTree_factor1_IdenticalADTree(){
        val buildParams = ADBuildParams(1, 3, 2, 0.6,5)
        val data = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,0,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }
        val vocab = listOf(A,B,C)
        val ADTree = buildADTree(vocab, data, buildParams)
        multiplyADTCounts(1.0, buildADTree(vocab, data, buildParams).rootNode)
        val countTable = makeWeightedCountTable(vocab, ADTree)

        Assert.assertEquals(2.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(3.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(4.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(1.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(5.0, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 1), countTable.scope)], 1E-10)
    }

    @Test
    fun multADTree_factor_weightedCountsWeightedByFactor(){
        val buildParams = ADBuildParams(1, 3, 2, 0.6,5)
        val data = listOf(
                listOf(0,0,0),
                listOf(0,0,0),
                listOf(1,0,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(0,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,1,0),
                listOf(1,0,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1),
                listOf(1,1,1)

        ).map { mapOf(A to it[0], B to it[1], C to it[2]) }
        val vocab = listOf(A,B,C)
        val ADTree = buildADTree(vocab, data, buildParams)
        multiplyADTCounts(0.1, ADTree.rootNode)
        val countTable = makeWeightedCountTable(vocab, ADTree)

        Assert.assertEquals(0.2, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.1, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.3, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.4, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 0), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(0.1, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 0, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(0.0, countTable.counts[assignmentToIndex(mapOf(A to 0, B to 1, C to 1), countTable.scope)], 1E-10)
        Assert.assertEquals(0.5, countTable.counts[assignmentToIndex(mapOf(A to 1, B to 1, C to 1), countTable.scope)], 1E-10)
    }
}