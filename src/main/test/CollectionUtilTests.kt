import Utils.cartesianProduct
import Utils.findPairFromSeparateGroups
import org.junit.Assert
import org.junit.Test

class CollectionUtilTests{
    @Test
    fun findPairInSeparateGroups_noGroupsGiven_throwException(){
        Assert.assertNull(findPairFromSeparateGroups(emptyList<List<Int>>(), {_, _ -> true}))
    }

    @Test
    fun findPairInSeparateGroups_oneGroupGiven_throwException(){
        Assert.assertNull(findPairFromSeparateGroups(listOf(listOf(1)), { _, _ -> true}))
    }

    @Test
    fun findPairInSeparateGroups_noMatches_returnNull(){
        val l1 = listOf(1,2,3)
        val l2 = listOf(3,4,5)
        val result = findPairFromSeparateGroups(listOf(l1,l2), {a, b -> a + b > 100})
        Assert.assertNull(result)
    }

    @Test
    fun findPairInSeparateGroups_onlyMatchInSameGroup_returnNull(){
        val l1 = listOf(1,2,3)
        val l2 = listOf(100,200,300)
        val result = findPairFromSeparateGroups(listOf(l1,l2), {a, b -> a + b == 4})
        Assert.assertNull(result)
    }

    @Test
    fun findPairInSeparateGroups_validAndInvalidMatch_returnValidMatch(){
        val l1 = listOf(0,1,2,3)
        val l2 = listOf(4)
        val result = findPairFromSeparateGroups(listOf(l1,l2), {a, b -> a + b == 5})
        Assert.assertEquals(Pair(1,4), result)
    }

    @Test
    fun findPairInSeparateGroups_validMatchOnly_returnValidMatch(){
        val l1 = listOf(0,1,2,3)
        val l2 = listOf(40)
        val result = findPairFromSeparateGroups(listOf(l1,l2), {a, b -> a + b == 42})
        Assert.assertEquals(Pair(2,40), result)
    }

    @Test
    fun findPairInSeparateGroups_multipleValidMatches_returnFirstValidMatch(){
        val l1 = listOf(0,1,2,3)
        val l2 = listOf(40, 50, 60)
        val result = findPairFromSeparateGroups(listOf(l1,l2), {a, b -> a + b > 40})
        Assert.assertEquals(Pair(0,50), result)
    }

    @Test
    fun findPairInSeparateGroups_matchRequiresUsingThirdGroup_returnValidMatch(){
        val l1 = listOf(0,1,2,3)
        val l2 = listOf(40, 50, 60)
        val l3 = listOf(1000, 3000)
        val result = findPairFromSeparateGroups(listOf(l1,l2, l3), {a, b -> a + b == 3002})
        Assert.assertEquals(Pair(2,3000), result)
    }

    @Test(expected = IllegalArgumentException :: class)
    fun cartesianProduct_emptyListFirst_throwsException(){
        cartesianProduct(emptyList(), listOf(1,2,3))
    }

    @Test(expected = IllegalArgumentException :: class)
    fun cartesianProduct_emptyListSecond_throwsException(){
        cartesianProduct(listOf(1,2,3), emptyList())
    }

    @Test(expected = IllegalArgumentException :: class)
    fun cartesianProduct_emptyListBoth_throwsException(){
        cartesianProduct(emptyList<Int>(), emptyList<Int>())
    }

    @Test
    fun cartesianProduct_singleItemEach_returnSinglePair(){
        val result = cartesianProduct(listOf(1), listOf(5)).toList()
        Assert.assertEquals(1, result.size)
        Assert.assertEquals(Pair(1,5), result[0])
    }

    @Test
    fun cartesianProduct_singleAndMulti_returnMultiplePairs(){
        val result = cartesianProduct(listOf(1), listOf(5,6,7)).toList()
        Assert.assertEquals(3, result.size)
        Assert.assertEquals(listOf(Pair(1,5), Pair(1,6), Pair(1,7)), result)
    }

    @Test
    fun cartesianProduct_multiAndMulti_returnMultiplePairs(){
        val result = cartesianProduct(listOf(2,3), listOf(4,5)).toList()
        Assert.assertEquals(4, result.size)
        Assert.assertEquals(listOf(Pair(2,4), Pair(2,5), Pair(3,4), Pair(3,5)), result)
    }

}
