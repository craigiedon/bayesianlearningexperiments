import DNUnawareness.*
import org.junit.Assert
import org.junit.Test

class DNStructSolverTests{


    @Test
    fun solveDNILP_rewardVarsOnly_returnNaiveStruct(){
        val reasonableParents = mapOf(A to listOf(dummyPInfo(A, emptySet())) )
        val rDom = setOf(A)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, true)
        Assert.assertEquals(mapOf(A to reasonableParents[A]!![0]), result)
    }

    @Test
    fun solveDNILP_singleRPChoices_returnOnlyDAG(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, emptySet()),
                dummyPInfo(A, setOf(B))
            ),
            B to listOf(
                dummyPInfo(B, emptySet()),
                dummyPInfo(B, setOf(A))
            )
        )

        val rDom = setOf(A)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, true)

        val expected = mapOf(
            A to reasonableParents[A]!![1],
            B to reasonableParents[B]!![0]
        )

        Assert.assertEquals(expected, result)
    }

    @Test()
    fun solveDNILP_onlyChoicesAreACycle_failure(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, setOf(B))
            ),
            B to listOf(
                dummyPInfo(B, setOf(A))
            )
        )

        val rDom = setOf(A)
        val result  = DNStructSolver.solveDNILP(reasonableParents, rDom, true)
        Assert.assertNull(result)
    }

    @Test
    fun solveDNILP_onlyChoicesCycleButNoConnectivity_failure(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, setOf(B)),
                dummyPInfo(A, setOf(B, C))
            ),
            B to listOf(
                dummyPInfo(B, setOf(A)),
                dummyPInfo(B, setOf(A, C))
            ),
            C to listOf(
                dummyPInfo(C, emptySet())
            )
        )

        val rDom = emptySet<RandomVariable>()
        val result  = DNStructSolver.solveDNILP(reasonableParents, rDom, false)
        Assert.assertNull(result)
    }

    @Test()
    fun solveDNILP_onlyChoicesAreDisconnected_exception(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, emptySet())
            ),
            B to listOf(
                dummyPInfo(B, emptySet())
            )
        )

        val rDom = setOf(A)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, true)
        Assert.assertNull(result)
    }

    @Test
    fun solveDNILP_connectivityNotRequired_returnDisconnected(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, emptySet())
            ),
            B to listOf(
                dummyPInfo(B, emptySet())
            )
        )

        val rDom = setOf(A)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, false)

        val expected = mapOf(
            A to reasonableParents[A]!![0],
            B to reasonableParents[B]!![0]
        )

        Assert.assertEquals(expected, result)
    }

    @Test
    fun solveDNILP_cycleAndNonCycleSolutions_returnNonCycle(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, emptySet()),
                dummyPInfo(A, setOf(B))
            ),
            B to listOf(
                dummyPInfo(B, emptySet()),
                dummyPInfo(B, setOf(A))
            )
        )

        val rDom = setOf(A)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, false)

        val expected = mapOf(
            A to reasonableParents.getValue(A)[0],
            B to reasonableParents.getValue(B)[0]
        )

        Assert.assertEquals(expected, result)
    }

    @Test
    fun solveDNILP_twoValidDAGS_returnHighestScoring(){
        val reasonableParents = mapOf(
            A to listOf(
                dummyPInfo(A, emptySet()),
                dummyPInfo(A, setOf(B), -0.9)
            ),
            B to listOf(
                dummyPInfo(B, emptySet()),
                dummyPInfo(B, setOf(A), -0.1)
            )
        )

        val rDom = setOf(A, B)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, true)

        val expected = mapOf(
            A to reasonableParents[A]!![0],
            B to reasonableParents[B]!![1]
        )

        Assert.assertEquals(expected, result)
    }

    @Test
    fun solveDNILP_orderAskedFor_orderingUpheld(){
        val O1 = RandomVariable("O1", 2, VarType.OUTCOME)
        val O2 = RandomVariable("O2", 2, VarType.OUTCOME)

        val reasonableParents = mapOf(
            O1 to listOf(
                dummyPInfo(O1, emptySet())
            ),
            O2 to listOf(
                dummyPInfo(O2, setOf(O1))
            )
        )

        val rDom = setOf(O1)
        val result = DNStructSolver.solveDNILP(reasonableParents, rDom, false)

        val expected = mapOf(
            O1 to reasonableParents.getValue(O1)[0],
            O2 to reasonableParents.getValue(O2)[0]
        )

        Assert.assertEquals(expected, result)
    }
}
