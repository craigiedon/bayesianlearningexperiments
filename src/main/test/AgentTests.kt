import DNUnawareness.Agent
import DNUnawareness.BetterActionInference
import DNUnawareness.checkForMisunderstandings
import org.junit.Assert
import org.junit.Test

class AgentTests{
    val A1 = RandomVariable("A1", 2, VarType.ACTION)
    val A2 = RandomVariable("A2", 2, VarType.ACTION)

    @Test
    fun checkForMisunderstanding_emptyAdviceHist_returnNull(){
        val betterActionInferences = listOf<BetterActionInference>()
        val result = checkForMisunderstandings(betterActionInferences, BetterActionInference(mapOf(A1 to 0), mapOf(A1 to 1), mapOf(B to 0), 0))
        Assert.assertNull(result)
    }

    @Test
    fun checkForMisunderstanding_noConflicts_returnNull(){
        val betterActionInferences = listOf(
            BetterActionInference(mapOf(A1 to 0), mapOf(A1 to 1), mapOf(B to 0), 0)
        )
        val result = checkForMisunderstandings(betterActionInferences, BetterActionInference(mapOf(A1 to 1), mapOf(A1 to 0), mapOf(B to 1), 1))
        Assert.assertNull(result)
    }

    @Test
    fun checkForMisunderstanding_actionAdviceDifferentButNotConflicting_returnNull(){
        val betterActionInferences = listOf(
            BetterActionInference(mapOf(A1 to 0, A2 to 0), mapOf(A1 to 0, A2 to 1), mapOf(B to 0), 0)
        )
        val result = checkForMisunderstandings(betterActionInferences, BetterActionInference(mapOf(A1 to 0, A2 to 0), mapOf(A1 to 1, A2 to 1), mapOf(B to 0), 1))
        Assert.assertNull(result)
    }

    @Test
    fun checkForMisunderstanding_newAdviceConflicts_returnPair(){
        val betterActionInferences = listOf(
            BetterActionInference(
                mapOf(A1 to 0, A2 to 0),
                mapOf(A1 to 1, A2 to 1),
                mapOf(B to 0), 0)
        )

        val result = checkForMisunderstandings(betterActionInferences,
            BetterActionInference(
                mapOf(A1 to 1, A2 to 1),
                mapOf(A1 to 0, A2 to 0),
                mapOf(B to 0), 1)
        )

        Assert.assertEquals(Pair(0,1), result)
    }

}