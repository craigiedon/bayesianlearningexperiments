fun adIncrementalStructureLearning(startingBN : BayesNet, data : List<RVAssignment>, stepSize : Int) : BNStruct {
    var learnedStructure = rawStructure(startingBN)
    val buildParams = ADBuildParams(10, 200, 100, 0.6, 5)
    val vocab = learnedStructure.keys.toList()
    val adTree = buildADTree(vocab, emptyList<RVAssignment>(), buildParams)
    for(i in 0..(data.size - 1) / stepSize){
        updateADTree(adTree, data.subList(i * stepSize, (i + 1) * stepSize))
        learnedStructure = greedyStructureSearch(vocab, adTree, 1.0)
    }

    return learnedStructure
}

fun naiveOneStepStructureLearning(startingBN: BayesNet, data : List<RVAssignment>, stepSize : Int) : Map<RandomVariable, Set<RandomVariable>>{
    var currentStructure = rawStructure(startingBN)
    var neighbours = validNeighbors(currentStructure).map { it.bnStruct }
    var sufficientStatistics = sufficientStats(neighbours + currentStructure, emptyList<RVAssignment>(), emptyMap())
    val smoothingCount = 5.0
    for((i, sample) in data.withIndex()){
        for((_, countTable) in sufficientStatistics){
            countTable.updateCounts(sample)
        }

        if(i % stepSize == 0){
            val neighboursAndScores = neighbours.map { Pair(it, BDeuScore(it, emptyMap(), sufficientStatistics, smoothingCount)) }
            val bestNeighbor = neighboursAndScores.maxBy { it.second }!!
            val currentStructureScore = BDeuScore(currentStructure, emptyMap(), sufficientStatistics, smoothingCount)
            if(bestNeighbor.second > currentStructureScore){
                currentStructure = bestNeighbor.first
                neighbours = validNeighbors(currentStructure).map { it.bnStruct }
                sufficientStatistics = sufficientStats(neighbours + currentStructure, data.take(i), sufficientStatistics)
            }
        }
    }

    return currentStructure
}

fun adOneStepStructureLearning(startingBN : BayesNet, data : List<RVAssignment>, stepSize : Int) : Map<RandomVariable, Set<RandomVariable>>{
    var currentStructure = rawStructure(startingBN)
    var neighbours = validNeighbors(currentStructure).map { it.bnStruct }
    var sufficientStatistics = sufficientStats(neighbours + currentStructure, emptyList<RVAssignment>(), emptyMap())
    val smoothingCount = 5.0
    val vocab = currentStructure.keys.toList()
    val buildParams = ADBuildParams(10, 200, 100, 0.6, 5)
    val adTree = buildADTree(vocab, emptyList<RVAssignment>(), buildParams)
    for((i, sample) in data.withIndex()){
        for((_, countTable) in sufficientStatistics){
            countTable.updateCounts(sample)
        }


        if(i % stepSize == 0){
            // I think this is slightly wrong... you are missing some counts here. Doesnt matter for the purpose of speed testing though
            if(i >= stepSize){
                updateADTree(adTree, data.subList(i - stepSize, i))
            }
            val neighboursAndScores = neighbours.map { Pair(it, BDeuScore(it, emptyMap(), sufficientStatistics, smoothingCount)) }
            val bestNeighbor = neighboursAndScores.maxBy { it.second }!!
            val currentStructureScore = BDeuScore(currentStructure, emptyMap(), sufficientStatistics, smoothingCount)
            if(bestNeighbor.second > currentStructureScore){
                currentStructure = bestNeighbor.first
                neighbours = validNeighbors(currentStructure).map { it.bnStruct }
                sufficientStatistics = sufficientStats(neighbours + currentStructure, adTree, sufficientStatistics)
            }
        }
    }

    return currentStructure
}

fun testRelativeSpeeds(){
    val trueBN = ExampleBNs.studentBN
    val data = generateSamples(trueBN, 50000)
    println("Samples generated")
    val startingBN = BayesNet(trueBN.nodes.mapValues { BNode(it.value.rv, emptyList(), listOf(0.5, 0.5)) })

    val stepSize = 1000

    val adIncrementalStart = System.currentTimeMillis()
    val adIncrementalLearned = adIncrementalStructureLearning(startingBN, data, stepSize)
    println("AD Incremental Time : ${System.currentTimeMillis() - adIncrementalStart}")

    val vocab = trueBN.nodes.keys.toList()
    val adBatchStart = System.currentTimeMillis()
    val adTree = buildADTree(vocab, data, ADBuildParams(10, 200, 100, 0.6, 5))
    val adBatchLearned = greedyStructureSearch(vocab, adTree, 1.0)
    println("AD Batch Time : ${System.currentTimeMillis() - adBatchStart}")

    val naiveOneStepStart = System.currentTimeMillis()
    val naiveOneStepLearned = naiveOneStepStructureLearning(startingBN, data, stepSize)
    println("Naive One Step Time : ${System.currentTimeMillis() - naiveOneStepStart}")

    val adOneStepStart = System.currentTimeMillis()
    val adOneStepLearned = adOneStepStructureLearning(startingBN, data, stepSize)
    println("AD One Step Time : ${System.currentTimeMillis() - adOneStepStart}")
}

fun ADBuildingVersusUpdate(){
    val trueBN = loadJsonBN("graphGeneration/insurance.json")
    val hiddenData = generateSamples(trueBN, 10000).map { s -> s.filter { (k,_) -> k.name != "Antilock"} }
    val data = completeData(trueBN, hiddenData)

    val buildParams = ADBuildParams(50, 500, 250, 0.5, 5)
    val startFull = System.currentTimeMillis()
    val fullADTree = buildADTree(trueBN.vocab.toList(), data, buildParams)
    println("Full duration: ${System.currentTimeMillis() - startFull}")
    println("Depth Full: (avg) - ${avgDepth(fullADTree)} (max) - ${maxDepth(fullADTree)}")

    val startInPlaceBuild = System.currentTimeMillis()
    val inPlaceAD = buildADTree(trueBN.vocab.toList(), emptyList<RVAssignment>(), buildParams)
    val stepSize = 100
    for(i in 0..(data.size / stepSize) - 1){
        updateADTree(inPlaceAD, data.subList(i * stepSize, (i + 1) * stepSize))
    }
    println("Depth sequential: (avg) - ${avgDepth(inPlaceAD)} (max) - ${maxDepth(inPlaceAD)}")
    println("In place build cumulative: ${System.currentTimeMillis() - startInPlaceBuild}")

    sufficientStats(listOf(rawStructure(trueBN)), inPlaceAD, emptyMap())
}

fun main(args : Array<String>){
    ADBuildingVersusUpdate()
}
