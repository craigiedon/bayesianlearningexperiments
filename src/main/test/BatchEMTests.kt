import Utils.randomParams
import java.util.*

fun main(args : Array<String>){
    //paramEMVersusJustIgnoreHidden()
    //structEMAbandonsInitialLinksLowData()
    badStartNeverGetsEdgesBack()
}

/* This should be an actual unit test
fun testHiddenVarRemoval(){
    val trueStruct = rawStructure(loadJsonBN("graphGeneration/asia.json"))
    val removedStruct = removeVar(trueStruct, trueStruct.keys.find { it.name == "either" }!!)
    println(removedStruct)
}
*/

fun badStartNeverGetsEdgesBack(){
    val loadedBN = loadJsonBN("graphGeneration/insurance.json")
    val hiddenRVCandidates = listOf("MakeModel", "VehicleYear", "RiskAversion", "RuggedAuto").map { name -> loadedBN.vocab.find { it.name == name }!! }
    val testData = generateSamples(loadedBN, 5000)

    val fullData = generateSamples(loadedBN, 5000)
    val hiddenRVs = hiddenRVCandidates.take(1)
    val observedData = hideVars(fullData, hiddenRVs)

    val trueBN = hideNodes(loadedBN, hiddenRVs)
    val trueStruct = rawStructure(trueBN)

    val hRVChild = whatDoesThisAffect(hiddenRVs[0], trueBN, trueBN.vocab.toList())
    val mandatoryParents = mapOf(hRVChild to setOf(hiddenRVs[0]))
    println("Mandatory Parents: $mandatoryParents")

    println("True Substruct: ${parentChildSubstructure(trueStruct, hiddenRVs)}")
    val SEMBadStart = structuralEM(randomParams(initialStructure(trueBN.vocab, mandatoryParents), 2), observedData, 50, mandatoryParents, parentSizeRestriction = 5)
    println("Learned substruct: ${parentChildSubstructure(rawStructure(SEMBadStart), hiddenRVs)}")
}

fun paramEMVersusJustIgnoreHidden(){
    val loadedBN = loadJsonBN("graphGeneration/insurance.json")
    val hiddenRVCandidates = listOf("MakeModel", "VehicleYear", "RiskAversion", "RuggedAuto").map { name -> loadedBN.vocab.find { it.name == name }!! }
    val dataSizes = listOf(100, 500, 1000)
    val testData = generateSamples(loadedBN, 5000)

    val results = HashMap<String, ArrayList<Double>>()

    for(numVars in 1..hiddenRVCandidates.size) {
        val hiddenRVs = hiddenRVCandidates.take(numVars)
        val trueBN = hideNodes(loadedBN, hiddenRVs)
        val trueStruct = rawStructure(trueBN)

        for(dataSize in dataSizes){

            val klsBatchEM = ArrayList<Double>()
            val klsNoHVLearnedStruct = ArrayList<Double>()

            for(experimentNum in 1..10){
                println("Exp Num : $experimentNum, HVs: $hiddenRVs, Data Size: $dataSize")
                val fullData = generateSamples(loadedBN, dataSize)
                val observedData = hideVars(fullData, trueBN.hiddenVars)

                val batchEM = batchEM(trueStruct, observedData, 50)
                val noHVLearnedStruct = fullyObservedParamlearning(greedyStructureSearch(trueBN.observableVars, observedData, 1.0, parentSizeRestriction =  5), observedData)

                klsBatchEM.add(KLDivEmpirical(trueBN.observableVars, trueBN, batchEM, testData))
                klsNoHVLearnedStruct.add(KLDivEmpirical(trueBN.observableVars, trueBN, noHVLearnedStruct, testData))
            }

            val avgKLBatch = klsBatchEM.average()
            val avgKLNoHVLearnedStruct = klsNoHVLearnedStruct.average()

            results.getOrPut("batchEM-hv$numVars", { ArrayList()}).add(avgKLBatch)
            results.getOrPut("noHVLearned-hv$numVars", { ArrayList() }).add(avgKLNoHVLearnedStruct)
        }
    }

    saveToJson(results, "paramEMVsHiddenResults", "results")
}

fun structEMAbandonsInitialLinksLowData(){
    val loadedBN = loadJsonBN("graphGeneration/insurance.json")
    val hiddenRVCandidates = listOf("MakeModel", "VehicleYear", "RiskAversion", "RuggedAuto").map { name -> loadedBN.vocab.find { it.name == name }!! }
    val dataSizes = listOf(10)
    val testData = generateSamples(loadedBN, 5000)

    val results = HashMap<String, ArrayList<Double>>()

    for(numVars in 1..hiddenRVCandidates.size) {
        val hiddenRVs = hiddenRVCandidates.take(numVars)
        val trueBN = hideNodes(loadedBN, hiddenRVs)
        val trueStruct = rawStructure(trueBN)

        for(dataSize in dataSizes){

            val klsSEM = ArrayList<Double>()
            val klsNoHVLearnedStruct = ArrayList<Double>()

            for(experimentNum in 1..10){
                println("Exp Num : $experimentNum, HVs: $hiddenRVs, Data Size: $dataSize")
                println("True HV Parent Child Structure : ${parentChildSubstructure(trueStruct, hiddenRVs)}")
                val fullData = generateSamples(loadedBN, dataSize)
                val observedData = hideVars(fullData, trueBN.hiddenVars)

                val SEM = structuralEM(randomParams(trueStruct, 2), observedData, 50, parentSizeRestriction = 5)
                println("SEM HV Parent Child Structure : ${parentChildSubstructure(rawStructure(SEM), hiddenRVs)}")
                val noHVLearnedStruct = fullyObservedParamlearning(greedyStructureSearch(trueBN.observableVars, observedData, 1.0, parentSizeRestriction =  5), observedData)

                klsSEM.add(KLDivEmpirical(trueBN.observableVars, trueBN, SEM, testData))
                klsNoHVLearnedStruct.add(KLDivEmpirical(trueBN.observableVars, trueBN, noHVLearnedStruct, testData))
            }

            val avgKLBatch = klsSEM.average()
            val avgKLNoHVLearnedStruct = klsNoHVLearnedStruct.average()

            results.getOrPut("SEM-hv$numVars", { ArrayList()}).add(avgKLBatch)
            results.getOrPut("noHVLearned-hv$numVars", { ArrayList() }).add(avgKLNoHVLearnedStruct)
        }
    }

    saveToJson(results, "paramEMVsHiddenResults", "results")
}

fun batchEMConvergenceRuns(){
    val loadedBN = loadJsonBN("graphGeneration/asia.json")
    val hiddenRVs = listOf("either").map { name -> loadedBN.vocab.find { it.name ==  name }!! }
    val trueBN = hideNodes(loadedBN, hiddenRVs)//, "VehicleYear", "MakeModel", "RiskAversion"))
    println("Hidden Vars: ${trueBN.hiddenVars}")
    val data = hideVars(generateSamples(trueBN, 1000), trueBN.hiddenVars)
    val testData = hideVars(generateSamples(trueBN, 5000), trueBN.hiddenVars)

    val randomParams = randomParams(rawStructure(trueBN), 2)

    val batchEM10s = (1..50).map { batchEM(randomParams(rawStructure(trueBN), 2), data, 10) }
    val batchEMBN10 = batchEM10s.maxBy { bn -> logLikelihood(data, bn) }!!
    val batchEMBN50 = batchEM(randomParams, data, 50)

    val ignoreHiddenVars = fullyObservedParamlearning(removeVars(rawStructure(trueBN), hiddenRVs), data)

    val kl10 = KLDivEmpirical(trueBN.observableVars, trueBN, batchEMBN10, testData)
    val kl50 = KLDivEmpirical(trueBN.observableVars, trueBN, batchEMBN50, testData)
    val klIgnore = KLDivEmpirical(trueBN.observableVars, trueBN, ignoreHiddenVars, testData)

    val ll10 = logLikelihood(data, batchEMBN10)
    val ll50 = logLikelihood(data, batchEMBN50)
    val llIgnore = logLikelihood(data, ignoreHiddenVars)

    val ll10Test = logLikelihood(testData, batchEMBN10)
    val ll50Test = logLikelihood(testData, batchEMBN50)
    val llIgnoreTest = logLikelihood(testData, ignoreHiddenVars)

    println("KL 10: $kl10, 50: $kl50, ignore: $klIgnore")
    println("LL (Train) 10: $ll10, 50: $ll50, ignore: $llIgnore")
    println("LL (Test) 10 :$ll10Test, 50 : $ll50Test, ignore: $llIgnoreTest")
}