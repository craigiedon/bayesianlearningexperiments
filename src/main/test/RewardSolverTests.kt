import Utils.random
import Utils.randomList
import org.junit.Assert
import org.junit.Test
import java.io.File
import java.util.*

/*
class RewardSolverTests{
    @Test
    fun singleVarNoConstraints_allZeroReward(){
        val rDom = listOf(A)
        val existsStates = listOf<Trial>()
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 1)

        Assert.assertEquals(1, result.size)
        Assert.assertTrue(result[0].values.all { it == 0.0 })
    }

    @Test
    fun singleVarOneConstraint_SingleNonZeroEntry(){
        val rDom = listOf(A)
        val existsStates = listOf(Trial(mapOf(A to 0), 10.0))
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 1)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.size)
        Assert.assertEquals(listOf(A), result[0].scope)
        Assert.assertEquals(10.0, result[0].values[0], 10E-10)
        Assert.assertEquals(0.0, result[0].values[1], 10E-10)
    }

    @Test
    fun twoVarsNoConstraints_allZero(){
        val rDom = listOf(A, B)
        val result = FactoredRewardSolver.solve(rDom, emptyList(), emptyList(), 1)

        Assert.assertEquals(1, result.size)
        Assert.assertTrue(result[0].values.all { it == 0.0 })
    }

    @Test
    fun twoVarsSingleConstraint_SingleNonZeroEntrySingleScope(){
        val rDom = listOf(A,B)
        val existsStates = listOf(Trial(mapOf(A to 0, B to 0), 10.0))
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 1)

        Assert.assertEquals(1, result.size)
        Assert.assertEquals(1, result[0].scope.size)
        Assert.assertEquals(0.0, result[0].values[1], 10E-10)
        Assert.assertEquals(10.0, result[0].values[0], 10E-10)
    }

    @Test
    fun twoVarsFullConstraintsButIndependent_twoRewardFuncs(){
        val rDom = listOf(A, B)
        val existsStates = listOf(
            Trial(mapOf(A to 0, B to 0), 7.0),
            Trial(mapOf(A to 1, B to 0), 12.0),
            Trial(mapOf(A to 0, B to 1), 8.0),
            Trial(mapOf(A to 1, B to 1), 13.0)
        )
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 1)

        Assert.assertEquals(2, result.size)
        Assert.assertTrue(result.all{it.scope.size == 1})

        // What I really need here is a function that I stick in an assignment and it tells me how much reward I get
    }

    @Test
    fun twoVarsFullConstraints_oneBigRewardFunc(){
        val rDom = listOf(A, B)
        val existsStates = listOf(
            Trial(mapOf(A to 0, B to 0), 7.0),
            Trial(mapOf(A to 1, B to 0), 12.0),
            Trial(mapOf(A to 0, B to 1), 8.0),
            Trial(mapOf(A to 1, B to 1), 0.0)
        )
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 2)

        Assert.assertEquals(1, result.size)
        Assert.assertEquals(2, result[0].scope.size)

        // What I really need here is a function that I stick in an assignment and it tells me how much reward I get
    }

    @Test
    fun twoVarsFullConstraintsLowBound_Infeasible(){
        val rDom = listOf(A, B)
        val existsStates = listOf(
            Trial(mapOf(A to 0, B to 0), 7.0),
            Trial(mapOf(A to 1, B to 0), 12.0),
            Trial(mapOf(A to 0, B to 1), 8.0),
            Trial(mapOf(A to 1, B to 1), 0.0)
        )
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 1)

        Assert.assertTrue(result.isEmpty())
    }

    @Test
    fun threeVarsFullConstraints_oneBigRewardFunc(){
        val rDom = listOf(A, B, C)
        val existsStates = listOf(
            Trial(mapOf(A to 0, B to 0, C to 0), 7.0),
            Trial(mapOf(A to 1, B to 0, C to 0), 10.0),
            Trial(mapOf(A to 0, B to 1, C to 0), 0.0),
            Trial(mapOf(A to 1, B to 1, C to 0), 2.0),
            Trial(mapOf(A to 0, B to 0, C to 1), 1.0),
            Trial(mapOf(A to 1, B to 0, C to 1), 8.0),
            Trial(mapOf(A to 0, B to 1, C to 1), 14.0),
            Trial(mapOf(A to 1, B to 1, C to 1), 1.0)
        )
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 3)

        Assert.assertEquals(1, result.size)
        Assert.assertEquals(3, result[0].scope.size)
    }


    // For later, better action constraints
    @Test
    fun twoVarsPartialTrial_correctRewardFunc(){
        val rDom = listOf(A, B)
        val existsStates = listOf(
            Trial(mapOf(A to 0), 7.0),
            Trial(mapOf(A to 1), 12.0),
            Trial(mapOf(B to 1), 8.0),
            Trial(mapOf(B to 1), 0.0)
        )
        val result = FactoredRewardSolver.solve(rDom, emptyList(), existsStates, 2)

        Assert.assertEquals(1, result.size)
        Assert.assertEquals(2, result[0].scope.size)
    }

    @Test
    fun twoVarsBetterActionReccomendation_assignsToSomeState(){
        val rDom = listOf(A, B)
        val betterActionAdvice = listOf(
            BetterActionAdvice(mapOf(A to 0), 7.0, 0),
            BetterActionAdvice(emptyMap(), 10.0, 1)
        )
        val result = FactoredRewardSolver.solve(rDom, betterActionAdvice, emptyList(), 2)

        Assert.assertEquals(1, result.size)
        Assert.assertEquals(1, result[0].scope.size)
    }

    @Test
    fun updateInfo_emptyTrialsNewTrial_singleTrialInList(){
        val result = updateInfo(emptyList(), emptyList(), Trial(mapOf(A to 0), 10.0))

        val expectedAdvice = emptyList<BetterActionAdvice>()
        val expectedTrials = listOf(Trial(mapOf(A to 0), 10.0))

        Assert.assertEquals(Pair(expectedTrials, expectedAdvice), result)
    }

    @Test
    fun updateInfo_emptyNewAdvice_singleAdviceInList(){
        val trials = emptyList<Trial>()
        val advice = emptyList<BetterActionAdvice>()
        val newAdvice = BetterActionAdvice(mapOf(A to 0), 10.0,1)
        val result = updateInfo(trials, advice, newAdvice)

        val expectedAdvice = listOf(BetterActionAdvice(mapOf(A to 0), 10.0, 1))

        Assert.assertEquals(expectedAdvice, result)
    }

    @Test
    fun updateInfo_singleTrialNewTrial_twoTrialsInList(){
        val trials = listOf(Trial(mapOf(A to 0), 10.0))
        val advice = emptyList<BetterActionAdvice>()
        val newTrial = Trial(mapOf(A to 1), 20.0)

        val result = updateInfo(trials, advice, newTrial)

        val expectedTrials = listOf(Trial(mapOf(A to 0), 10.0), Trial(mapOf(A to 1), 20.0))
        val expectedAdvice = emptyList<BetterActionAdvice>()

        Assert.assertEquals(Pair(expectedTrials, expectedAdvice), result)
    }

    @Test
    fun updateInfo_newTrialIsIrrelevant_maintainPreviousList(){
        val trials = listOf(Trial(mapOf(A to 1, B to 1), 10.0))
        val advice = emptyList<BetterActionAdvice>()
        val newTrial = Trial(mapOf(A to 1), 10.0)

        val result = updateInfo(trials, advice, newTrial)

        val expectedTrials = listOf(Trial(mapOf(A to 1, B to 1), 10.0))
        val expectedAdvice = emptyList<BetterActionAdvice>()

        Assert.assertEquals(Pair(expectedTrials, expectedAdvice), result)
    }

    @Test
    fun updateInfo_newTrialDeprecatesOldOne_filteredTrials(){
        val trials = listOf(Trial(mapOf(A to 0), 10.0))
        val advice = emptyList<BetterActionAdvice>()
        val newTrial = Trial(mapOf(A to 0, B to 0), 10.0)

        val result = updateInfo(trials, advice, newTrial)

        val expectedTrials = listOf(Trial(mapOf(A to 0, B to 0), 10.0))
        val expectedAdvice = emptyList<BetterActionAdvice>()

        Assert.assertEquals(Pair(expectedTrials, expectedAdvice), result)
    }

    @Test
    fun updateInfo_newTrialDeprecatesBetterActionAdvice_filteredAdvice(){
        val trials = emptyList<Trial>()
        val advice = listOf(BetterActionAdvice(mapOf(A to 0), 3.0, 1))
        val newTrial = Trial(mapOf(A to 0, B to 0), 10.0)

        val result = updateInfo(trials, advice, newTrial)

        val expectedTrials = listOf(newTrial)
        val expectedAdvice = emptyList<BetterActionAdvice>()

        Assert.assertEquals(Pair(expectedTrials, expectedAdvice), result)
    }

    @Test
    fun updateInfo_adviceIrrelevantBecauseOfTrial_noChange(){
        val trials = listOf(Trial(mapOf(A to 0, B to 0), 10.0))
        val advice = emptyList<BetterActionAdvice>()
        val newAdvice = BetterActionAdvice(mapOf(A to 0, B to 0), 3.0, 1)

        val result = updateInfo(trials, advice, newAdvice)

        val expectedAdvice = emptyList<BetterActionAdvice>()

        Assert.assertEquals(expectedAdvice, result)
    }

    @Test
    fun updateInfo_adviceIrrelevantBecauseOfOldAdvice_noChange(){
        val trials = emptyList<Trial>()
        val advice = listOf(BetterActionAdvice(mapOf(A to 0, B to 0), 10.0,0))
        val newAdvice = BetterActionAdvice(mapOf(A to 0), 3.0, 1)

        val result = updateInfo(trials, advice, newAdvice)

        val expectedAdvice = advice

        Assert.assertEquals(expectedAdvice, result)
    }

    @Test
    fun updateInfo_newAdviceDeprecatesOldAdvice_filteredAdvice(){
        val trials = emptyList<Trial>()
        val advice = listOf(BetterActionAdvice(mapOf(A to 0), 3.0, 0))
        val newAdvice = BetterActionAdvice(mapOf(A to 0, B to 0), 10.0, 1)

        val result = updateInfo(trials, advice, newAdvice)

        val expectedAdvice = listOf(newAdvice)

        Assert.assertEquals(expectedAdvice, result)
    }
}

fun main(args : Array<String>){
    unfactoredVersusFactoredSpeed()
}
// Performance Tests
fun unfactoredVersusFactoredSpeed(){

    val logPrinter = File("logs/printTest.txt").printWriter()

    for(rewardDomainSize in listOf(15)){
        for(repetition in 1..10){
            val trueRewardDom = (1..rewardDomainSize).map{RandomVariable("X$it", 2)}
            val factorScopes = randomFactorScopes(trueRewardDom, 4)
            val rewardTables = factorScopes.map{ RewardTable (it, randomList(0.0, 10.0, numAssignments((it))))}
            val fullTrials = generateSamples(trueRewardDom, 1000)
                .map { Trial(it, totalValue(it, rewardTables)) }

            val learnedRewardDom = mutableListOf(trueRewardDom.random())
            var trialEvidence = emptyList<Trial>() // Note I am currently assuming that reward domain knowledge equals vocab knowledge

            for((i, fullTrial) in fullTrials.withIndex()){
                trialEvidence = updateInfo(trialEvidence, emptyList(), fullTrial).first

                var (factoredResult, factoredTime) = timeFunc { FactoredRewardSolver.solve(learnedRewardDom, emptyList(), trialEvidence, 4) }

                while(factoredResult.isEmpty()){
                    println("RDom : $learnedRewardDom Infeasible! Asking expert for help.")
                    learnedRewardDom.add(unknownRewardVar(trialEvidence, learnedRewardDom, trueRewardDom))
                    factoredResult = timeFunc { FactoredRewardSolver.solve(learnedRewardDom, emptyList(), trialEvidence, 4) }.first
                }

                println("Trial Num : $i, Rep: $repetition, trueRDom: $trueRewardDom, learnedRDom: ${learnedRewardDom.size}, Time: $factoredTime")
                logPrinter.println("$rewardDomainSize, $repetition, $factoredTime")
            }
        }
    }

    logPrinter.close()
}

*/
