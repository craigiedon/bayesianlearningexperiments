import DNUnawareness.*
import org.junit.Assert
import org.junit.Test

class ParentExtensionTests{
    val O1 = RandomVariable("O1", 2, VarType.OUTCOME)
    val O2 = RandomVariable("O2", 2, VarType.OUTCOME)
    val B1 = RandomVariable("B1", 2, VarType.BELIEF)

    @Test(expected=IllegalArgumentException::class)
    fun extendParents_conservativeNoNewVocab_throwException(){
        val config = StructLearnConfig(0.0001, 1.0, 0.1, 4)
        val parentExt = Conservative(config, 0.99)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to { p -> 0.0 }
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5))
        )
        val additionalVocab = setOf<RandomVariable>()
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)
    }

    @Test
    fun extendParents_conservative_newProbsPropOldOnes(){
        val config = StructLearnConfig(0.0001, 1.0, 0.1, 4)
        val parentExt = Conservative(config, 0.99)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  minParentsPrior(O1, setOf(O1, O2), config.singleParentCost),
            O2 to minParentsPrior(O2, setOf(O1, O2), config.singleParentCost)
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        val oldReasonableO1 = structUpdate(O1, oldStructPrior[O1]!!, setOf(O1 , O2), trials, parentEv, priorParams, config)

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)

        val oldPO2 = oldReasonableO1.find { it.parentSet == setOf(O2) }!!
        val oldPEmpty = oldReasonableO1.find { it.parentSet == emptySet<RandomVariable>() }!!
        val newPO2 = result.reasonablePSets[O1]!!.find { it.parentSet == setOf(O2) }!!
        val newPEmpty = result.reasonablePSets[O1]!!.find { it.parentSet == emptySet<RandomVariable>() }!!

        Assert.assertEquals(oldPO2.logProbability - oldPEmpty.logProbability, newPO2.logProbability - newPEmpty.logProbability, 1E-7)
    }

    @Test
    fun extendParents_conservativeMultipleNew_newProbsCorrect(){
        val config = StructLearnConfig(0.0001, 1.0, 0.1, 4)
        val parentExt = Conservative(config, 0.99)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  minParentsPrior(O1, setOf(O1, O2), config.singleParentCost),
            O2 to minParentsPrior(O2, setOf(O1, O2), config.singleParentCost)
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        val oldReasonableO1 = structUpdate(O1, oldStructPrior[O1]!!, setOf(O1, O2), trials, parentEv, priorParams, config)

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)

        val oldPO2 = oldReasonableO1.find { it.parentSet == setOf(O2) }!!
        val newPO2B2 = result.reasonablePSets[O1]!!.find { it.parentSet == setOf(O2, B1) }!!

        Assert.assertEquals(newPO2B2.logProbability, oldPO2.logProbability + Math.log(config.singleParentCost), 1E-7)
    }

    @Test
    fun extendParents_conservative_maxParentsStillObeyed(){
        val config = StructLearnConfig(0.0001, 1.0, 0.1, 1)
        val parentExt = Conservative(config, 0.99)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  minParentsPrior(O1, setOf(O1, O2), config.singleParentCost),
            O2 to minParentsPrior(O2, setOf(O1, O2), config.singleParentCost)
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)

        Assert.assertEquals(
            setOf(setOf(O2), setOf(B1), setOf()),
            result.reasonablePSets[O1]!!.map { it.parentSet }.toSet())
    }

    @Test
    fun extendParents_conservative_probsOnNewVarFollowMinPrior(){
        val config = StructLearnConfig(0.0001, 1.0, 0.1, 4)
        val parentExt = Conservative(config, 0.99)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  minParentsPrior(O1, setOf(O1, O2), config.singleParentCost),
            O2 to minParentsPrior(O2, setOf(O1, O2), config.singleParentCost)
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)
        Assert.assertEquals(setOf(emptySet<RandomVariable>()), result.reasonablePSets[B1]!!.map { it.parentSet }.toSet())
    }

    @Test
    fun extendParents_nonCon_reasonableParentsBasedOnMinPrior(){
        val config = StructLearnConfig(0.01, 1.0, 0.1, 4)
        val parentExt = NonConservative(config)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  mapPrior(mapOf(emptySet<RandomVariable>() to Math.log(1.0))),
            O2 to  mapPrior(mapOf(emptySet<RandomVariable>() to Math.log(1.0)))
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)
        Assert.assertEquals(setOf(emptySet(), setOf(O2), setOf(B1), setOf(O2, B1)), result.reasonablePSets[O1]!!.map { it.parentSet }.toSet())
    }

    @Test
    fun extendParents_nonCon_noneOverMaxParents(){
        val config = StructLearnConfig(0.01, 1.0, 0.1, 1)
        val parentExt = NonConservative(config)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  mapPrior(mapOf(emptySet<RandomVariable>() to Math.log(1.0))),
            O2 to  mapPrior(mapOf(emptySet<RandomVariable>() to Math.log(1.0)))
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>()
        val trials = listOf<Trial>()

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)
        Assert.assertEquals(setOf(emptySet(), setOf(O2), setOf(B1)), result.reasonablePSets[O1]!!.map { it.parentSet }.toSet())
    }

    @Test
    fun extendParents_nonCon_priorFollowsMinStruct(){
        val config = StructLearnConfig(0.01, 1.0, 0.1, 1)
        val parentExt = NonConservative(config)
        val oldStructPrior : Map<RandomVariable, LogPrior<PSet>> = mapOf(
            O1 to  mapPrior(mapOf(emptySet<RandomVariable>() to Math.log(1.0))),
            O2 to  mapPrior(mapOf(emptySet<RandomVariable>() to Math.log(1.0)))
        )

        val priorParams = listOf(
            Factor(listOf(O1), listOf(0.5, 0.5)),
            Factor(listOf(O2), listOf(0.5, 0.5))
        )

        val additionalVocab = setOf(B1)
        val parentEv = listOf<DirEdge>(Pair(O2, O1))
        val trials = listOf<Trial>()

        val result = parentExt.extendParents(oldStructPrior, priorParams, additionalVocab, parentEv, trials)
        Assert.assertEquals(setOf(setOf(O2)), result.reasonablePSets[O1]!!.map { it.parentSet }.toSet())
    }
}