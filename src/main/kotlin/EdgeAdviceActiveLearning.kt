import Utils.logOfBase
import Utils.shuffle
import Utils.sumInnerLnProbs
import java.util.*

fun main(args : Array<String>){
    val numSamplesTestResults = HashMap<Int, List<Map<String, Map<String, Double>>>>()
    for(numSamples in listOf(100, 500, 1000, 5000, 10000)){
        println("Num Samples: $numSamples")
        val results = ArrayList<Map<String, Map<String, Double>>>()
        for(i in 1..10){
            println(i)
            results.add(interactiveAdviceApprox(numSamples, 3))
        }
        numSamplesTestResults[numSamples] = results
    }

    val adviceAmountTestResults = HashMap<Int, List<Map<String, Map<String, Double>>>>()
    for(adviceAmount in listOf(1, 3, 5, 10)){
        println("Advice Amount: $adviceAmount")
        val results = ArrayList<Map<String, Map<String, Double>>>()
        for(i in 1..10){
            println(i)
            results.add(interactiveAdviceApprox(1000, adviceAmount))
        }
        adviceAmountTestResults[adviceAmount] = results
    }

    val fullResults = mapOf("NumSamples" to numSamplesTestResults, "adviceAmount" to adviceAmountTestResults)
    saveToJson(fullResults, "graphGeneration", "expertAdviceExperiments")
}

fun interactiveAdviceApprox(numTrainingSamples : Int, adviceAmount : Int) : Map<String, Map<String, Double>>{
    val trueBN = loadJsonBN("graphGeneration/asia.json")
    val trueStruct = rawStructure(trueBN)
    val vocab = trueBN.vocab.toList()
    val observedData = generateSamples(trueBN, numTrainingSamples)

    val threshold = 0.0

    val buildParams = ADBuildParams(10, 200, 100, 0.5, 100)
    var graphPost : Map<BNStruct, Double>

    // Before youve seen the data
    println("Random Advice Run")
    val randomExpertAdvice = ArrayList<EdgeRelation>()
    for(i in 1..adviceAmount){
        val randomVar1 = vocab[(Math.random() * vocab.size).toInt()]
        val randomVar2 = vocab.filter { it != randomVar1 }[(Math.random() * vocab.size - 1).toInt()]
        val expertAnswer = edgeRelationInStruct(randomVar1, randomVar2, trueStruct)
        println("Gave advice : $expertAnswer")
        randomExpertAdvice.add(expertAnswer)
    }
    println()

    // After you've got the info
    println("Informed Advice Run")
    val informedExpertAdvice = ArrayList<EdgeRelation>()
    val adTree = buildADTree(trueBN.vocab.toList(), observedData, buildParams)
    for (i in 1..adviceAmount){
        // Approx graph post using MCMC or other method
        graphPost = shotgunStochasticSearch(vocab, adTree, informedExpertAdvice)
        val bestQ = bestQuestion(vocab, graphPost, threshold) ?: break
        val expertAnswer = edgeRelationInStruct(bestQ.first, bestQ.second, trueStruct)

        println("Gave advice : $expertAnswer")
        informedExpertAdvice.add(expertAnswer)
    }

    val mapNoAdvice = greedyStructureSearch(vocab, adTree, 1.0)
    val mapInformedAdvice = greedyStructureSearch(vocab, adTree, 1.0, mustHaveParents = edgeRelsToParentStructure(informedExpertAdvice), mustBeNoEdges = edgeRelsToNonEdges(informedExpertAdvice))
    val mapRandomAdvice = greedyStructureSearch(vocab, adTree, 1.0, mustHaveParents = edgeRelsToParentStructure(randomExpertAdvice), mustBeNoEdges = edgeRelsToNonEdges(randomExpertAdvice))
    val informedAdvicePosterior = shotgunStochasticSearch(vocab, adTree, informedExpertAdvice)
    val randomAdvicePosterior = shotgunStochasticSearch(vocab, adTree, randomExpertAdvice)
    val noAdvicePosterior = shotgunStochasticSearch(vocab, adTree, emptyList())

    val l1ErrorMap = mapOf("NoAdviceMAP" to L1EdgeError(trueStruct, mapOf(mapNoAdvice to 1.0)),
        "InformedAdviceMAP" to L1EdgeError(trueStruct, mapOf(mapInformedAdvice to 1.0)),
        "RandomAdviceMAP" to L1EdgeError(trueStruct, mapOf(mapRandomAdvice to 1.0)),
        "NoAdviceBayes" to L1EdgeError(trueStruct, noAdvicePosterior),
        "InformedAdviceBayes" to L1EdgeError(trueStruct, informedAdvicePosterior),
        "RandomAdviceBayes" to L1EdgeError(trueStruct, randomAdvicePosterior)
    )

    val testData = generateSamples(trueBN, 5000)
    val testErrorMap = mapOf("NoAdviceMAP" to structurePredictionError(adTree, testData, mapOf(mapNoAdvice to 1.0)),
        "InformedAdviceMAP" to structurePredictionError(adTree, testData, mapOf(mapInformedAdvice to 1.0)),
        "RandomAdviceMAP" to structurePredictionError(adTree, testData, mapOf(mapRandomAdvice to 1.0)))
        //"NoAdviceBayes" to structurePredictionError(adTree, testData, noAdvicePosterior),
        //"InformedAdviceBayes" to structurePredictionError(adTree, testData, informedAdvicePosterior),
        //"RandomAdviceBayes" to structurePredictionError(adTree, testData, randomAdvicePosterior))

    return mapOf("L1" to l1ErrorMap, "Prediction" to testErrorMap)
}

typealias EdgeQuestion = Pair<RandomVariable, RandomVariable>
fun bestQuestion(vocab : List<RandomVariable>, graphPost : Map<BNStruct, Double>, threshold : Double) : EdgeQuestion?{
    val edgeQuestions = unorderedPairs(vocab)
    val questionEntropies = HashMap<Pair<RandomVariable, RandomVariable>, Double>()
    for(edgeQuestion in edgeQuestions){
        val (rv1, rv2) = edgeQuestion
        var probNoEdge = 0.0
        var probRV1Parent = 0.0
        var probRV2Parent = 0.0
        for((possibleStruct, structProb) in graphPost){
            val edgeRel = edgeRelationInStruct(rv1, rv2, possibleStruct)
            when(edgeRel){
                is EdgeRelation.NoEdge ->
                    probNoEdge += structProb
                is EdgeRelation.DirectedEdge ->
                    if(edgeRel.parent == rv1)
                        probRV1Parent += structProb
                    else
                        probRV2Parent += structProb
            }
        }

        questionEntropies[edgeQuestion] = 0.0
        if(probNoEdge != 0.0){
            questionEntropies[edgeQuestion] = questionEntropies[edgeQuestion]!! - probNoEdge * logOfBase(probNoEdge, 2.0)
        }
        if(probRV1Parent != 0.0){
             questionEntropies[edgeQuestion] = questionEntropies[edgeQuestion]!! - probRV1Parent * logOfBase(probRV1Parent, 2.0)
        }
        if(probRV2Parent != 0.0){
             questionEntropies[edgeQuestion] = questionEntropies[edgeQuestion]!! - probRV2Parent * logOfBase(probRV2Parent, 2.0)
        }

        if(questionEntropies[edgeQuestion]!!.isNaN()){
            throw IllegalStateException("How is entropy not a number?")
        }
    }
    val bestQ = questionEntropies.maxBy { it.value }!!
    if(bestQ.value < threshold){
        return null
    }
    return bestQ.key
}

fun <T> unorderedPairs(items : List<T>) =
    items.indices.flatMap { i -> items.drop(i + 1)
        .map { Pair(items[i], it) }
    }

fun approxGraphPosteriorWithEvidence(vocab : List<RandomVariable>, burnIn : Int, numSamples : Int, adTree : ADTree, expertAdvice : List<EdgeRelation>) : Map<BNStruct, Double>{
    val graphStructsCounts = graphStructMCMC(initialStructure(vocab, expertAdvice), burnIn, numSamples, adTree, expertAdvice)
    val totalProbMass = graphStructsCounts.values.sum().toDouble()
    return graphStructsCounts.mapValues { (_, count) -> count.toDouble() / totalProbMass  }
}

fun edgeRelationInStruct(rv1 : RandomVariable, rv2 : RandomVariable, bnStruct : BNStruct) : EdgeRelation{
    if(bnStruct[rv1]!!.contains(rv2)){
        return EdgeRelation.DirectedEdge(rv2, rv1)
    }
    if(bnStruct[rv2]!!.contains(rv1)){
        return EdgeRelation.DirectedEdge(rv1, rv2)
    }

    return EdgeRelation.NoEdge(rv1, rv2)
}

fun hasFeature(bnStruct : BNStruct, desiredEdgeFeature: EdgeRelation) : Boolean {
    val actualRelation = edgeRelationInStruct(desiredEdgeFeature.r1, desiredEdgeFeature.r2, bnStruct)
    if(desiredEdgeFeature is EdgeRelation.NoEdge && actualRelation is EdgeRelation.NoEdge){
        return true
    }
    if(desiredEdgeFeature is EdgeRelation.DirectedEdge && actualRelation is EdgeRelation.DirectedEdge && desiredEdgeFeature.parent == actualRelation.parent){
        return true
    }
    return false
}

fun graphStructMCMC(initialStruct : BNStruct, burnIn : Int, numSamples : Int, adTree : ADTree, expertAdvice : List<EdgeRelation>) : Map<BNStruct, Int>{
    if(!expertAdvice.all { hasFeature(initialStruct, it)}){
        throw IllegalArgumentException("Starting structure must obey expert advice")
    }

    val sampledStructures = HashMap<BNStruct, Int>()
    var currentStruct = initialStruct
    for(t in 1..(burnIn + numSamples)){
        val neighbours = validNeighbors(currentStruct, 5)
            .filter { (bnStruct, _)  -> expertAdvice.all { edgeFeature -> hasFeature(bnStruct, edgeFeature) }}

        if(neighbours.isNotEmpty()){
            val randNeighbourIndex = (Math.random() * neighbours.size).toInt()
            val candidateNeighbour = neighbours[randNeighbourIndex]
            val newNeighbours = validNeighbors(candidateNeighbour.bnStruct, 5)
                .filter { (bnStruct, _)  -> expertAdvice.all { edgeFeature -> hasFeature(bnStruct, edgeFeature) }}

            var candNodeScore = 0.0
            var oldNodeScore = 0.0
            for((rv, differentParents) in candidateNeighbour.differingNodes){
                val candCounts = makeWeightedCountTable(listOf(rv) + differentParents, adTree)
                val oldCounts = makeWeightedCountTable(listOf(rv) + currentStruct[rv]!!, adTree)
                candNodeScore += BDeuNodeScore(rv, differentParents, candCounts, 1.0)
                oldNodeScore += BDeuNodeScore(rv, currentStruct[rv]!!, oldCounts, 1.0)
            }

            val rejectionThreshold = Math.exp(candNodeScore - oldNodeScore) * (neighbours.size.toDouble() / newNeighbours.size.toDouble())
            if(Math.random() < minOf(1.0, rejectionThreshold)){
                //Accept proposal
                currentStruct = candidateNeighbour.bnStruct
            }
        }

        if(t > burnIn){
            sampledStructures[currentStruct] = (sampledStructures[currentStruct] ?: 0) + 1
        }
    }

    if(sampledStructures.values.sum() != numSamples){
        throw IllegalStateException("You don't have the amount of samples that you asked for")
    }

    return sampledStructures
}

fun shotgunStochasticSearch(vocab : List<RandomVariable>, adTree: ADTree, mustHaveFeatures: List<EdgeRelation>) : Map<BNStruct, Double>{
    val graphScores = HashMap<BNStruct, Double>()
    val numIterations = 50 * Math.log(vocab.size.toDouble()).toInt()
    val numInnerIterations = 50
    val initialStruct = initialStructure(vocab, mustHaveFeatures)
    graphScores[initialStruct] = BDeuScore(initialStruct, emptyMap(), sufficientStats(initialStruct, adTree), 1.0)

    for(i in 1..numIterations){
        var currentStruct = initialStruct
        // Stochastically adding edges in starting from empty struct
        for(rv1 in shuffle(vocab)){
            for(rv2 in shuffle(vocab)){
                if(rv1 == rv2) continue
                val (newStruct, newScore) = randomAdd(currentStruct, graphScores[currentStruct]!!, rv1, rv2, adTree, mustHaveFeatures)
                graphScores[newStruct] = newScore
                currentStruct = newStruct
            }
        }
        // Adds, reverses and removes
        for(j in 1..numInnerIterations){
            //println("\tj = $j")
            for(rv1 in shuffle(vocab)){
                for(rv2 in shuffle(vocab)){
                    if(rv1 == rv2) continue
                    // This needs conditioned on which way around the parenthood goes
                    val (newStruct, newScore) = if(currentStruct[rv1]!!.contains(rv2))
                        randomReverseRemove(currentStruct, graphScores[currentStruct]!!, rv1, rv2, adTree, mustHaveFeatures)
                    else if(currentStruct[rv2]!!.contains(rv2))
                        randomReverseRemove(currentStruct, graphScores[currentStruct]!!, rv2, rv1, adTree, mustHaveFeatures)
                    else
                        randomAdd(currentStruct, graphScores[currentStruct]!!, rv1, rv2, adTree, mustHaveFeatures)

                    graphScores[newStruct] = newScore
                    currentStruct = newStruct
                }
            }
        }
    }

    val totalLnMass = sumInnerLnProbs(graphScores.values.toList())
    val graphPosterior = graphScores.mapValues { Math.exp(it.value - totalLnMass) }
    val topEntries = graphPosterior.entries.sortedByDescending { it.value }.take(5000)
    val topEntriesMass = topEntries.sumByDouble{it.value}
    return topEntries.associate { Pair(it.key, it.value / topEntriesMass) }
}

fun randomReverseRemove(currentStruct : BNStruct, currentStructScore : Double, child: RandomVariable, parent: RandomVariable, adTree: ADTree, mustHaveFeatures: List<EdgeRelation>) : Pair<BNStruct, Double>{
    val parentRemovedStruct = HashMap(currentStruct)
    parentRemovedStruct[child] = parentRemovedStruct[child]!! - parent
    val parentRemovalScore = currentStructScore -
        BDeuNodeScore(child, currentStruct[child]!!, makeWeightedCountTable(listOf(child) + currentStruct[child]!!, adTree), 1.0) +
        BDeuNodeScore(child, parentRemovedStruct[child]!!, makeWeightedCountTable(listOf(child) + parentRemovedStruct[child]!!, adTree), 1.0)

    val parentReversedStruct = HashMap(parentRemovedStruct)
    parentReversedStruct[parent] = parentReversedStruct[parent]!! + child
    val parentReversalScore = parentRemovalScore -
        BDeuNodeScore(parent, currentStruct[parent]!!, makeWeightedCountTable(listOf(parent) + currentStruct[parent]!!, adTree), 1.0) +
        BDeuNodeScore(parent, currentStruct[parent]!! + child, makeWeightedCountTable(listOf(parent) + parentReversedStruct[parent]!!, adTree), 1.0)

    val scores = arrayListOf(currentStructScore, parentRemovalScore, parentReversalScore)

    if(!mustHaveFeatures.all { hasFeature(parentRemovedStruct, it)}){
        scores[1] = Math.log(0.0)
    }
    if(pathExists(parent, child, parentRemovedStruct) || !mustHaveFeatures.all { hasFeature(parentReversedStruct, it) }){
        scores[2] = Math.log(0.0)
    }

    val lnTotalMass = sumInnerLnProbs(scores)
    val normedProbs = scores.map { Math.exp(it - lnTotalMass) }

    val randomVal = Math.random()
    if(randomVal < normedProbs[0]){
        return Pair(currentStruct, currentStructScore)
    }
    else if(randomVal < normedProbs[0] + normedProbs[1]){
        return Pair(parentRemovedStruct, parentRemovalScore)
    }
    else{
        return Pair(parentReversedStruct, parentReversalScore)
    }
}

fun randomAdd(currentStruct : BNStruct, currentStructScore : Double, child: RandomVariable, potentialParent: RandomVariable, adTree: ADTree, mustHaveFeatures: List<EdgeRelation>) : Pair<BNStruct, Double>{
    if(pathExists(child, potentialParent, currentStruct)){
        return Pair(currentStruct, currentStructScore)
    }
    val oldParents = currentStruct[child]!!
    val newParents = currentStruct[child]!! + potentialParent

    val newStruct = HashMap(currentStruct)
    newStruct[child] = newParents

    if(!mustHaveFeatures.all{hasFeature(newStruct, it)}){
        return Pair(currentStruct, currentStructScore)
    }

    val oldNodeScore = BDeuNodeScore(child, oldParents, makeWeightedCountTable(listOf(child) + oldParents, adTree), 1.0)
    val newNodeScore = BDeuNodeScore(child, newParents, makeWeightedCountTable(listOf(child) + newParents, adTree) , 1.0)
    val parentAddedScore = currentStructScore - oldNodeScore + newNodeScore

    val scoreProportion = Math.exp(currentStructScore - parentAddedScore)

    if(Math.random() < 1 - 1.0 / (1 + scoreProportion)){
        return Pair(currentStruct, currentStructScore)
    }
    else{
        return Pair(newStruct, parentAddedScore)
    }
}