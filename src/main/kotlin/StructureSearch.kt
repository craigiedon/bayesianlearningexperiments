import org.apache.commons.math3.special.Gamma
import java.util.*

data class Neighbour(val bnStruct : BNStruct, val differingNodes: List<Pair<RandomVariable, Set<RandomVariable>>>)

fun greedyStructureSearch(vocab : List<RandomVariable>,
                          data : List<RVAssignment>,
                          priorEquivSampleSize: Double,
                          mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(),
                          mustBeNoEdges: List<Pair<RandomVariable, RandomVariable>> = emptyList(),
                          parentSizeRestriction: Int = 10000) =
    greedyStructureSearch(vocab, buildADTree(vocab, data, ADBuildParams(10, 500, 250, 0.5, parentSizeRestriction + 1)), priorEquivSampleSize, mustHaveParents, mustBeNoEdges, parentSizeRestriction)

fun greedyStructureSearch(vocab: List<RandomVariable>,
                          adTree : ADTree,
                          priorEquivSampleSize: Double,
                          mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(),
                          mustBeNoEdges: List<Pair<RandomVariable, RandomVariable>> = emptyList(),
                          parentSizeRestriction: Int = 10000) =
        greedyStructureSearch(initialStructure(vocab, mustHaveParents), adTree, priorEquivSampleSize, mustHaveParents, mustBeNoEdges, parentSizeRestriction)
fun greedyStructureSearch(startingStructure : BNStruct,
                          adTree : ADTree,
                          priorEquivSampleSize : Double,
                          mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(),
                          mustBeNoEdges : List<Pair<RandomVariable, RandomVariable>> = emptyList(),
                          parentSizeRestriction : Int = 10000) : BNStruct{
    var prevStats = sufficientStats(listOf(startingStructure), adTree, emptyMap())
    val startingScore = BDeuScore(startingStructure, emptyMap(), prevStats, priorEquivSampleSize, mustHaveParents)
    if(startingScore == Math.log(0.0)){
        throw IllegalArgumentException("Starting BN must be a legal model")
    }

    var localBest = Pair(startingStructure, startingScore)
    var globalBest = localBest
    val tabuList = LinkedHashSet<BNStruct>()
    var globalImprovementFailures = 0
    var globalImprovements = 0

    do {
        val neighbors = validNeighbors(localBest.first, parentSizeRestriction).filter{!tabuList.contains(it.bnStruct)}
        if(neighbors.isEmpty()){
            break
        }

        prevStats = sufficientStats(neighbors.map{ it.bnStruct }+ localBest.first, adTree, prevStats)
        val cachedNodeScores = updateScoreCache(emptyMap(), localBest.first, prevStats, priorEquivSampleSize)
        val neighborsAndScores = neighbors.map {
            Pair(it, BDeuScore(it.bnStruct, cachedNodeScores, prevStats, priorEquivSampleSize, mustHaveParents, mustBeNoEdges))
        }

        val localBestNeighbourAndScore = neighborsAndScores.maxBy { it.second }!!
        localBest = Pair(localBestNeighbourAndScore.first.bnStruct, localBestNeighbourAndScore.second)
        tabuList.add(localBestNeighbourAndScore.first.bnStruct)

        if(localBest.second > globalBest.second){
            globalBest = localBest
            globalImprovementFailures = 0
            globalImprovements++
        }
        else{
            globalImprovementFailures += 1
        }
    } while(globalImprovementFailures < 100)

    println("Moves : $globalImprovements")
    return globalBest.first
}

fun statConstrainedStructureSearch(initialStruct : BNStruct, availableStats : SufficientStats, priorEquivSampleSize : Double,
                                   mustHaveParents : Map<RandomVariable, Set<RandomVariable>>, parentSizeRestriction : Int) : BNStruct{

    var stepsTaken = 0
    fun greedyStepRec(currentStruct : BNStruct, prevCachedScores : BDeuScoreCache, globalBest : Pair<BNStruct, Double>, improvementFailures : Int, tabuList : Set<BNStruct>) : BNStruct{
        stepsTaken++
        val validNeighbours = validNeighbors(currentStruct, parentSizeRestriction).filter{ (struct, _) ->
            hasRequiredStats(struct, availableStats) && hasRequiredParents(struct, mustHaveParents) && !tabuList.contains(struct)
        }

        if (validNeighbours.isEmpty())
            return globalBest.first

        val cachedScores = validNeighbours.fold(prevCachedScores, {scores, (struct, _) -> updateScoreCache(scores, struct, availableStats, priorEquivSampleSize)})
        val neighboursAndScores = validNeighbours.map { Pair(it, BDeuScore(it.bnStruct, cachedScores, emptyMap(), priorEquivSampleSize)) }
        val (bestNeighbour, bestScore) = neighboursAndScores.maxBy { it.second }!!
        return when{
            bestScore > globalBest.second -> greedyStepRec(bestNeighbour.bnStruct, cachedScores, Pair(bestNeighbour.bnStruct, bestScore), 0, tabuList + bestNeighbour.bnStruct)
            improvementFailures < 10 -> greedyStepRec(bestNeighbour.bnStruct, cachedScores, globalBest, improvementFailures + 1, tabuList + bestNeighbour.bnStruct)
            else -> globalBest.first
        }
    }

    val cachedScores = updateScoreCache(emptyMap(), initialStruct, availableStats, priorEquivSampleSize)
    val initialScore = BDeuScore(initialStruct, cachedScores, availableStats, priorEquivSampleSize, mustHaveParents = mustHaveParents)
    if(initialScore == Math.log(0.0))
        throw IllegalArgumentException("Starting BN must be a legal model")

    val result = greedyStepRec(initialStruct, cachedScores, Pair(initialStruct, initialScore), 0, HashSet())
    println("Stat constrained steps taken: ${stepsTaken - 10}")
    return result
}

fun updateScoreCache(cachedScores : BDeuScoreCache, newStruct : BNStruct, availableStats: SufficientStats, priorEquivSampleSize: Double) : BDeuScoreCache{
    val cacheMisses = newStruct.entries.map { it.toPair() }.filter{ !cachedScores.containsKey(it) }
    val newScores = cacheMisses.associate { Pair(it, BDeuNodeScore(it.first, it.second, availableStats[it.second + it.first]!!, priorEquivSampleSize)) }
    return cachedScores + newScores
}

fun hasRequiredStats(bnStruct: BNStruct, availableStats : SufficientStats) =
    bnStruct.all { (child, parents) -> availableStats.containsKey(parents + child) }

fun hasRequiredParents(bnStruct: BNStruct, mustHaveParents: Map<RandomVariable, Set<RandomVariable>>) =
    mustHaveParents.all { (rv, requiredParents) -> bnStruct[rv]!!.containsAll(requiredParents) }

fun initialStructure(vocab : Collection<RandomVariable>, mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap()) : Map<RandomVariable, Set<RandomVariable>>{
    return vocab.associate { Pair(it, mustHaveParents.getOrElse(it, {emptySet()}))}
}

fun initialStructure(vocab: Collection<RandomVariable>, mandatoryFeatures : List<EdgeRelation>) : Map<RandomVariable, Set<RandomVariable>>{
    return initialStructure(vocab, edgeRelsToParentStructure(mandatoryFeatures))
}

fun edgeRelsToNonEdges(edgeRels : List<EdgeRelation>) =
    edgeRels.filterIsInstance<EdgeRelation.NoEdge>().map { Pair(it.r1, it.r2) }

fun edgeRelsToParentStructure(edgeRels : List<EdgeRelation>) =
    edgeRels.filterIsInstance<EdgeRelation.DirectedEdge>()
        .groupBy { it.child }
        .mapValues { (_, edgeRel) -> HashSet(edgeRel.map { it.parent }) }


typealias BDeuScoreCache = Map<Pair<RandomVariable, Set<RandomVariable>>, Double>
fun BDeuNodeScore(child : RandomVariable, parents : Set<RandomVariable>, counts : CountTable, priorSampleSize: Double) : Double{
    var score = 0.0
    for(parentAssignment in allAssignments(parents.toList())){

        val betaNumeratorArgs = ArrayList<Double>()
        val betaDenominatorArgs = ArrayList<Double>()
        for(childVal in 0 until child.domainSize){
            val alpha = priorSampleSize / (counts.counts.size)
            val familyAssignment = parentAssignment + Pair(child, childVal)
            val assignmentCount = counts.counts[assignmentToIndex(familyAssignment, counts.scope)]
            betaNumeratorArgs.add(assignmentCount + alpha)
            betaDenominatorArgs.add(alpha)
        }

        val logBetaNumerator = logBeta(betaNumeratorArgs)
        val logBetaDenominator = logBeta(betaDenominatorArgs)

        score += logBetaNumerator - logBetaDenominator
    }

    if(score.isInfinite()){
        println("Infinite node score? What happened?")
    }

    return score
}

fun BDeuNodeScore(child : RandomVariable, parents : Set<RandomVariable>, counts : CountTable, auxParamPrior : Factor, priorSampleSize: Double) : Double{
    var score = 0.0
    for(parentAssignment in allAssignments(parents.toList())){

        val betaNumeratorArgs = ArrayList<Double>()
        val betaDenominatorArgs = ArrayList<Double>()
        for(childVal in 0 until child.domainSize){
            val familyAssignment = parentAssignment + Pair(child, childVal)
            val count = counts.getCount(familyAssignment)
            val pseudoCount = auxParamPrior.getValue(familyAssignment) * priorSampleSize
            betaNumeratorArgs.add(count + pseudoCount)
            betaDenominatorArgs.add(pseudoCount)
        }

        val logBetaNumerator = logBeta(betaNumeratorArgs)
        val logBetaDenominator = logBeta(betaDenominatorArgs)

        score += logBetaNumerator - logBetaDenominator
    }

    if(score.isInfinite()){
        println("Infinite node score? What happened?")
    }

    return score
}

fun BDeuScore(currentStructure : Map<RandomVariable, Set<RandomVariable>>, cachedScores : Map<Pair<RandomVariable, Set<RandomVariable>>, Double>,
              sufficientStats : Map<Set<RandomVariable>, CountTable>,
              priorSampleSize: Double, mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(),
              mustBeNoEdges: List<Pair<RandomVariable, RandomVariable>> = emptyList()) : Double{
    val logPrior = 0.0 // Could do something more interesting here like penalise num of edges
    var logLikelihood = 0.0

    if (!mustHaveParents.all { currentStructure[it.key]!!.containsAll(it.value) }){
        return Math.log(0.0)
    }

    if(mustBeNoEdges.any { currentStructure[it.first]!!.contains(it.second) || currentStructure[it.second]!!.contains(it.first) }){
        return Math.log(0.0)
    }

    for((child, parents) in currentStructure){
        val cachedScore = cachedScores[Pair(child,parents)]
        if(cachedScore != null){
            logLikelihood += cachedScore
        }
        else{
            logLikelihood += BDeuNodeScore(child, parents, sufficientStats[parents + child]!!, priorSampleSize)
        }
    }

    if(logLikelihood.isInfinite()){
        println("Inifinte score, how?")
    }

    return logPrior + logLikelihood
}

fun BDeuScore(currentStructure : Map<RandomVariable, Set<RandomVariable>>, data : List<RVAssignment>, equivalentSampleSize : Double) : Double{
    val logPrior = 0.0
    var logLikelihood = 0.0

    for((child, parents) in currentStructure){
        val parentAssignments = allAssignments(parents.toList())
        for(parentAssignment in parentAssignments){
            val betaNumeratorArgs = ArrayList<Double>()
            val betaDenominatorArgs = ArrayList<Double>()

            for(childVal in 0..child.domainSize-1){
                val alpha = equivalentSampleSize / (child.domainSize * parentAssignments.size)
                val count = data
                    .count { sample -> sample[child] == childVal && sample.filterKeys { parents.contains(it) } == parentAssignment }

                betaNumeratorArgs.add(count + alpha)
                betaDenominatorArgs.add(alpha)
            }

            val logBetaNumerator = logBeta(betaNumeratorArgs)
            val logBetaDenominator = logBeta(betaDenominatorArgs)

            logLikelihood += (logBetaNumerator - logBetaDenominator)
        }
    }

    return logPrior + logLikelihood
}

fun logBeta(values: List<Double>): Double {
    val sumOfLogGammas = values.map{ Gamma.logGamma(it)}.sum()
    val logGammaOfSum = Gamma.logGamma(values.sum())
    return sumOfLogGammas - logGammaOfSum
}

fun validNeighbors(currentStructure: BNStruct, parentSizeRestriction : Int = 10000) : List<Neighbour>{
    val neighboringStructures = ArrayList<Neighbour>()

    for((rv, parents) in currentStructure){
        // Remove edges (always fine for BNs)
        for(removableParent in parents){
            val changedFamilies =  arrayListOf(Pair(rv, parents.filter { p -> p != removableParent }.toSet()))
            val neighborRemovedStruct = currentStructure + changedFamilies
            neighboringStructures.add(Neighbour(neighborRemovedStruct, changedFamilies))

            // Try reversing edge (Watch for cycles)
            if(!pathExists(rv, removableParent, neighborRemovedStruct) && currentStructure[removableParent]!!.size < parentSizeRestriction){
                changedFamilies.add(Pair(removableParent, currentStructure[removableParent]!! + rv))
                val neighborReversedStruct = neighborRemovedStruct + changedFamilies
                neighboringStructures.add(Neighbour(neighborReversedStruct, changedFamilies))
            }
        }

        // Add edges
        if(parents.size < parentSizeRestriction){ // Don't add for any parents
            //Watch for cycles
            for(additionalParent in currentStructure.keys.filter { !parents.contains(it) && !pathExists(it, rv, currentStructure) }){
                val changedFamilies = listOf(Pair(rv, (parents + additionalParent)))
                val neighborAddition = currentStructure + changedFamilies
                neighboringStructures.add(Neighbour(neighborAddition, changedFamilies))
            }
        }
    }

    return neighboringStructures
}

fun pathExists(from : RandomVariable, to: RandomVariable, bnStruct: BNStruct) =
    pathExistsDirGraph(to, from, bnStruct)

fun main(args : Array<String>){
    val trueBN = loadJsonBN("graphGeneration/asia.json")
    val data = generateSamples(trueBN, 10000)

    val reducedVocab = trueBN.vocab.filter{it.name != "either"}
    println(reducedVocab)
    val adTree = buildADTree(reducedVocab, data, ADBuildParams(5, 200, 100, 0.6, 5))

    val learnedBN = greedyStructureSearch(reducedVocab, adTree, 1.0)
    println(learnedBN)
}