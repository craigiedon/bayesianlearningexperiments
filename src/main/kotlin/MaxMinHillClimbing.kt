import com.google.common.collect.Sets
import org.apache.commons.math3.distribution.ChiSquaredDistribution
import java.util.*

fun main(args : Array<String>){
    val A = RandomVariable("A", 2)
    val B = RandomVariable("B", 2)
    val C = RandomVariable("C", 2)
    val D = RandomVariable("D", 2)
    val E = RandomVariable("E", 2)
    val F = RandomVariable("F", 2)
    val T = RandomVariable("T", 2)
    val H = RandomVariable("H", 2)
    val I = RandomVariable("I", 2)
    val J = RandomVariable("J", 2)
    val K = RandomVariable("K", 2)
    val L = RandomVariable("L", 2)

    val bn = BayesNet(mapOf(
            A to BNode(A, emptyList(), listOf(0.4, 0.6)),
            B to BNode(B, emptyList(), listOf(0.5, 0.5)),
            C to BNode(C, listOf(A), listOf(0.7, 0.3, 0.2, 0.8)),
            D to BNode(D, listOf(A), listOf(0.1, 0.9, 0.7, 0.3)),
            E to BNode(E, listOf(B), listOf(0.6, 0.4, 0.8, 0.2)),
            F to BNode(F, listOf(B, C), listOf(0.3, 0.7, 0.5, 0.5, 0.1, 0.9, 0.8, 0.2)),
            T to BNode(T, listOf(C,D), listOf(0.1, 0.9, 0.8, 0.2, 0.3, 0.7, 0.6, 0.4)),
            H to BNode(H, listOf(D), listOf(0.6, 0.4, 0.8, 0.2)),
            I to BNode(I, listOf(T,H), listOf(0.1, 0.9, 0.8, 0.2, 0.5, 0.5, 0.3, 0.7)),
            J to BNode(J, listOf(H), listOf(0.7, 0.3, 0.2, 0.8)),
            K to BNode(K, listOf(I), listOf(0.6, 0.4, 0.8, 0.2)),
            L to BNode(L, listOf(I,J), listOf(0.1, 0.9, 0.8, 0.2, 0.3, 0.7, 0.6, 0.4))
    ))

    val samples = generateSamples(bn, 10000)
    val cpcs = MMPC(bn, samples)

    println("Final CPCs: $cpcs")
}

fun MMPCSimple(bn : BayesNet, target : RandomVariable, data : List<RVAssignment>) : Set<RandomVariable>{
    val candidateParentChildren = HashSet<RandomVariable>()
    // Forward Phase
    val potentialCPCs = HashSet(bn.nodes.keys - target)
    while(potentialCPCs.isNotEmpty()){
        //Actually here you should probably be crossing out zero'd candidates as you go...
        val (maxRV, maxAssocVal, zeroVars) = maxMinHeuristic(target, candidateParentChildren, potentialCPCs , data)
        if (maxAssocVal != 0.0){
            candidateParentChildren.add(maxRV)
            potentialCPCs.remove(maxRV)
        }
        potentialCPCs.removeAll(zeroVars)
    }

    println("Forward Phase: $target - $candidateParentChildren")

    // Backward Phase
    val candIterator = candidateParentChildren.iterator()
    while (candIterator.hasNext()){
        val candidate  = candIterator.next()
        // It seems like this phase is exponential in the size of CPC?
        val cpcsWithoutCandidate = candidateParentChildren - candidate
        if(Sets.powerSet(cpcsWithoutCandidate).any { association(candidate, target, it, data) == 0.0 }){
            candIterator.remove()
        }
    }

    println("Backward Phase: $target - $candidateParentChildren")

    return candidateParentChildren
}

fun MMPC(bn : BayesNet, data : List<RVAssignment>): Map<RandomVariable, Set<RandomVariable>>{
    val naiveCPCs = bn.nodes.keys.associate { Pair(it, MMPCSimple(bn, it, data)) }
    return naiveCPCs.mapValues { target -> target.value.filter { rv -> naiveCPCs[rv]!!.contains(target.key) }.toSet() }
}

fun maxMinHeuristic(target : RandomVariable, candidateParentChildren : Set<RandomVariable>, potentialCPCs: Set<RandomVariable>, data : List<RVAssignment>) : MaxMinResult {
    val minAssocs = (potentialCPCs).map { Pair(it, minAssoc(it, target, candidateParentChildren, data)) }
    val maxMinAssoc = minAssocs.maxBy { it.second }!!
    return MaxMinResult(maxMinAssoc.first, maxMinAssoc.second, minAssocs.filter { it.second == 0.0 }.map { it.first })
}

data class MaxMinResult(val maxVar: RandomVariable, val maxVal: Double, val zeroVars : List<RandomVariable>)

fun minAssoc(candidate : RandomVariable, target: RandomVariable, candidateParentChildren: Set<RandomVariable>, data : List<RVAssignment>) : Double {
    return Sets.powerSet(candidateParentChildren - candidate - target).map { cpc -> association(candidate, target, cpc, data) }.min()!!
}

fun association(candidate: RandomVariable, target: RandomVariable, cpcSubset: Set<RandomVariable>, data : List<RVAssignment>) : Double{
    // Note, we should assume independence for any subset that will fall below the sample unweightedCount
    val orderedSubset = cpcSubset.toList()
    val jointVars = listOf(candidate, target) + orderedSubset

    val jointCounts = IntArray(numAssignments(jointVars))
    val subsetCounts = IntArray(numAssignments(orderedSubset))
    val candCounts = IntArray(numAssignments(listOf(candidate) + orderedSubset))
    val targetCounts = IntArray(numAssignments(listOf(target)  + orderedSubset))

    for(assignment in data){
        jointCounts[assignmentToIndex(assignment, jointVars)] += 1
        subsetCounts[assignmentToIndex(assignment, orderedSubset)] += 1
        candCounts[assignmentToIndex(assignment, listOf(candidate) + orderedSubset)] += 1
        targetCounts[assignmentToIndex(assignment, listOf(target) + orderedSubset)] += 1
    }

    var halfG = 0.0

    for(fullInd in jointCounts.indices){
        val subInd = convertIndex(fullInd, jointVars, orderedSubset)
        val candInd = convertIndex(fullInd, jointVars, listOf(candidate) + orderedSubset)
        val targetInd = convertIndex(fullInd, jointVars, listOf(target) + orderedSubset)

        val jointCount = jointCounts[fullInd]
        if(jointCount > 0){
            //println("Candidate: $candidate, Subset: $orderedSubset, target: $target")
            val subsetCount = subsetCounts[subInd]
            val candCount = candCounts[candInd]
            val targetCount = targetCounts[targetInd]

            val observedCounts = Math.log(jointCount.toDouble())
            val expectedCountsNullHyp = Math.log(candCount * targetCount.toDouble() / subsetCount)
            halfG += jointCount * (observedCounts - expectedCountsNullHyp)
        }
    }

    val gStat = 2 * halfG

    val chiDist = ChiSquaredDistribution((target.domainSize - 1) * (candidate.domainSize - 1) * numAssignments(orderedSubset).toDouble())
    val cumProb = chiDist.cumulativeProbability(gStat)
    //println("G-stat: %.3f\nCum-Prob: %.3f\nP-val: %.3f\n".format(gStat, cumProb, 1 - cumProb))
    return if (cumProb > 0.95) cumProb else 0.0
}

