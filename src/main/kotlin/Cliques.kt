import java.util.*

class CliqueNode(val scope: Set<RandomVariable>, val neighbors : ArrayList<CliqueNode>){
    override fun toString(): String = "$scope -> ${neighbors.map { it.scope }}"
}
class CliqueTree(val nodes: List<CliqueNode>){
    override fun toString() = nodes.joinToString("\n")
}
class Message(val sender: CliqueNode, val receiver : CliqueNode, val factor : Factor){
    override fun toString() = "from: $sender, to: $receiver, $factor"
}


fun convertToCliqueForest(factors : List<Factor>) : List<CliqueTree>{
    return maxSpanningForest(cliquesFromVE(factors))
}

fun convertToCliqueForest(bn : BayesNet) = convertToCliqueForest(extractFactors(bn))

fun cliquesFromVE(factors : List<Factor>) : List<Set<RandomVariable>>{
    val cliques = ArrayList<Set<RandomVariable>>()
    var remainingRVs : List<RandomVariable> = factors.flatMap { it.scope }.distinct()
    var remainingFactorScopes = factors.map { it.scope.toSet() }

    while(remainingRVs.isNotEmpty()){
        val bestRV : RandomVariable = minResultingDegreeVar(remainingRVs, remainingFactorScopes)
        remainingRVs -= bestRV

        val relevantFactors = remainingFactorScopes.filter { it.contains(bestRV) }
        val clique : Set<RandomVariable> = relevantFactors.flatten().toSet()
        if(!cliques.any { it.containsAll(clique) }){
            cliques.add(clique)
        }
        remainingFactorScopes -= relevantFactors
        remainingFactorScopes += listOf((clique - bestRV))
    }

    if(remainingFactorScopes.flatten().isNotEmpty()){
        throw IllegalStateException("All factors should have been eliminated, what is going on?")
    }

    return cliques
}
fun cliquesFromVE(bn : BayesNet) : List<Set<RandomVariable>> =
    cliquesFromVE(extractFactors(bn))

fun cliquify(chordalGraph : Graph, elimOrdMap : Map<RandomVariable, Int>) : List<Set<RandomVariable>>{
    val elimOrd = elimOrdMap.keys.sortedBy { elimOrdMap[it] }
    val cliques = ArrayList<Set<RandomVariable>>()

    for(rv in elimOrd){
        val clique = setOf(rv) + chordalGraph.nodes[rv]!!.neighbors.map { it.rv }.filter { elimOrdMap[it]!! > elimOrdMap[rv]!! }.toSet()

        if(!cliques.any {it.containsAll(clique)}){
            cliques.add(clique)
        }
    }

    return cliques
}

fun maxSpanningForest(cliques : List<Set<RandomVariable>>) : List<CliqueTree>{
    val cliqueEdges = ArrayList<Pair<Int,Int>>()
    for(i in cliques.indices){
        for(j in (i+1..cliques.size - 1)){
            val intersection = cliques[i].toSet() intersect cliques[j].toSet()
            if(intersection.isNotEmpty()){
                cliqueEdges.add(Pair(i,j))
            }
        }
    }

    cliqueEdges.sortByDescending { (cliques[it.first] intersect cliques[it.second]).size }
    val disjointSets = ArrayList(cliques.indices.map { setOf(it) })
    val maxTreeEdges = ArrayList<Pair<Int,Int>>()

    for ((i, j) in cliqueEdges){
        val setI = disjointSets.find { it.contains(i) }!!
        val setJ = disjointSets.find { it.contains(j) }!!
        if(setI != setJ){
            maxTreeEdges.add(Pair(i,j))
            disjointSets.remove(setI)
            disjointSets.remove(setJ)
            disjointSets.add(setI union setJ)
        }
    }

    val cliqueNodes = cliques.map { CliqueNode(it, ArrayList()) }
    for((i,j) in maxTreeEdges){
        cliqueNodes[i].neighbors.add(cliqueNodes[j])
        cliqueNodes[j].neighbors.add(cliqueNodes[i])
    }

    val cliqueTrees = disjointSets.map {
        CliqueTree(cliqueNodes.filterIndexed { i, _ -> it.contains(i) })
    }

    return cliqueTrees
}
