import DNUnawareness.*
import Utils.*
import com.google.ortools.sat.*
import java.util.*

data class UtilityBN(val beliefNetwork : BayesNet, val rewardFunctions : List<RewardTable>)

fun generateDecisionNetwork(numActions : Int, numBeliefs : Int, numOutcomes : Int, rewardDomSize : Int, maxParents : Int, maxChildren: Int) : DecisionNetwork{
    val beliefs = (1..numBeliefs).map{RandomVariable("B$it", 2, VarType.BELIEF)}
    val outcomes = (1..numOutcomes).map{RandomVariable("O$it", 2, VarType.OUTCOME)}
    val actions = (1..numActions).map{RandomVariable("A$it", 2, VarType.ACTION)}
    val precision = 2

    // Enforce that there are more outcome vars in reward domain than belief variables so agent actually has something to do...
    val numOutcomeRewards = random((rewardDomSize / 2) + 1, rewardDomSize + 1)
    val numBelRewards = rewardDomSize - numOutcomeRewards
    val rewardDomain = sampleNoReplacement(outcomes, numOutcomeRewards) + sampleNoReplacement(beliefs, numBelRewards)
    val dnStructure = utilityBNStruct(actions, beliefs, outcomes, rewardDomain, maxParents, maxChildren)
    val rewardFunc = RVTable(rewardDomain, randomList(0.0, 1.0, numAssignments(rewardDomain), precision))
    val bn = randomParams(dnStructure, precision)
    return DecisionNetwork(rewardFunc, bn.nodes.values , actions)
}

/*
fun genUtilityBN(actions : List<RandomVariable>, beliefs : List<RandomVariable>, outcomes : List<RandomVariable>, rewardDomainSize : Int, maxFactorSize : Int, maxTreeWidth : Int, maxParents : Int) : UtilityBN {
    val rewardDomain = sampleNoReplacement(beliefs + outcomes, rewardDomainSize)
    val dnStructure = utilityBNStruct(actions, beliefs, outcomes, rewardDomain, maxTreeWidth, maxParents)
    val factoredRewardDom = randomFactorScopes(rewardDomain, maxFactorSize)
    val rewardFunctions = factoredRewardDom.map{ RewardTable (it, randomList(0.0, 10.0, numAssignments((it))))}
    val beliefNetwork = randomParams(dnStructure)

    return UtilityBN(beliefNetwork, rewardFunctions)
}
*/

/*
fun genConnectedDAG(actions : List<RandomVariable>, beliefs : List<RandomVariable>, outcomes: List<RandomVariable>, rewardDomain: List<RandomVariable>, maxParents: Int, maxChildren : Int) : BNStruct {
    System.loadLibrary("jniortools")
    val s = Solver("DN Generator")
    val nonRewardDom = (beliefs + outcomes).filter { it !in rewardDomain }
    val ordering = actions + nonRewardDom + rewardDomain

    /* Enforce:
        - Max Parent Size
        - Beliefs cant have outcome parents
        - outcomes must have at least one parent
        - Ordering obeyed
     */

    val parentSetVars = HashMap<RandomVariable, Map<PSet, IntVar>>()
    for(rv in beliefs){
        val orderPos = ordering.indexOf(rv)
        val pSets = powerSet(beliefs.filter { ordering.indexOf(it) < orderPos }, 0, maxParents).map { it.toSet() }
        parentSetVars[rv] = pSets.associateWith { s.makeBoolVar("$it -> $rv") }
    }

    for(rv in outcomes){
        val orderPos = ordering.indexOf(rv)
        val pSets = powerSet(ordering.filter { ordering.indexOf(it) < orderPos }, 1, maxParents)
            .map { it.toSet() }
            .filter { pSet -> pSet.any{ it.varType == VarType.ACTION || it.varType == VarType.OUTCOME}}
        parentSetVars[rv] = pSets.associateWith { s.makeBoolVar("$it -> $rv") }
    }


    // Each variable must have exactly one parent set
    for((rv, pMap) in parentSetVars){
        val oneParentSetConstraint = s.makeSumEquality(pMap.values.toTypedArray(), 1)
        s.addConstraint(oneParentSetConstraint)
    }

    // All non-reward domain variables must have at least one child
    for(rv in (actions + nonRewardDom)){
        val relevantPSets = parentSetVars.flatMap { (_, pMap) ->  pMap.filter { (pSet, _) -> rv in pSet}.map { it.value }}
        val atLeastOneChildConstraint = s.makeSumGreaterOrEqual(relevantPSets.toTypedArray(), 1)
        s.addConstraint(atLeastOneChildConstraint)
    }

    val allVars = parentSetVars.values.flatMap { it.values }.toTypedArray()
    val decisionBuilder = s.makePhase(allVars, Solver.CHOOSE_RANDOM, Solver.ASSIGN_RANDOM_VALUE)
    s.newSearch(decisionBuilder)

    val structs = ArrayList<BNStruct>()
    while(s.nextSolution() && structs.size < 10){
        // Filter out edges which don't exist
        val struct = parentSetVars.mapValues { (_, pMap) -> pMap.entries.find{ it.value.value() != 0.toLong() }!!.key }
        structs.add(struct)
    }

    s.endSearch()
    s.delete()

    //println(structs.size)
    return structs.random()
}
*/

fun utilityBNStruct(actions : List<RandomVariable>, beliefs : List<RandomVariable>, outcomes : List<RandomVariable>, rewardDomain : List<RandomVariable>, maxTreeWidth : Int, maxParents : Int) : BNStruct {
    System.loadLibrary("jniortools")
    val cpModel = CpModel()
    val nonRewardDom = (beliefs + outcomes).filter { it !in rewardDomain }
    val ordering = actions + nonRewardDom + rewardDomain

    val parentVars = ordering
        .flatMap { rv -> ordering.filter { it != rv }.map { Pair(rv, it) } }
        .associate { (rv, parentRv) -> Pair(Pair(rv, parentRv), cpModel.newBoolVar("P_${parentRv.name}${rv.name}"))}

    println("Num Decision Variables: ${parentVars.size}")

    val yEdges = orderedPairs(ordering).associate { Pair(it, cpModel.newBoolVar("y_${it.first.name}_${it.second.name}")) }
    val zVars = ordering.associate { Pair(it, cpModel.newIntVar(0, ((ordering.size - 1).toLong()), "z_${it.name}")) }


    // Enforce max tree width (3a)
    for(rv in ordering){
        val relevantEdges = yEdges.filterKeys{ it.first == rv }
        cpModel.addLinearSum(relevantEdges.values.toTypedArray(), 0, maxTreeWidth.toLong())
    }

    // Ensure triangulation / moralization elimination ordering is obeyed (3b)
    for((yPair, edgeVar) in yEdges){
        cpModel.addScalProd(
            listOf(edgeVar, zVars[yPair.first], zVars[yPair.second]).toTypedArray(),
            listOf(ordering.size + 1, 1, -1).toIntArray(),
            -1000000,
            ordering.size.toLong())
    }

    // Triangulation (3c)
    for(i in ordering){
        for(j in ordering.filter { it != i }){
            for(k in ordering.filter{it != i && it != j}){
                val yIJ = yEdges[Pair(i, j)]!!
                val yIK = yEdges[Pair(i, k)]!!
                val yJK = yEdges[Pair(j, k)]!!
                val yKJ = yEdges[Pair(k, j)]!!
                cpModel.addScalProd(arrayOf(yIJ, yIK, yJK, yKJ), intArrayOf(1, 1, -1, -1), -10000, 1)
            }
        }
    }


    for(rv in ordering){
        val relevantParentVars = parentVars.filterKeys { it.first == rv }.mapKeys { it.key.second }

            // Max parents
            val limitMaxParents = cpModel.addLinearSum(relevantParentVars.values.toTypedArray(), 0, maxParents.toLong())
            //cpModel.addConstraint(limitMaxParents)

            for((parent, parentMPVar) in relevantParentVars){
                // A variable cant have a parent that comes after it in the ordering 4 (b)
                cpModel.addScalProd(arrayOf(parentMPVar), intArrayOf(ordering.size), -10000, ordering.size.toLong() + ordering.indexOf(rv) - ordering.indexOf(parent))

                // edges described by DAG must be present in moral graph 4 (c)
                cpModel.addScalProd(arrayOf(parentMPVar, yEdges[Pair(rv, parent)], yEdges[Pair(parent,rv)]), intArrayOf(1, -1 ,-1), -1000000, 0)
            }

            // Parents must be moralized 4 (d)
            val parentPairs = unorderedPairs(ordering.filter { it != rv })
            for((p1, p2) in parentPairs){
                cpModel.addScalProd(
                    arrayOf(parentVars[Pair(rv, p1)]!!, parentVars[Pair(rv, p2)]!!, yEdges[Pair(p1,p2)]!!, yEdges[Pair(p2, p1)]!!),
                    intArrayOf(1,1,-1,-1),
                    -100000, 1)
            }
    }

    // Actions can't have any parents
    for(rv in actions){
        val relevantParentVars = parentVars.filterKeys { it.first == rv }.values.toTypedArray()
        cpModel.addLinearSumEqual(relevantParentVars, 0)
    }

    // Belief variables can only contain other belief vars as parents. Again, can be enforced via parent sets given
    for(rv in beliefs){
        val invalidParents = parentVars.filterKeys { (child, parent) -> child == rv  && (parent !in beliefs)}.values.toTypedArray()
        cpModel.addLinearSumEqual(invalidParents, 0)
    }

    // Non-Reward Beliefs must have at least one outcome child (otherwise they are redundant!)
    for(rv in beliefs.filter { it !in rewardDomain }){
        val possibleOutcomeChildren = parentVars.filterKeys { (child, parent) -> parent == rv  && (child !in beliefs)}.values.toTypedArray()
        cpModel.addBoolOr(possibleOutcomeChildren)
    }


    /* Outcomes must have at least one parent that is an action or outcome */
    for(rv in outcomes){
        val relevantParents = parentVars.filterKeys { (child, parent) -> child == rv  && (parent in actions || parent in outcomes)}.values.toTypedArray()
        cpModel.addBoolOr(relevantParents)
    }

    // All variables have at least one child
    for(rv in ordering.filterNot { it in rewardDomain }){
        val relevantParentVars = parentVars.filterKeys { (_, parent) ->  parent == rv }
        cpModel.addBoolOr(relevantParentVars.values.toTypedArray())
    }

    val structs = ArrayList<BNStruct>()
    class DAGSolutionCallback : CpSolverSolutionCallback(){
        override fun onSolutionCallback(){
            val nonZeroEntries = parentVars.filterValues { value(it) != 0.toLong() }
            val struct : MutableMap<RandomVariable, Set<RandomVariable>> = nonZeroEntries.keys
                .groupBy({ it.first } , {it.second})
                .mapValues { it.value.toSet() } as MutableMap
            for(rv in (beliefs + outcomes)){
                if(rv !in struct){
                    struct[rv] = emptySet()
                }
            }
            println(struct)
            println(structs.size)
            structs.add(struct)
            if(structs.size > 1000){
                stopSearch()
            }
        }
    }

    //print("Num constraints ${cpModel.constraints()}")
    val solver = CpSolver()
    val solCall = DAGSolutionCallback()
    solver.searchAllSolutions(cpModel, solCall)
    return structs.random()
}

fun main(){
    /*
    val smallDN = readDN("graphGeneration/smallDN.json")
    val mediumDN = readDN("graphGeneration/mediumDN.json")
    val largeDN = readDN("graphGeneration/largeDN.json")

    val numParamsSmall = smallDN.sortedChanceNodes.sumBy { it.cpt.size }
    val numParamsMedium = mediumDN.sortedChanceNodes.sumBy { it.cpt.size }
    val numParamsLarge = largeDN.sortedChanceNodes.sumBy { it.cpt.size }

    println(numParamsSmall)
    println(numParamsMedium)
    println(numParamsLarge)
    */

    val smallDN = generateDecisionNetwork(4,4,4,3,3,3)
    val mediumDN = generateDecisionNetwork(8,8,8,4,3,3)
    //val largeDN = generateDecisionNetwork(12,12,12,5,3,3)
    saveToJson(DNToDNData(smallDN), "graphs", "smallDN")
    saveToJson(DNToDNData(mediumDN), "graphs", "mediumDN")
    //saveToJson(DNToDNData(largeDN), "graphs", "largeDN")

    println("Done")
}