/*
fun maxExpectedUtility(decisionNetwork : DNUnawareness.DecisionNetwork) : Pair<RVAssignment, Double>{
    // Bucket elimination algorithm
    return bestAction
}
*/

/*
    So first experiment I should chart up is the one I've already done which just looks to introduce a new variable every time agent notices that partial assignment has gone awry.
    Can you get this done before meeting tomorrow?

    Next step after this is to actually get an agent making decisions.
    If the agent is making decisions, then it can receive better action advice, incorporate this into its reward, and also point out contradictions in advice.
    We don't need to actually add in the learning of structure for the moment (or possibly even parameter learning).

    Once we have demonstrated these things, the system is basically back to where it was in the previous paper (plus factored reward, minus probability learning),
    We can then from here choose to create more sophisticated implicatures for preferential independence / general preferences, and see if these make a difference to our performance.
    Will need to come up with a sensible deprecation strategy. Formulate the easy monotonic version of each piece of advice, and the possibly more difficult defeasible version.
    How do we check when one of these constraints is violated? How do we assign blame? What do we do when this happens? In some cases it is clear that a defeasible inference has
    been violated, but in others there could be multiple reasons we can't currently construct a sufficient reward function.

 */

/*
fun skeletonOfOverallOperation(){
    dn = generateDN
    initialVocab
    generateSamples
}
*/