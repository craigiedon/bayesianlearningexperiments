import Utils.logOfBase
import Utils.randomCPT
import Utils.randomParams
import java.util.*

/*
fun testRegularFriedman(trueBN : BayesNet){
    LearningLog.trueBN = trueBN
    LearningLog.testData.addAll(generateSamples(trueBN, 10000))

    val data = generateSamples(trueBN, 10000)
    val rubbishStartingBN = BayesNet(trueBN.nodes.mapValues {
        BNode(it.value.rv, emptyList(), randomCPT(it.key, emptyList()))
    })

    // Incremental Learning
    val learnedBN100 = friedmanStructureLearning(randomParams(rawStructure(trueBN)), data, 100, 5.0)
    val learnedBN400 = friedmanStructureLearning(randomParams(rawStructure(trueBN)), data, 400, 5.0)
    val learnedBN800 = friedmanStructureLearning(randomParams(rawStructure(trueBN)), data, 800, 5.0)

    //println("Incremental BN:\n$learnedBN100")

    // Naive learning
    println("Start Naive")
    LearningLog.KLs["Naive"] = ArrayList()
    LearningLog.SHDs["Naive"] = ArrayList()
    val naiveStepSize = 1000
    var currentNaiveStruct = rawStructure(rubbishStartingBN)

    val orderedVocab = trueBN.vocab.toList()
    val parentSizeRestriction = 5
    val adTree = buildADTree(orderedVocab, emptyList(), ADBuildParams(5, 200, 100, 0.6, parentSizeRestriction + 1))
    for(i in 0..(data.size / naiveStepSize) - 1){
        println(i)
        val additionalData = data.subList(i * naiveStepSize, (i + 1) * naiveStepSize)

        updateADTree(adTree, additionalData)

        currentNaiveStruct = greedyStructureSearch(currentNaiveStruct, adTree, 5.0, parentSizeRestriction = parentSizeRestriction)
        val naiveStructStats = sufficientStats(listOf(currentNaiveStruct), adTree, emptyMap())
        val naiveLearnedParams =  learnParams(currentNaiveStruct, naiveStructStats, 5.0)

        LearningLog.KLs["Naive"]!!.add(KLDivEmpirical(trueBN, naiveLearnedParams, LearningLog.testData))
        LearningLog.SHDs["Naive"]!!.add(structuralHammingDistance(trueBN, naiveLearnedParams))
    }

    println("Naive:\n$currentNaiveStruct")

    saveToJson(LearningLog, "Friedman")
}
fun friedmanStructureLearningHidden(startingNetwork : BayesNet, data : List<RVAssignment>, stepSize : Int, smoothingCount : Double, decay: Double) : BayesNet{
    var currentBN = startingNetwork
    var currentStructure = rawStructure(currentBN)
    var neighbours = validNeighbors(currentStructure).map { it.bnStruct }
    var sufficientStats = sufficientStatsCached(emptyMap(), neighbours + currentStructure)
    for(i in data.indices){
        val completedCounts = completeData(currentBN, listOf(data[i]))
        for((scope, countTable) in sufficientStats){
            // Decay old counts
            for((countInd, count) in countTable.counts.withIndex()){
                countTable.counts[countInd] = count * decay
            }
            // Add in new weighted counts
            countTable.updateCounts(completedCounts)
        }
        if(i % stepSize == 0){
            val neighboursAndScores = neighbours.map{Pair(it, averageBDEuScore(it, sufficientStats, smoothingCount))}
            val bestNeighbour = neighboursAndScores.maxBy { it.second }!!
            val currentStructureScore = averageBDEuScore(currentStructure, sufficientStats, smoothingCount)
            if(bestNeighbour.second > currentStructureScore){
                currentStructure = bestNeighbour.first
            }
            neighbours = validNeighbors(currentStructure).map { it.bnStruct }
            sufficientStats = sufficientStatsCached(sufficientStats, neighbours + currentStructure)
        }
        currentBN = learnParams(currentStructure, sufficientStats, smoothingCount)
        // Careful with this, at the moment you are having to manually specify the true structure in a global way. You may want to change this somehow
        //LearningLog.KLs.getOrPut("FriedmanBDeu, k=$stepSize", { ArrayList()}).add(KLDivergence(ExampleBNs.studentBN.nodes.keys.filter{it != G}, ExampleBNs.studentBN, currentBN))
        //LearningLog.SHDs.getOrPut("FriedmanBDeu, k=$stepSize", {ArrayList()}).add(structuralHammingDistance(ExampleBNs.studentBN, currentBN))
    }
    return currentBN
}

fun friedmanStructureLearning(startingNetwork : BayesNet, data : List<RVAssignment>, stepSize : Int, smoothingCount : Double) : BayesNet {
    var currentStructure = rawStructure(startingNetwork)
    var currentBN = startingNetwork
    var neighbours = validNeighbors(currentStructure).map { it.bnStruct }
    var sufficientStatistics = sufficientStatsCached(emptyMap(), neighbours + currentStructure)

    for((i, sample) in data.withIndex()){

        for((rvSet, countTable) in sufficientStatistics){
            countTable.updateCounts(sample.assignment)
        }

        if(i > 0 && i % stepSize == 0){
            println(i)
            val neighboursAndScores = neighbours.map { Pair(it, averageBDEuScore(it, sufficientStatistics, smoothingCount)) }
            val bestNeighbor = neighboursAndScores.maxBy{it.second}!!
            val currentStructureScore = averageBDEuScore(currentStructure, sufficientStatistics, smoothingCount)
            if(bestNeighbor.second > currentStructureScore){
                currentStructure = bestNeighbor.first
            }

            neighbours = validNeighbors(currentStructure).map { it.bnStruct }
            sufficientStatistics = sufficientStatsCached(sufficientStatistics, neighbours + currentStructure)

            LearningLog.KLs.getOrPut("FriedmanBDeu, k=$stepSize", { ArrayList()}).add(KLDivEmpirical(LearningLog.trueBN!!, currentBN, LearningLog.testData))
            LearningLog.SHDs.getOrPut("FriedmanBDeu, k=$stepSize", {ArrayList()}).add(structuralHammingDistance(LearningLog.trueBN!!, currentBN))
        }


        currentBN = learnParams(currentStructure, sufficientStatistics, smoothingCount)
    }

    val sufficientStatSizes = sufficientStatistics.mapValues { it.value.counts.sum() }
    val maxStat = sufficientStatSizes.maxBy { it.value }
    val minStat = sufficientStatSizes.minBy { it.value }
    val avgStat = sufficientStatSizes.values.average()

    println("Max: $maxStat")
    println("Min: $minStat")
    println("Average: $avgStat")


    return currentBN
}

fun averageMDL(structure : Map<RandomVariable, Set<RandomVariable>>, sufficientStats : Map<Set<RandomVariable>, CountTable>, smoothingCount : Double) : Double{
    var averageMDL = 0.0
    for((rv, parents) in structure){
        val sufficientStat = sufficientStats[parents + rv]!!
        val numData = sufficientStat.counts.sum()
        averageMDL += MDL(rv, parents, sufficientStat, smoothingCount) / numData
    }

    return averageMDL
}

fun MDL(rv : RandomVariable, parents : Set<RandomVariable>, sufficientStat : CountTable, smoothingCount : Double) : Double{
    var conditionalEntropy = 0.0
    val totalAssignments = numAssignments(parents.toList() + rv)
    val smoothingAlpha = smoothingCount / totalAssignments

    for(parentAssignment in allAssignments(parents.toList())){
        // Calc the marginal counts
        val totalCount = (0..(rv.domainSize - 1))
                .map { assignmentToIndex(parentAssignment + Pair(rv, it), sufficientStat.scope) }
                .sumByDouble { sufficientStat.counts[it] }

        for(childVal in 0..(rv.domainSize - 1)){
            val fullAssignment = parentAssignment + Pair(rv, childVal)
            val assignmentIndex = assignmentToIndex(fullAssignment, sufficientStat.scope)
            val count = sufficientStat.counts[assignmentIndex]
            conditionalEntropy += (count + smoothingAlpha) * logOfBase((count + smoothingAlpha) / (totalCount + smoothingAlpha * rv.domainSize), 2.0)
        }
    }

    val numData = sufficientStat.counts.sum()

    return conditionalEntropy - logOfBase(numData, 2.0) / 2.0 * parents.size
}

fun averageBDEuScore(structure: Map<RandomVariable, Set<RandomVariable>>, sufficientStats : Map<Set<RandomVariable>, CountTable>, equivalentSampleSize : Double) : Double{
    var score = 0.0
    for((child, parents) in structure){
        val sufficientStat = sufficientStats[parents + child]!!
        val numData = sufficientStat.counts.sum()
        score += (BDeuNodeScore(child, parents, sufficientStat, equivalentSampleSize) / numData)
    }
    return score
}

fun sufficientStatsCached(cachedStatistics : Map<Set<RandomVariable>, CountTable>, relevantStructures : List<Map<RandomVariable, Set<RandomVariable>>>) : Map<Set<RandomVariable>, CountTable>{
    val stats = HashMap<Set<RandomVariable>, CountTable>()
    for(structure in relevantStructures){
        for((child, parents) in structure){
            val family : Set<RandomVariable> = parents + child
            // Carry over needed cached values
            if(cachedStatistics.containsKey(family)){
                stats[family] = cachedStatistics[family]!!
            }
            else{
                val superSetFamily = cachedStatistics.keys.find { supersetFamily -> supersetFamily.containsAll(family) }
                if(superSetFamily != null){
                    stats[family] = projectCounts(cachedStatistics[superSetFamily]!!, family.toList())
                }
                else{
                    stats[family] = CountTable(family.toList(), DoubleArray(numAssignments(family.toList())))
                }
            }
        }
    }

    return stats
}
*/

fun learnParams(currentStructure : Map<RandomVariable, Set<RandomVariable>>, sufficientStats : Map<Set<RandomVariable>, CountTable>, smoothingCount : Double) : BayesNet{
    val nodeMap = HashMap<RandomVariable, BNode>()
    for((rv, parentSet) in currentStructure){
        val orderedParents = parentSet.toList()
        val sufficientStat = sufficientStats[parentSet + rv]!!
        val totalAssignments = numAssignments(orderedParents + rv)
        val cpt = DoubleArray(totalAssignments)
        val smoothingAlpha = smoothingCount / totalAssignments

        val indicesHit = ArrayList<Int>()
        for(parentAssignment in allAssignments(orderedParents)){
            val marginalCount = (0..(rv.domainSize - 1)).map{ rvVal ->
                val assgnIndex = assignmentToIndex(parentAssignment + Pair(rv, rvVal), sufficientStat.scope)
                sufficientStat.counts[assgnIndex]
            }.sum()

            for(childVal in 0..(rv.domainSize - 1)){
                val fullAssignment = parentAssignment + Pair(rv, childVal)
                val suffStatIndex = assignmentToIndex(fullAssignment, sufficientStat.scope)
                val cptAssignmentIndex = assignmentToIndex(fullAssignment, listOf(rv) + orderedParents)
                val count = sufficientStat.counts[suffStatIndex]
                cpt[cptAssignmentIndex] = (count + smoothingAlpha) / (marginalCount + smoothingAlpha * rv.domainSize)
                indicesHit.add(cptAssignmentIndex)
            }
        }

        nodeMap[rv] = BNode(rv, orderedParents, cpt.toList())
    }
    return BayesNet(nodeMap)
}
