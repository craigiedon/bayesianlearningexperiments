import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.io.File

object ExampleBNs{
    val studentBN = BayesNet(mapOf(
            C to BNode(C, emptyList(), listOf(0.9, 0.1)),
            D to BNode(D, listOf(C), listOf(0.2,0.8,0.7,0.3)),
            G to BNode(G, listOf(D, I), listOf(0.1, 0.9, 0.2, 0.8, 0.3, 0.7, 0.4, 0.6)),
            I to BNode(I, emptyList(), listOf(0.55, 0.45)),
            S to BNode(S, listOf(I), listOf(0.35, 0.65, 0.15, 0.85)),
            L to BNode(L, listOf(G), listOf(0.5, 0.5, 0.2, 0.8)),
            J to BNode(J, listOf(S, L), listOf(0.1, 0.9, 0.45, 0.55, 0.3, 0.7, 0.9, 0.1)),
            H to BNode(H, listOf(G), listOf(0.4, 0.6, 0.8, 0.2))
    ))

    val hiddenBN = BayesNet(mapOf(
            C to BNode(C, emptyList(), listOf(0.9, 0.1)),
            H to BNode(H, emptyList(), listOf(0.4, 0.6)),
            D to BNode(D, listOf(C, H), listOf(0.2, 0.8, 0.7, 0.3, 0.4, 0.6, 0.1, 0.9), hidden = true),
            S to BNode(S, listOf(D), listOf(0.4, 0.6, 0.75, 0.25)),
            G to BNode(G, listOf(D), listOf(0.1, 0.9, 0.4, 0.6))
    ))

    val tinyBN = BayesNet(mapOf(
            G to BNode(G, listOf(D, I), listOf(0.1, 0.9, 0.8, 0.2, 0.3, 0.7, 0.6, 0.4)),
            D to BNode(D, emptyList(), listOf(0.1, 0.9)),
            I to BNode(I, emptyList(), listOf(0.9, 0.1))
    ))
}

fun main(args : Array<String>){
    val query = mapOf(D to 0, I to 0)
    val queryProb = simpleLnQuery(query, ExampleBNs.tinyBN)
    val expQuery = Math.exp(queryProb)
    println(expQuery)
}

fun loadJsonBN(filePath : String) : BayesNet{
    val mapper = jacksonObjectMapper().registerKotlinModule()
    val bayesNetData = mapper.readValue<Map<String, BNodeData>>(File(filePath))
    val vocab_map = bayesNetData.entries.associate { Pair(it.key, RandomVariable(it.key, it.value.domainSize)) }

    val bnMap = bayesNetData
            .mapKeys { vocab_map[it.key]!!}
            .mapValues { bd -> BNode(bd.key, bd.value.parents.map { vocab_map[it]!! }, bd.value.cpt)}
    return BayesNet(bnMap)
}
