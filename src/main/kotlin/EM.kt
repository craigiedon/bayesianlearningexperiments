import Utils.randomParams
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.io.BufferedWriter
import java.io.FileWriter
import java.util.*

fun fullyObservedParamlearning(bnStructure : Map<RandomVariable, Set<RandomVariable>>, data : List<RVAssignment>) : BayesNet{
    val hardCounts : Map<RandomVariable, MutableMap<Map<RandomVariable, Int>, Double>> = bnStructure.mapValues {
        val familyScope = listOf(it.key) + it.value
        val familyAssignments = allAssignments(familyScope)
        HashMap(familyAssignments.associate { Pair(it,0.0) })
    }

    for(sample in data){
        for((rv, parents) in bnStructure){
            val familyScope = listOf(rv) + parents
            val familyAssignment = sample.filterKeys { familyScope.contains(it) }
            hardCounts[rv]!![familyAssignment] = hardCounts[rv]!![familyAssignment]!! + 1
        }
    }

    val newNodes = HashMap<RandomVariable, BNode>()
    for((rv, parentSet) in bnStructure){
        val orderedParents = parentSet.toList()
        val updatedCPT = DoubleArray(numAssignments(listOf(rv) + orderedParents))
        val parentAssignments = allAssignments(orderedParents)
        for(parentAssignment in parentAssignments){
            val familyAssignment = HashMap(parentAssignment)
            val smoothingAlpha = 1.0 / (rv.domainSize * parentAssignments.size)
            var marginalCount = 0.0
            for(childVal in 0..rv.domainSize - 1){
                familyAssignment[rv] = childVal
                marginalCount += hardCounts[rv]!![familyAssignment]!!
            }

            for(childVal in 0..rv.domainSize - 1){
                familyAssignment[rv] = childVal
                val cptIndex = assignmentToIndex(familyAssignment, listOf(rv) + orderedParents)
                updatedCPT[cptIndex] = (hardCounts[rv]!![familyAssignment]!! + smoothingAlpha) / (marginalCount + smoothingAlpha * rv.domainSize)
            }
        }

        newNodes.put(rv, BNode(rv, orderedParents, updatedCPT.toList()))
    }

    return BayesNet(newNodes)
}

fun EStep(bn : BayesNet, data : List<RVAssignment>, desiredSufficientStats : List<Set<RandomVariable>> = emptyList()) : SufficientStats{

    val suffStats = if(desiredSufficientStats.isEmpty())
        initializeStats(bn.nodes.map { (_, node) -> setOf(node.rv) + node.parents })
    else
        initializeStats(desiredSufficientStats)

    val fullFactors = extractFactors(bn)

    for(sample in data){
        val reducedFactors = fullFactors.map { evidenceApplicationPrune(sample, it, emptyList())}.filter { !it.scope.isEmpty() }
        val cliqueForest = convertToCliqueForest(reducedFactors)
        val potentialAssignments = assignPotentials(cliqueForest, reducedFactors)
        val calibratedCliqueTrees = cliqueForest.map { cliqueTree -> sumProductCalibrate(cliqueTree, potentialAssignments) }

        val cachedQueries = HashMap<Set<RandomVariable>, Factor>()

        for((familySet, ct) in suffStats){
            val unknownVarSet = (familySet - sample.keys)

            if(unknownVarSet.isEmpty()){
                val countsIndex = assignmentToIndex(sample, ct.scope)
                ct.counts[countsIndex] += 1.0
            }
            else{
                val unknownVarDistribution = cachedQueries[unknownVarSet]
                    ?: outOfCliqueQuery(unknownVarSet.toList(), CalibratedCliqueForest(calibratedCliqueTrees))

                cachedQueries[unknownVarSet] = unknownVarDistribution

                for (unknownVarAssignment in allAssignments(unknownVarDistribution.scope)){
                    val possibleAssignment = unknownVarAssignment + sample
                    val queryIndex = assignmentToIndex(unknownVarAssignment, unknownVarDistribution.scope)
                    val countsIndex = assignmentToIndex(possibleAssignment, ct.scope)

                    ct.counts[countsIndex] += unknownVarDistribution.values[queryIndex]
                }
            }

        }
    }
    return suffStats
}

fun MStep(bnSkeleton : Map<RandomVariable, Set<RandomVariable>>, softCounts : Map<Set<RandomVariable>, CountTable>, pseudoCountStrength : Double) : BayesNet {

    val newNodes = HashMap<RandomVariable, BNode>()
    for((rv, parents) in bnSkeleton){

        val orderedParents = parents.toList()
        val family = setOf(rv) + orderedParents
        val updatedCPT = DoubleArray(numAssignments(listOf(rv) + orderedParents))
        val parentAssignments = allAssignments(orderedParents.toList())

        for(parentAssignment in parentAssignments){
            val familyAssignment = HashMap(parentAssignment)
            var marginalCount = 0.0
            val smoothingAlpha = pseudoCountStrength / (rv.domainSize * parentAssignments.size)
            for(childVal in 0..rv.domainSize - 1){
                familyAssignment[rv] = childVal
                val countTableIndex = assignmentToIndex(familyAssignment, softCounts[family]!!.scope)
                marginalCount += softCounts[family]!!.counts[countTableIndex]
            }

            for(childVal in 0..rv.domainSize - 1){
                familyAssignment[rv] = childVal
                val countTableIndex = assignmentToIndex(familyAssignment, softCounts[family]!!.scope)
                val cptIndex = assignmentToIndex(familyAssignment, listOf(rv) + orderedParents)
                updatedCPT[cptIndex] = (softCounts[family]!!.counts[countTableIndex] + smoothingAlpha) / (marginalCount + smoothingAlpha * rv.domainSize)
                if(updatedCPT[cptIndex].isNaN()){
                    println("NAN alert")
                }
            }
        }

        if(updatedCPT.any{it < 0.0}){
            println("Negative number??? How????")
        }

        newNodes.put(rv, BNode(rv, orderedParents, updatedCPT.toList()))
    }

    return BayesNet(newNodes)
}

fun batchEM(bnStruct : BNStruct, data : List<RVAssignment>, iterations : Int) =
    batchEM(randomParams(bnStruct, 2), data, iterations)

fun batchEM(startingBN: BayesNet, data : List<RVAssignment>, iterations: Int) : BayesNet{
    var softCounts : Map<Set<RandomVariable>, CountTable>
    var currentBN = startingBN

    for(i in 1..iterations){
        softCounts = EStep(currentBN, data)
        currentBN = MStep(rawStructure(currentBN), softCounts , 1.0)
    }

    return currentBN
}

fun mergeCountTables(ct1 : CountTable, ct2 : CountTable, mergeFunc : (Double, Double) -> Double) : CountTable{
    if(ct1.scope.toSet() != ct2.scope.toSet()){
        throw IllegalArgumentException("Cannot merge two unweightedCount tables of different scopes")
    }

    val sumTable = CountTable(ct1.scope, DoubleArray(ct1.counts.size))

    for(i in sumTable.counts.indices){
        val ct1Index = i
        val ct2Index = convertIndex(i, ct1.scope, ct2.scope)
        sumTable.counts[i] = mergeFunc(ct1.counts[ct1Index], ct2.counts[ct2Index])
    }

    return sumTable
}

fun <K, V> mergeMaps(m1 : Map<K, V>, m2: Map<K,V>, mergeFunc: (V,V) -> V) : Map<K, V> =
    m1.mapValues { mergeFunc(it.value, m2[it.key]!!) }

fun mergeSufficientStats(m1 : Map<Set<RandomVariable>, CountTable>, m2 : Map<Set<RandomVariable>, CountTable>, mergeFunc: (Double, Double) -> Double) =
    mergeMaps(m1, m2, {c1, c2 -> mergeCountTables(c1, c2, mergeFunc)})


fun incrementalEM(startingBN : BayesNet, data : List<RVAssignment>, iterations: Int) : BayesNet{
    val individualSoftCounts = data.map { EStep(startingBN, listOf(it)) }.toMutableList()
    var softCounts : Map<Set<RandomVariable>, CountTable> = individualSoftCounts.reduce { m1, m2 -> mergeSufficientStats(m1,m2, {c1, c2 -> c1 + c2}) }
    var currentBN = MStep(rawStructure(startingBN), softCounts, 1.0)

    val shuffledIndices = data.indices.toList()
    for(iteration in 1..iterations){
        Collections.shuffle(shuffledIndices)
        for(i in shuffledIndices){
            val newSoftCount = EStep(currentBN, listOf(data[i]))
            softCounts = mergeSufficientStats(softCounts, individualSoftCounts[i], {total, old -> total - old})
            softCounts = mergeSufficientStats(softCounts, newSoftCount, {total, new -> total + new})
            individualSoftCounts[i] = newSoftCount
            currentBN = MStep(rawStructure(currentBN), softCounts, 1.0)
        }
        LearningLog.KLs.getOrPut("incremental", {ArrayList()}).add(KLDivExact(ExampleBNs.hiddenBN.nodes.keys.filter { !startingBN.nodes[it]!!.hidden }, ExampleBNs.hiddenBN, currentBN))
        LearningLog.LLs.getOrPut("incremental", {ArrayList()}).add(logLikelihood(data, currentBN))
    }

    return currentBN
}

fun stepWiseEM(startingBN : BayesNet, data : List<RVAssignment>, iterations : Int, miniBatchSize : Int, stepPower : Double) : BayesNet{

    var softCounts : SufficientStats = initializeStats(startingBN.nodes.values.map { it.parents.toSet() + it.rv })
    var currentBN = startingBN

    val shuffledIndices = data.indices.toList()
    for(iteration in 0..(iterations - 1)){
        Collections.shuffle(shuffledIndices)
        for(i in 0..(data.size / miniBatchSize) - 1){
            val steps = iteration * (data.size / miniBatchSize) + i
            val miniBatch = shuffledIndices.subList(i * miniBatchSize, (i+1) * miniBatchSize).map{ data[it] }
            softCounts = stepWiseESameSize(currentBN, softCounts, miniBatch, 1.0, steps, stepPower)
            val priorStrength = 1.0 * (miniBatchSize.toDouble() / data.size)
            currentBN = MStep(rawStructure(currentBN), softCounts, priorStrength)
        }
    }

    return currentBN
}

fun stepWiseESameSize(currentBN : BayesNet, currentSufficientStats: SufficientStats, newMiniBatch : List<RVAssignment>, baseStepSize: Double, steps: Int, annealingRate: Double) : SufficientStats{
    if(currentSufficientStats.isEmpty() || currentSufficientStats.entries.first().value.counts.sum() == 0.0)
        return sufficientStats(listOf(rawStructure(currentBN)), completeData(currentBN, newMiniBatch))
    val newTrialStats = sufficientStats(currentSufficientStats.keys, completeData(currentBN, newMiniBatch))
    val mixNew = baseStepSize * Math.pow(steps + 2.0, -annealingRate)
    val interpolateFunc : (Double, Double) -> Double = {current, new -> (1 - mixNew) * current + mixNew * new}
    return mergeSufficientStats(currentSufficientStats, newTrialStats, interpolateFunc)
}

fun stepWiseE(currentBN: BayesNet, currentSufficientStats : SufficientStats, newMiniBatch : List<RVAssignment>, baseStepSize : Double, steps: Int, annealingRate : Double) : SufficientStats {

    if (currentSufficientStats.isEmpty()){
        return sufficientStats(listOf(rawStructure(currentBN)), completeData(currentBN, newMiniBatch))
    }
    if(currentSufficientStats.entries.first().value.counts.sum() == 0.0){
        return sufficientStats(currentSufficientStats.keys, completeData(currentBN, newMiniBatch))
    }

    val newTrialStats = sufficientStats(currentSufficientStats.keys, completeData(currentBN, newMiniBatch))

    val currentStatsSize = currentSufficientStats.values.first().counts.sum()

    if(currentStatsSize == 0.0)
        return newTrialStats

    val normalizedStats = scaleStats(currentSufficientStats, newMiniBatch.size / currentStatsSize)

    val mixNew = baseStepSize * Math.pow(steps + 2.0, -annealingRate)
    val mergeFunc : (Double, Double) -> Double = {current, new -> (1 - mixNew) * current + mixNew * new}
    val newNormedStats = mergeSufficientStats(normalizedStats, newTrialStats, mergeFunc)

    val scaledStats = scaleStats(newNormedStats, currentStatsSize / newMiniBatch.size + 1.0)
    if(scaledStats.values.any{it.counts.any { it.isInfinite() || it.isNaN() }}
        || Math.round(scaledStats.values.first().counts.sum()) != Math.round(currentStatsSize) + newMiniBatch.size){
        println("Numerical Error in Stepwise E")
    }

    return scaledStats
}

fun main(args : Array<String>){
    println(LearningLog.KLs)
    println(LearningLog.LLs)

    saveToJson(LearningLog, "graphGeneration", "evaluationLog")
}

object LearningLog {
    var trueBN : BayesNet? = null
    val testData = ArrayList<RVAssignment>()

    val KLs = HashMap<String, MutableList<Double>>()
    val LLs = HashMap<String, MutableList<Double>>()
    val SHDs = HashMap<String, MutableList<Int>>()
}
