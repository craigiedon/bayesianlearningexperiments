package DNUnawareness
import RandomVariable
import pathExists
import pathExistsDirGraph

sealed class StartOrderResult
object SOSuccess : StartOrderResult()
data class SOFail(val culprits : List<RandomVariable>) : StartOrderResult()


fun validOrderExists(possibleChildren: Map<RandomVariable, Set<RandomVariable>>, rewardDomain: Set<RandomVariable>) : StartOrderResult{
    val beliefChildren = possibleChildren.filterKeys { it.varType == VarType.BELIEF }
    val outcomeChildren = possibleChildren.filterKeys { it.varType == VarType.OUTCOME }

    val bCulprits = beliefChildren
            .filter { it.key !in rewardDomain }
            .filter { (_, children) -> children.none{ it.varType == VarType.OUTCOME} }
            .keys
            .toList()


    // Possible culprits will be non-reward belief variables which don't have outcome children
    if(bCulprits.isNotEmpty()){
        return SOFail(bCulprits)
    }

    val oCulprits = outcomeChildren
        .keys
        .filter { it !in rewardDomain}
        .filter { !pathExistsDirGraph(it, rewardDomain, possibleChildren) }
        .filter { pathExists(it, it, possibleChildren) }

    // Possible culprits will be outcome variables that don't connect to a reward domain, and are stuck in a cycle
    if(oCulprits.isNotEmpty()){
        return SOFail(oCulprits)
    }

    return SOSuccess
}


/*
fun belOrderExists(beliefsPossibleChildren: Map<RandomVariable, Set<RandomVariable>>, rewardDomain: Set<RandomVariable>) : Boolean{
    System.loadLibrary("jniortools")
    val cpModel = CpModel()

    // Each variable will have a number corresponding to its position in total order
    val orderVars = beliefsPossibleChildren.keys.associateWith { cpModel.newIntVar(1, beliefsPossibleChildren.size.toLong(), it.name) }
    cpModel.addAllDifferent(orderVars.values.toTypedArray())

    for((rv, childSet) in beliefsPossibleChildren){
        // If youre not in Reward Dom, and cant connect to any outcomes, you must come before at least one belief variable
        if(rv !in rewardDomain && !childSet.any{it.varType != VarType.BELIEF}){
            val bigM = orderVars.size * 2
            val beforeChildren = ArrayList<IntVar>()
            for(child in childSet){
                val beforeChild = cpModel.newBoolVar("${rv.name} < ${child.name}")
                // ILP version of: parentOrder < childOrder
                cpModel.addScalProd(arrayOf(orderVars[rv],orderVars[child], beforeChild), intArrayOf(1, -1, bigM), -bigM.toLong(), bigM.toLong())
                beforeChildren.add(beforeChild)
            }
            cpModel.addBoolOr(beforeChildren.toTypedArray())
        }
    }

    val solver = CpSolver()
    val status = solver.solve(cpModel)
    return status == CpSolverStatus.FEASIBLE
}
*/

/*
fun outcomeOrdExists(outcomeParents : Map<RandomVariable, Set<RandomVariable>>,
                     outcomeChildren: Map<RandomVariable, Set<RandomVariable>>,
                     rewardDomain: Set<RandomVariable>) : Boolean{
    System.loadLibrary("jniortools")
    val cpModel = CpModel()
    val solver = CpSolver()


    // Each variable will have a number corresponding to its position in total order
    val orderVars = outcomeChildren.keys.associateWith { cpModel.newIntVar(1, outcomeChildren.size.toLong(), it.name) }
    cpModel.addAllDifferent(orderVars.values.toTypedArray())


    for((rv, childSet) in outcomeChildren){
        // Ignore any variables which are in the reward domain
        if(rv !in rewardDomain){
            val bigM = orderVars.size * 2
            val beforeChildren = ArrayList<IntVar>()
            for(child in childSet){
                val beforeChild = cpModel.newBoolVar("${rv.name} < ${child.name}")
                // ILP version of: parentOrder < childOrder
                cpModel.addScalProd(arrayOf(orderVars[rv],orderVars[child], beforeChild), intArrayOf(1, -1, bigM), -bigM.toLong(), bigM.toLong())
                beforeChildren.add(beforeChild)
            }
            cpModel.addBoolOr(beforeChildren.toTypedArray())
        }
    }

    for((rv, parents) in outcomeParents){
        // If an rv doesnt have any action parents, it must come after one of its outcome parents
        if(!parents.any { it.varType == VarType.ACTION }){
            val parentOutcomes = parents.filter { it.varType == VarType.OUTCOME }
            if(parentOutcomes.isNotEmpty()){
                val afterOutcomesConstraints = ArrayList<IntVar>()
                for(parent in parentOutcomes){
                    val afterOutcomesConstraint = cpModel.newBoolVar("${rv.name} > ${parent.name}")
                    afterOutcomesConstraints.add(afterOutcomesConstraint)
                }
                cpModel.addBoolOr(afterOutcomesConstraints.toTypedArray())
            }
        }
    }


    val status = solver.solve(cpModel)
    return status == CpSolverStatus.FEASIBLE
}
*/


/*
fun feasibleBeliefOrder(beliefsPossibleChildren : Map<RandomVariable, Set<RandomVariable>>, parentEvidence : List<DirEdge>, rewardDomain: Set<RandomVariable>) : Map<RandomVariable, Int>?{
    System.loadLibrary("jniortools")
    val solver = Solver("Reward Function Solver")

    val orderVars = beliefsPossibleChildren.keys.associate {
        Pair(it, solver.makeIntVar(1, beliefsPossibleChildren.size.toLong(), it.name))
    }

    for((rv, childSet) in beliefsPossibleChildren){
        // For parent->child evidence, the parent must come before child in ordering
        val evidenceConstraints = parentEvidence.filter { (p, c) -> p == rv && c.varType == VarType.BELIEF }
        for((parentEv, childEv) in evidenceConstraints){
            solver.addConstraint(solver.makeLess(orderVars[parentEv], orderVars[childEv]))
        }
        // If youre not in Reward Dom, and cant connect to any outcomes, you must come before at least one belief variable
        if(rv !in rewardDomain && !childSet.any{it.varType != VarType.BELIEF}){
            val orPrecConstraints = childSet.map { child -> solver.makeIsLessVar(orderVars[rv], orderVars[child]) }.toTypedArray()
            solver.addConstraint(solver.makeSumGreaterOrEqual(orPrecConstraints , 1))
        }
    }

    val displayParams = DefaultPhaseParameters()
    displayParams.display_level = DefaultPhaseParameters.NONE
    val decisionBuilder = solver.makeDefaultPhase(orderVars.values.toTypedArray(), displayParams)
    solver.newSearch(decisionBuilder)
    val result = if(solver.nextSolution()) orderVars.mapValues{ it.value.value().toInt() } else null

    solver.endSearch()
    solver.delete()

    return result
}
*/

/*
fun feasibleOutcomeOrder(outcomesPossibleParents : Map<RandomVariable, Set<RandomVariable>>, outcomesPossibleChildren : Map<RandomVariable, Set<RandomVariable>>, parentEvidence : List<DirEdge>, rewardDomain : Set<RandomVariable>) : Map<RandomVariable, Int>?{
    System.loadLibrary("jniortools")
    val solver = Solver("Reward Function Solver")

    val orderVars = outcomesPossibleChildren.keys.associate {
        Pair(it, solver.makeIntVar(1, outcomesPossibleChildren.size.toLong(), it.name))
    }

    for((rv, childSet) in outcomesPossibleChildren){
        val evidenceConstraints = parentEvidence.filter { (parent, _) -> parent == rv }
        for((parentEv, childEv) in evidenceConstraints){
            solver.addConstraint(solver.makeLess(orderVars[parentEv], orderVars[childEv]))
        }
        // Ignore any variables which are in the reward domain
        if(rv !in rewardDomain){
            val orPrecConstraints = childSet.map { child -> solver.makeIsLessVar(orderVars[rv], orderVars[child]) }.toTypedArray()
            solver.addConstraint(solver.makeSumGreaterOrEqual(orPrecConstraints , 1))
        }
    }

    for((rv, parents) in outcomesPossibleParents){
        // If an rv doesnt have any action parents, it must come after one of its outcome parents
        if(!parents.any { it.varType == VarType.ACTION }){
            val parentOutcomes = parents.filter { it.varType == VarType.OUTCOME }
            if(parentOutcomes.isNotEmpty()){
                val afterOutcomesConstraint = parentOutcomes.map { parent -> solver.makeIsGreaterVar(orderVars[rv], orderVars[parent])}.toTypedArray()
                solver.addConstraint(solver.makeSumGreaterOrEqual(afterOutcomesConstraint, 1))
            }
        }
    }

    val displayParams = DefaultPhaseParameters()
    displayParams.display_level = DefaultPhaseParameters.NONE
    val decisionBuilder = solver.makeDefaultPhase(orderVars.values.toTypedArray(), displayParams)
    solver.newSearch(decisionBuilder)
    val result = if(solver.nextSolution()) orderVars.mapValues { it.value.value().toInt() } else null
    solver.endSearch()
    solver.delete()

    return result
}
*/
