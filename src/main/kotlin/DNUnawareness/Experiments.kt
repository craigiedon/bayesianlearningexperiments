package DNUnawareness

import BDeuNodeScore
import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import RandomVariable
import RewardTable
import loadJson
import runSystem
import saveToJson
import BNode
import DirEdge
import BayesNet
import KLDivFastCached
import CountTable
import Trial
import Utils.*
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import generateSamples
import org.apache.commons.io.FilenameUtils
import simpleLnQueryFull
import structuralHammingDistance

data class TaskConfig(
    val numTrials: Int,
    val trueGraphPath: String,
    val agentStarterPath: String
)

data class ExperimentConfig(val truePolicy : Boolean,
                            val agentExploration: Double,
                            val parentExtensionStrategy: String,
                            val expertMessagesMinInterval : Int,
                            val subOptimalThresh: Double,
                            val structuralUpdateFrequency: Int,
                            val structLearnConfig: StructLearnConfig,
                            val inReasonableMix : Double,
                            val enforceConnectivity : Boolean
)

data class BNExperimentConfig(
    val parentExtensionStrategy: String,
    val structuralUpdateFrequency: Int,
    val structLearnConfig: StructLearnConfig,
    val inReasonableMix : Double,
    val vocabIntroInterval : Int
)

data class AgentStartInfo(
    val vocab : Set<String>,
    val rewardScope : Set<String>
)

object ResultsLogger{
    val printToConsole = true
    private var outputFolder : String? = null
    private var taskName : String? = null

    fun setPath(folderPath: String, fName: String){
        outputFolder = folderPath
        taskName = fName
        File(outputFolder).mkdirs()
        File(outputFolder).listFiles()
            .filter { it.name.startsWith(fName) }
            .forEach { it.delete() }
    }

    fun logTimeStampedResult(timeStamp: TimeStamp, result : Double, metric : String){
        if(printToConsole){
            println("$metric - ts: $timeStamp, val: $result")
        }

        val outputPath = FilenameUtils.concat(outputFolder, "$taskName-$metric.txt")
        val writer = PrintWriter(FileWriter(outputPath, true))
        writer.println("$timeStamp, $result")
        writer.close()

    }
    fun logParentEv(timeStamp: TimeStamp, parentEv : List<DirEdge>){
        if(printToConsole){
            println("Current parent ev: $parentEv")
        }

        val outputPath = FilenameUtils.concat(outputFolder, "$taskName-ParentEv.txt")
        val writer = PrintWriter(FileWriter(outputPath, true))
        val parentEvNames = parentEv.map { Pair(it.first.name, it.second.name) }
        writer.println("$timeStamp, $parentEvNames")
        writer.close()
    }

    fun logBetterActionAdv(timeStamp : TimeStamp, advice: BetterActionInference){
        val outputPath = FilenameUtils.concat(outputFolder, "$taskName-BetterAction.txt")
        val writer = PrintWriter(FileWriter(outputPath, true))
        writer.println("$timeStamp, $advice")
        writer.close()
    }

    fun logDN(dn : DecisionNetwork){
        saveToJson(DNToDNData(dn), outputFolder!!, "$taskName-learnedDN")
    }

    fun logBN(bn : BayesNet){
        saveToJson(bn, outputFolder!!, "$taskName-learnedBN")
    }
}

fun runExperiment(experimentName: String, taskId : String, taskConfig: TaskConfig, config : ExperimentConfig) {
    ResultsLogger.setPath("logs/$experimentName", taskId)

    val trueDN = readDN(taskConfig.trueGraphPath)
    val initialVocab : Set<RandomVariable>
    val agentInitRewardDom : Set<RandomVariable>
    if(!config.truePolicy){
        val agentStartInfo = loadJson(taskConfig.agentStarterPath, AgentStartInfo::class.java)
        initialVocab = trueDN.vocab.filter { it.name in agentStartInfo.vocab }.toSet()
        agentInitRewardDom = trueDN.vocab.filter { it.name in agentStartInfo.rewardScope }.toSet()
    }
    else{
        initialVocab = trueDN.vocab.toSet()
        agentInitRewardDom = trueDN.rewardFunction.scope.toSet()
    }

    // This switch could actually be done with better polymorphic json attributes (see sequential codebase)
    val parentExtStrat = when(config.parentExtensionStrategy){
        "Conservative" -> Conservative(config.structLearnConfig, config.inReasonableMix)
        "Non-Conservative" -> NonConservative(config.structLearnConfig)
        else -> throw IllegalArgumentException("Invalid Parent Ext Strat Given")
    }

    val dnUpdater = if(!config.truePolicy){
        BuntineUpdate(
            initialVocab,
            parentExtStrat,
            config.structuralUpdateFrequency,
            config.structLearnConfig,
            config.enforceConnectivity)
    }
    else{
        StaticUpdate(trueDN)
    }

    val agent = Agent(initialVocab, agentInitRewardDom, config.agentExploration, dnUpdater)
    val expert = Expert(trueDN, config.expertMessagesMinInterval, config.subOptimalThresh)


    val learnedDN = runSystem(trueDN,agent, expert, taskConfig.numTrials, config.truePolicy)

    ResultsLogger.logDN(learnedDN)
}

fun runBNExperiment(experimentName: String, taskId: String, taskConfig: TaskConfig, config: BNExperimentConfig){
    ResultsLogger.setPath("logs/$experimentName", taskId)

    val trueBN = loadJsonBN(taskConfig.trueGraphPath)

    val agentStartInfo = loadJson(taskConfig.agentStarterPath, AgentStartInfo::class.java)
    val initialVocab = trueBN.vocab.filter { it.name in agentStartInfo.vocab }.toSet()

    // This switch could actually be done with better polymorphic json attributes (see sequential codebase)
    val parentExtStrat = when(config.parentExtensionStrategy){
        "Conservative" -> Conservative(config.structLearnConfig, config.inReasonableMix)
        "Non-Conservative" -> NonConservative(config.structLearnConfig)
        else -> throw IllegalArgumentException("Invalid Parent Ext Strat Given")
    }

    val dnUpdater = BuntineUpdate(
        initialVocab,
        parentExtStrat,
        config.structuralUpdateFrequency,
        config.structLearnConfig,
        false)

    val trueTrials = generateSamples(trueBN, taskConfig.numTrials).map { Trial(it, 1.0) }
    val projectedTrials = ArrayList<Trial>()
    val klTrials = generateSamples(trueBN, 5000)
    val klTrueLnProbs = klTrials.map { simpleLnQueryFull(it, trueBN) }

    val emptyRT = RewardTable(listOf(), listOf(1.0))
    var learnedDN = (dnUpdater.update(emptyRT, projectedTrials, emptyList()) as UpdateResult.Success).result

    for((t, trial) in trueTrials.withIndex()){
        println("Trial $t")
        // Regular Trial Update
        val projectedTrial = Trial(project(trial.assignment, learnedDN.vocab), 1.0)
        projectedTrials.add(projectedTrial)
        learnedDN = (dnUpdater.trialUpdate(emptyRT, projectedTrials, emptyList(), projectedTrial) as UpdateResult.Success).result

        // Introducing new variables
        val additionalVariables = (trueBN.vocab - learnedDN.vocab).shuffled()
        if(additionalVariables.isNotEmpty() && (t + 1) % config.vocabIntroInterval  == 0){
            val newVar = additionalVariables.random()
            learnedDN = (dnUpdater.addNewVars(learnedDN, setOf(newVar), emptyRT, projectedTrials, emptyList()) as UpdateResult.Success).result
            projectedTrials.clear()
        }

        val learnedBN = dnToBN(learnedDN)

        // Log KL Div and BD-score metrics
        val klDiv = KLDivFastCached(trueBN, learnedBN, klTrials, klTrueLnProbs)
        /*
        val batchBDScore = trueBN.vocab.sumByDouble { rv ->
            val pSet = learnedBN.nodes[rv]?.parents?.toSet() ?: emptySet()
            BDeuNodeScore(rv, pSet, CountTable(listOf(rv) + pSet, trueTrials.map { it.assignment }), 1.0)
        }
        val shd = structuralHammingDistance(trueBN, learnedBN)
        */

        ResultsLogger.logTimeStampedResult(t, klDiv, "klDiv")
        /*
        ResultsLogger.logTimeStampedResult(t, batchBDScore, "bdScore")
        ResultsLogger.logTimeStampedResult(t, shd.toDouble(), "shd")
        */
        println()
    }


    ResultsLogger.logBN(dnToBN(learnedDN))
}

fun dnToBN(dnResult : DecisionNetwork) : BayesNet {
    val bMap = dnResult
        .beliefNodes
        .associateBy { it.rv }
    return BayesNet(bMap)
}

fun loadJsonBN(filePath : String) : BayesNet {
    val mapper = jacksonObjectMapper().registerKotlinModule()
    val bayesNetData = mapper.readValue<Map<String, BNodeData>>(File(filePath))
    val vocabMap = bayesNetData.entries.associate { Pair(it.key, RandomVariable(it.key, it.value.domainSize)) }

    val bnMap = bayesNetData
        .mapKeys { vocabMap[it.key]!!}
        .mapValues { bd -> BNode(bd.key, bd.value.parents.map { vocabMap[it]!! }, bd.value.cpt)}
    return BayesNet(bnMap)
}

data class BNodeData(val name: String, val parents : List<String>, val cpt: List<Double>, val domainSize: Int)


@Suppress("UNCHECKED_CAST")
fun <T> classInstance(packageName : String, cName : String) : T =
    ClassLoader.getSystemClassLoader().loadClass("$packageName.$cName").newInstance() as T

fun vocabToNames(vocab: List<RandomVariable>): Map<String, RandomVariable> = vocab.associate {Pair(it.name, it)}

data class DNData (val rvs : List<RandomVariable>, val cpts: List<CPTData>, val utilities: List<RewardData>){
    @get:JsonIgnore val nameToRV = rvs.associate { Pair(it.name, it) }
}
data class CPTData(val name: String, val parents: List<String>, val values: List<Double>)
data class RewardData (val scope: List<String>, val rewards: List<Double>)

fun readDN(filePath: String): DecisionNetwork {
    val dnData = loadJson(filePath, DNData::class.java)

    val actions = dnData.rvs.filter { it.varType == VarType.ACTION }
    val chanceNodes = dnData.cpts.map { binaryCPTDatatoBNode(it, dnData.nameToRV) }

    // Support for multiple utility nodes not yet supported
    val firstUtil = dnData.utilities.first()
    val rewardTable = RewardTable(firstUtil.scope.map { dnData.nameToRV[it]!! }, firstUtil.rewards)
    return DecisionNetwork(rewardTable, chanceNodes, actions)
}

fun readSubNetwork(filePath: String, trueDNRVs: Map<String, RandomVariable>): DecisionNetwork {
    val dnData = loadJson(filePath, DNData::class.java)

    val actions = dnData.rvs.map { trueDNRVs[it.name]!! }.filter { it.varType == VarType.ACTION }
    val chanceNodes = dnData.cpts.map { binaryCPTDatatoBNode(it, trueDNRVs) }

    // Support for multiple utility nodes not yet supported
    val firstUtil = dnData.utilities.first()
    val rewardTable = RewardTable(firstUtil.scope.map { trueDNRVs[it]!! }, firstUtil.rewards)

    return DecisionNetwork(rewardTable, chanceNodes, actions)
}

fun DNToDNData(dn : DecisionNetwork) : DNData{
    return DNData(
        dn.vocab,
        dn.sortedChanceNodes.map(::BNodeToBinaryCPTData),
        listOf(RewardData(dn.rewardFunction.scope.map { it.name }, dn.rewardFunction.values))
    )
}

fun binaryCPTDatatoBNode(cptData : CPTData, rvs : Map<String, RandomVariable>) : BNode {
    if(rvs[cptData.name]!!.domainSize != 2){
        throw IllegalArgumentException("Non binary variable used in binary node conversion: ${cptData.name}")
    }
    return BNode(rvs[cptData.name]!!, cptData.parents.map { rvs[it]!! }, cptData.values.flatMap { listOf(1.0 - it, it) })
}

fun BNodeToBinaryCPTData(bNode : BNode) : CPTData{
    if(bNode.rv.domainSize != 2){
        throw IllegalArgumentException("Non binary variable used in binary node conversion: ${bNode.rv}")
    }
    return CPTData(bNode.rv.name, bNode.parents.map { it.name }, bNode.cpt.everyNth(2))
}

// For Learning Decision Networks
fun main(args: Array<String>) {
    if(args.size == 3){
        val (taskId, taskConfigPath, configPath) = args
        val taskConfig = loadJson(taskConfigPath, TaskConfig::class.java)
        val config = loadJson(configPath, ExperimentConfig::class.java)
        val configName = FilenameUtils.getBaseName(configPath)
        val dnName = FilenameUtils.getBaseName(taskConfig.trueGraphPath)
        val experimentName = "$dnName-$configName"
        runExperiment(experimentName, taskId, taskConfig, config)
    }
    else{
        throw IllegalArgumentException("Incorrect Arguments: <Task-Id> <Task-Config-Path> <Config-Path>")
    }
}

/*
// For learning Bayes Nets
fun main(args: Array<String>){
    if(args.size == 3){
        val (taskId, taskConfigPath, configPath) = args
        val taskConfig = loadJson(taskConfigPath, TaskConfig::class.java)
        val config = loadJson(configPath, BNExperimentConfig::class.java)
        val configName = FilenameUtils.getBaseName(configPath)
        val bnName = FilenameUtils.getBaseName(taskConfig.trueGraphPath)
        val experimentName = "$bnName-$configName"
        runBNExperiment(experimentName, taskId, taskConfig, config)
    }
    else{
        throw IllegalArgumentException("Incorrect Arguments: <Task-Id> <Task-Config-Path> <Config-Path>")
    }
}
*/
