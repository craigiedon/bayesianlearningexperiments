import java.util.*

typealias Forest<V> = List<RootedTree<V>>
data class RootedTree<V>(val root : V, val childMap: Map<V, Set<V>>)

fun <V> spanningForestWithRoots(dirGraph : Map<V, Collection<V>>) : Forest<V> {
    val unvisited = LinkedList(dirGraph.keys)
    val rootedTrees = ArrayList<RootedTree<V>>()

    while (unvisited.isNotEmpty()) {
        // Pick an unvisited variable as new root
        val root = unvisited.pop()
        val childMap = HashMap<V, Set<V>>()
        val nextToProcess = LinkedList<V>(listOf(root))

        // Walk along all connected variables that havent been visited yet
        while (nextToProcess.isNotEmpty()) {
            val item = nextToProcess.poll()
            val unexploredNeighbours = dirGraph[item]!!.filter { it in unvisited }.toSet()

            childMap[item] = unexploredNeighbours
            unvisited.removeAll(unexploredNeighbours)
            nextToProcess.addAll(unexploredNeighbours)
        }
        rootedTrees.add(RootedTree(root, childMap))
    }

    return rootedTrees
}
