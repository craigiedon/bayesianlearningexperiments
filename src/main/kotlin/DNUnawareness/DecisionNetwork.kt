package DNUnawareness

import RewardTable
import topologicalSort
import BNode
import Factor
import RandomVariable
import RVAssignment
import allAssignments
import applyEvidence
import evidenceApplicationPrune
import extractFactors
import variableEliminationQuery
import kotlin.streams.asSequence

class DecisionNetwork(val rewardFunction : RewardTable, private val unsortedChanceNodes: Collection<BNode>, val actions: List<RandomVariable>){
    val sortedChanceNodes : List<BNode> = topologicalSort(unsortedChanceNodes.toList())
    val beliefNodes  : List<BNode> = sortedChanceNodes.filter { it.rv.varType == VarType.BELIEF }
    val outcomeNodes : List<BNode> = sortedChanceNodes.filter { it.rv.varType == VarType.OUTCOME }
    val beliefs = sortedChanceNodes.map { it.rv }.filter { it.varType == VarType.BELIEF }
    val outcomes = sortedChanceNodes.map { it.rv }.filter { it.varType == VarType.OUTCOME }
    val chanceVars = beliefs + outcomes
    val vocab get() = actions + beliefs + outcomes
    fun nodeForRV(rv : RandomVariable) = (unsortedChanceNodes).find { it.rv == rv }
}

fun children(dn : DecisionNetwork, rv : RandomVariable) : List<RandomVariable> =
    dn.sortedChanceNodes
        .filter { rv in it.parents }
        .map { it.rv }

fun fillDefaults(actionSubset: RVAssignment, fullActions : List<RandomVariable>): RVAssignment {
    if(!fullActions.containsAll(actionSubset.keys)){
        throw IllegalArgumentException("Assigned actions must be subset of full actions")
    }
    val unmentionedActions = fullActions - actionSubset.keys
    return actionSubset + unmentionedActions.associate { Pair(it, 0) }
}

fun optimalAction(beliefAssignments: RVAssignment, dn : DecisionNetwork, availableActions: Collection<RandomVariable>): Pair<RVAssignment, Double> {
    // Pre-pruning factors as belief assignment is the same every time
    val factors = extractFactors(dn)
        .asSequence()
        .map { evidenceApplicationPrune(beliefAssignments, it, dn.rewardFunction.scope) }
        .map { applyEvidence(beliefAssignments, it) }
        .filter{ it.scope.isNotEmpty() }
        .toList()

    return allAssignments(availableActions.toList())
        .map { Pair(it, expectedUtility(dn.rewardFunction, fillDefaults(it, dn.actions) + beliefAssignments, factors))  }
        .maxBy { it.second }!!
}

fun expectedUtility(evidence: RVAssignment, actionAssignment: RVAssignment, dn : DecisionNetwork) : Double {
    val fullAction = fillDefaults(actionAssignment, dn.actions)
    val allEvidence = evidence + fullAction

    val rDomProbs = variableEliminationQuery(dn.rewardFunction.scope, allEvidence, extractFactors(dn))

    return allAssignments(dn.rewardFunction.scope)
        .sumByDouble{ assignment ->
            val prob = rDomProbs.getValue(assignment)
            val reward = dn.rewardFunction.getValue(assignment)
            prob * reward
        }
}

fun expectedUtility(rewardFunction : RewardTable, allEvidence: RVAssignment, factors : List<Factor>) : Double {
    val rDomProbs = variableEliminationQuery(rewardFunction.scope, allEvidence, factors)

    return allAssignments(rewardFunction.scope)
        .sumByDouble { assignment ->
            val prob = rDomProbs.getValue(assignment)
            val reward = rewardFunction.getValue(assignment)
            prob * reward
        }
}
