package DNUnawareness

import BDeuNodeScore
import RVAssignment
import RandomVariable
import Trial
import Factor
import CountTable
import DirEdge
import variableEliminationQuery

data class ParentSetInfo(val child: RandomVariable, val parentSet: Set<RandomVariable>, val logProbability : Double, val countTable : CountTable, val jointParamPrior : Factor){
    fun count(childAssignment: Int, otherAssignments: RVAssignment) =
        countTable.getCount(otherAssignments + Pair(child, childAssignment))

    fun count(parentAssignments: RVAssignment) =
        child.domain.sumByDouble { count(it, parentAssignments) }

    fun auxJoint(childAssignment: Int, parentAssignments: RVAssignment) =
        jointParamPrior.getValue(parentAssignments + Pair(child, childAssignment))

    fun auxMarginal(parentAssignments: RVAssignment) =
        child.domain.sumByDouble{ auxJoint(it, parentAssignments) }

    fun addTrial(trial: Trial) {
        countTable.updateCounts(trial.assignment)
    }

    fun addTrials(trials: List<Trial>) {
        trials.forEach{ addTrial(it) }
    }
}

fun createPSetInfo(child : RandomVariable, pSet: PSet, trials : List<Trial>, logPrior: LogPrior<PSet>, paramPrior: List<Factor>, priorSampleSize : Double, expertEv : List<DirEdge>) : ParentSetInfo{
    val scope = listOf(child) + pSet
    val countTable = CountTable(scope, trials.map { it.assignment })
    val auxPriorParam = variableEliminationQuery(scope, emptyMap(), paramPrior)

    val score = if(violatesEvidence(child, pSet, expertEv)){
        Math.log(0.0)
    }
    else{
        logPrior(pSet) + BDeuNodeScore(child, pSet, countTable, auxPriorParam, priorSampleSize)
    }
    return ParentSetInfo(child, pSet, score, countTable, auxPriorParam)
}
