package DNUnawareness

import RVAssignment
import RandomVariable
import Trial
import VarType
import generateSample
import java.util.*
import AssignmentReward
import DirEdge
import RewardTable
import Utils.productBy
import Utils.project
import Utils.repeatNum
import java.lang.IllegalStateException


class ExistsState(override var assignment: RVAssignment, override val reward: Double, timeStamp: Int) : AssignmentReward {
    val domainTrialTimes = hashSetOf(timeStamp)
}

class ExistsBetterState(var action: RVAssignment, var beliefs: RVAssignment, rewardGT: Double) : AssignmentReward {
    override val assignment: RVAssignment get() = action + beliefs
    override val reward: Double = rewardGT
}

data class BetterActionInference(val worseAction: RVAssignment, val betterAction: RVAssignment, val beliefs: RVAssignment, val trialTime: TimeStamp)

class Agent(initVocab: Set<RandomVariable>,
            initRewardScope : Set<RandomVariable>,
            val exploreProp: Double,
            val dnUpdater: DNUpdater) {


    val trialHistory = ArrayList<Pair<Trial, TimeStamp>>()
    val trials get() = trialHistory.map { it.first }
    val vocab : LinkedHashSet<RandomVariable> = LinkedHashSet(initVocab)

    val actions get() = vocab.filter { it.varType == VarType.ACTION }.toSet()
    val beliefs get() = vocab.filter { it.varType == VarType.BELIEF }.toSet()
    val outcomes get() = vocab.filter { it.varType == VarType.OUTCOME }.toSet()
    val chanceVars get() = beliefs + outcomes


    //**** Monotonic Information ****
    val existsStateKnowledge = ArrayList<ExistsState>()
    val betterActionKnowledge = ArrayList<ExistsBetterState>()
    val parenthoodKnowledge: MutableList<DirEdge> = ArrayList()
    //********

    val betterActionInferences = ArrayList<BetterActionInference>()

    var dn : DecisionNetwork

    val rewardScope get() = dn.rewardFunction.scope

    init{
        val initRT = RewardTable(initRewardScope.toList(), repeatNum(0.0, initRewardScope.productBy { it.domainSize }))
        val dnResult = dnUpdater.update(initRT, trials, parenthoodKnowledge)
        dn = when(dnResult){
            is UpdateResult.Success -> dnResult.result
            else -> throw IllegalStateException("Agent should not fail to make DN on initialization")
        }
    }
}


fun checkForMisunderstandings(actionInferences: List<BetterActionInference>, newInf: BetterActionInference) : Pair<TimeStamp,TimeStamp>?{
    for (oldInf in actionInferences) {
        val beliefsTheSame = oldInf.beliefs == newInf.beliefs
        val actionsConflict = oldInf.worseAction == newInf.betterAction &&
            oldInf.betterAction == newInf.worseAction

        if (beliefsTheSame && actionsConflict) {
            //ResultsLogger.addDialogueMessage(String.format("Agent asks expert to resolve misunderstanding between\n oldAdvice: %s and\n newAdvice: %s\n", oldInference, newInference))
            println("Old inference: ${oldInf.beliefs}, ${oldInf.betterAction} > ${oldInf.worseAction}")
            println("New inference: ${newInf.beliefs}, ${newInf.betterAction} > ${newInf.worseAction}")
            return Pair(oldInf.trialTime, newInf.trialTime)
        }
    }

    return null
}

fun eGreedy(dn: DecisionNetwork, exploreProp : Double, actionInferences : List<BetterActionInference>, projectedBeliefs: RVAssignment): RVAssignment {
    val exploitVsExploreDecision = Math.random()
    val relevantAdvice = actionInferences.findLast { it.beliefs == projectedBeliefs }
    return when{
        exploitVsExploreDecision < exploreProp -> generateSample(dn.actions)
        relevantAdvice != null -> relevantAdvice.betterAction
        else -> optimalAction(projectedBeliefs, dn, dn.actions).first
    }
}

fun updateAgentTrials(agent : Agent, trial: Trial, timeStamp: TimeStamp) {
    val projectedTrial = trial.copy(assignment = project(trial.assignment, agent.vocab))

    agent.trialHistory.add(Pair(projectedTrial, timeStamp))

    val alreadyKnownState = agent.existsStateKnowledge
        .find { s -> s.assignment == projectedTrial.assignment && s.reward == projectedTrial.reward }

    if (alreadyKnownState != null) {
        alreadyKnownState.domainTrialTimes.add(timeStamp)
    }
    else {
        agent.existsStateKnowledge.add(ExistsState(projectedTrial.assignment, projectedTrial.reward, timeStamp))
    }
}

fun updateHistoryWithNewAction(agent : Agent, actionVars: Collection<RandomVariable>) {
    val newActionDefault = actionVars.associateWith { 0 }
    for((i, dt) in agent.trialHistory.withIndex()){
        val (trial, timeStamp) = dt
        agent.trialHistory[i] = Pair(trial.copy(assignment = trial.assignment + newActionDefault), timeStamp)
    }

    for((i, advice) in agent.betterActionInferences.withIndex()){
        agent.betterActionInferences[i] = advice.copy(
            worseAction = advice.worseAction + newActionDefault,
            betterAction = advice.betterAction + newActionDefault
        )
    }
}
