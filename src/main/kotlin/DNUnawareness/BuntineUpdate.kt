package DNUnawareness

import RandomVariable
import RewardTable
import java.util.*
import Trial
import allAssignments
import BNode
import DirEdge
import Factor
import extractFactors
import uniformJoint
import java.lang.IllegalArgumentException

sealed class UpdateResult<T,S> {
    class Success<T,S>(val result: T) : UpdateResult<T, S>()
    class FailureWithReason<T,S>(val reason : S) : UpdateResult<T,S>()
    class FailureAmbiguous<T,S> : UpdateResult<T,S>()
}

typealias  DNUpdateResult = UpdateResult<DecisionNetwork, List<RandomVariable>>

data class StructLearnConfig(
    val aliveThresh : Double,
    val alphaStrength: Double,
    val singleParentCost : Double,
    val maxParents : Int
)

interface DNUpdater{
    fun addNewVars(latestDN : DecisionNetwork, extraVars: Set<RandomVariable>, rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>) : DNUpdateResult
    fun trialUpdate(rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>, newTrial: Trial)  : DNUpdateResult
    fun update(rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>) : DNUpdateResult
}

class StaticUpdate(private val decisionNetwork: DecisionNetwork) : DNUpdater{
    override fun trialUpdate(rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>, newTrial: Trial): DNUpdateResult {
        return UpdateResult.Success(decisionNetwork)
    }

    override fun update(rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>): DNUpdateResult {
        return UpdateResult.Success(decisionNetwork)
    }

    override fun addNewVars(latestDN: DecisionNetwork, extraVars: Set<RandomVariable>, rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>): DNUpdateResult {
        return UpdateResult.Success(decisionNetwork)
    }

}

class BuntineUpdate(
    private var vocab: Set<RandomVariable>,
    private var parentExtStrat: ParentExtensionStrategy,
    private val structUpdFreq: Int,
    private val structLearnConfig: StructLearnConfig,
    private val enforceConnectivity : Boolean
) : DNUpdater {
    //For each RV, its list of reasonable parent sets from the previous round (along with their probabilities, and CPTS)
    private var paramPriors = vocab.map { Factor(listOf(it), uniformJoint(listOf(it))) }
    private var structPrior = vocab.associateWith { minParentsPrior(it, vocab, structLearnConfig.singleParentCost) }

    var reasonableParents = vocab
        .associateWith {  structUpdate(it, structPrior[it]!!, vocab, emptyList(), emptyList(), paramPriors, structLearnConfig) }
    var timeSinceLastStructuralUpdate = 0

    override fun addNewVars(latestDN : DecisionNetwork, extraVars : Set<RandomVariable>, rewardTable : RewardTable, trials : List<Trial>, parentEv : List<DirEdge>) : DNUpdateResult{
        vocab += extraVars
        val (newReasonableParents, newStructPrior, newParamPrior) = parentExtStrat
            .extendParents(this.structPrior, extractFactors(latestDN), extraVars, parentEv, trials)


        structPrior = newStructPrior
        paramPriors = newParamPrior
        reasonableParents = newReasonableParents


        // Should this take the trials or not?
        return when(val ilpResult = bestDNStruct(reasonableParents, parentEv,  rewardTable.scope.toSet(), enforceConnectivity)){
            is UpdateResult.Success -> UpdateResult.Success(buildDN(rewardTable, ilpResult.result, structLearnConfig.alphaStrength))
            is UpdateResult.FailureWithReason -> UpdateResult.FailureWithReason(ilpResult.reason)
            is UpdateResult.FailureAmbiguous -> UpdateResult.FailureAmbiguous()
        }
    }


    override fun trialUpdate(rewardTable: RewardTable, trials: List<Trial>, parentEv: List<DirEdge>, newTrial: Trial): DNUpdateResult {
        // Step 2.1: Parameter Update Algorithm
        this.reasonableParents = vocab.associateWith { parameterUpdate(it, this.reasonableParents[it]!!, structLearnConfig.alphaStrength, newTrial, parentEv) }

        // Step 2.2: Structural Update Algorithm
        timeSinceLastStructuralUpdate += 1
        if (timeSinceLastStructuralUpdate >= structUpdFreq || this.reasonableParents.values.any{it.isEmpty()}) {
            timeSinceLastStructuralUpdate = 0
            this.reasonableParents = vocab
                .associateWith { structUpdate(it, structPrior[it]!!, vocab, trials, parentEv, paramPriors, structLearnConfig) }

        }

        // Step 3: Integer Linear Programming
        val ilpResult = bestDNStruct(this.reasonableParents, parentEv, rewardTable.scope.toSet(), enforceConnectivity)
        return when(ilpResult){
            is UpdateResult.Success -> UpdateResult.Success(buildDN(rewardTable, ilpResult.result, structLearnConfig.alphaStrength))
            is UpdateResult.FailureWithReason -> UpdateResult.FailureWithReason(ilpResult.reason)
            is UpdateResult.FailureAmbiguous -> UpdateResult.FailureAmbiguous()
        }
    }

    override fun update(rewardTable : RewardTable, trials: List<Trial>, parentEv: List<DirEdge>) : DNUpdateResult {
        // Step 2.1: Parameter Update Algorithm
        this.reasonableParents = vocab.associateWith { parameterUpdate(it, this.reasonableParents[it]!!, structLearnConfig.alphaStrength, null, parentEv) }

        // Step 2.2: Structural Update Algorithm
        if (this.reasonableParents.values.any{it.isEmpty()}) {
            timeSinceLastStructuralUpdate = 0
            this.reasonableParents = vocab
                .associateWith { structUpdate(it, structPrior[it]!!, vocab, trials, parentEv, paramPriors, structLearnConfig) }

        }

        // Step 3: Integer Linear Programming
        val ilpResult = bestDNStruct(this.reasonableParents, parentEv, rewardTable.scope.toSet(), enforceConnectivity)
        return when(ilpResult){
            is UpdateResult.Success -> UpdateResult.Success(buildDN(rewardTable, ilpResult.result, structLearnConfig.alphaStrength))
            is UpdateResult.FailureWithReason -> UpdateResult.FailureWithReason(ilpResult.reason)
            is UpdateResult.FailureAmbiguous -> UpdateResult.FailureAmbiguous()
        }
    }

}

fun cptFromCounts(rv: RandomVariable, pSetInfo: ParentSetInfo, priorTotalStrength: Double): BNode {
    val orderedParents = pSetInfo.parentSet.toList()
    val thetaVals = allAssignments(orderedParents).flatMap { parentAssignment ->
        val countMarginal = pSetInfo.count(parentAssignment)
        val auxMarginal = pSetInfo.auxMarginal(parentAssignment)

        rv.domain.map { childAssignment ->
            val alphaJoint = priorTotalStrength * pSetInfo.auxJoint(childAssignment, parentAssignment)
            val alphaMarginal = priorTotalStrength * auxMarginal
            val countJoint = pSetInfo.count(childAssignment, parentAssignment)

            //Conditional probability of child given its parents
            (countJoint + alphaJoint) / (countMarginal + alphaMarginal)
        }
    }

    return BNode(rv, orderedParents, thetaVals)
}

fun buildDN(rewardTable: RewardTable, bestParents : Map<RandomVariable, ParentSetInfo>, alphaStrength: Double) : DecisionNetwork {
    val beliefNodes = bestParents
        .filterKeys { it.varType != VarType.ACTION }
        .map { cptFromCounts(it.key, it.value, alphaStrength) }
    val actions = bestParents.keys.filter { it.varType == VarType.ACTION }
    return DecisionNetwork(rewardTable, beliefNodes, actions)
}

fun parameterUpdate(child: RandomVariable, reasonableParentSets: List<ParentSetInfo>,
                    alphaStrength: Double, newSample: Trial? = null, expertEvidence: List<DirEdge>) : List<ParentSetInfo>{

    val newReasonableParentSets =
        if(newSample == null){
            reasonableParentSets
        }
        else{
            reasonableParentSets.map { pSetInfo ->
                //Sample counts are updated incrementally here
                pSetInfo.addTrial(newSample)

                val sampleParentAssignment = newSample.assignment.filterKeys { pSetInfo.parentSet.contains(it) }
                val sampleChildAssignment = newSample.assignment[child]!!

                val countJoint = pSetInfo.count(sampleChildAssignment, sampleParentAssignment)
                val countMarginal = pSetInfo.count(sampleParentAssignment)
                val alphaJoint = alphaStrength * pSetInfo.auxJoint(sampleChildAssignment, sampleParentAssignment)
                val alphaMarginal = alphaStrength * pSetInfo.auxMarginal(sampleParentAssignment)

                //Update Structural Probability
                val newLogProb = pSetInfo.logProbability + Math.log(countJoint + alphaJoint - 1) - Math.log(countMarginal + alphaMarginal - 1)
                pSetInfo.copy(logProbability = newLogProb)
            }
        }

    return logNormPSetInfos(newReasonableParentSets
        .filter{ !violatesEvidence(child, it.parentSet, expertEvidence)}
    )
}

fun violatesEvidence(child : RandomVariable, parentSet : PSet, parentEvidence: List<DirEdge>) =
    parentEvidence.any { (pEv, cEv) ->
        val requiredParentViolation = cEv == child && pEv !in parentSet
        val asymmetryViolation = pEv == child && cEv in parentSet
        requiredParentViolation || asymmetryViolation
    }

data class LatticeNode<V>(val item : V, val parents : MutableList<LatticeNode<V>> = ArrayList(), val children : MutableList<LatticeNode<V>> = ArrayList())
typealias PSetNode = LatticeNode<Set<RandomVariable>>
typealias PSet = Set<RandomVariable>

fun <V> createLatticeNode(item : V, parents: List<LatticeNode<V>>) : LatticeNode<V>{
    val node = LatticeNode(item, parents.toMutableList())
    parents.forEach { it.children.add(node) }
    return node
}

fun structUpdate(X: RandomVariable,
                 logPrior: LogPrior<PSet>,
                 vocab: Set<RandomVariable>,
                 trials: List<Trial>,
                 expertEv: List<DirEdge>,
                 paramPrior: List<Factor>,
                 config: StructLearnConfig): List<ParentSetInfo> {

    val openList = LinkedList<PSetNode>()
    var aliveList = emptyList<PSetNode>()

    val lattice = HashMap<PSet, PSetNode>()
    val pSetInfos = HashMap<PSet, ParentSetInfo>()

    val minNode = PSetNode(minimalParentSet(X, expertEv))
    if(minNode.item.size > config.maxParents){
        throw IllegalArgumentException("Impossible to enforce mandatory parents of ${minNode.item} with max parents size of ${config.maxParents}")
    }

    lattice[minNode.item] = minNode
    pSetInfos[minNode.item] = createPSetInfo(X, minNode.item, trials, logPrior, paramPrior, config.alphaStrength, expertEv)
    var bestScore = pSetInfos[minNode.item]!!.logProbability
    aliveList += minNode

    val minNodeExtensions = vocab
        .filter { it != X && it !in minNode.item }
        .filter { (X.varType == VarType.OUTCOME) ||  X.varType == VarType.BELIEF && it.varType == VarType.BELIEF }
        .map { createLatticeNode(minNode.item + it, listOf(minNode)) }
        .filter { it.item.size <= config.maxParents }

    openList.addAll(minNodeExtensions)

    while(openList.isNotEmpty()){
        val node = openList.poll()

        if(node.item !in pSetInfos){
            val pInfo = createPSetInfo(X, node.item, trials, logPrior, paramPrior, config.alphaStrength, expertEv)
            pSetInfos[node.item] = pInfo

            if(pInfo.logProbability > Math.log(config.aliveThresh)  + bestScore){
                aliveList += node
                // Add all reasonable pSet's extensions to open list for consideration
                for(sibling in siblings(node)){
                    val pSetUnion = node.item + sibling.item
                    if(pSetUnion !in lattice && pSetUnion.size <= config.maxParents){
                        val unionNode = createLatticeNode(pSetUnion, listOf(node, sibling))
                        lattice[pSetUnion] = unionNode
                        openList.add(unionNode)
                    }
                }
            }

            // A higher best score means that nodes we previously thought reasonable might now be unreasonable
            if(pInfo.logProbability > bestScore){
                bestScore = pInfo.logProbability
                aliveList = revisedAliveList(config.aliveThresh, aliveList, pSetInfos)
            }
        }
    }

    val reasonablePSets = reasonablePSets(LinkedList(aliveList))

    // Question: Should this be normed or not?
    return logNormPSetInfos(reasonablePSets.map { pSetInfos[it]!!})
}

fun <V> siblings(node : LatticeNode<V>) : List<LatticeNode<V>> =
    node.parents.flatMap { p -> p.children.filter { it.item != node.item } }


fun isParentSetProvablyFalse(child: RandomVariable, parents: Set<RandomVariable>, explicitParentEvidence: List<DirEdge>): Boolean {
    val actionHasParents = child.varType == VarType.ACTION && !parents.isEmpty()
    val beliefHasNonBeliefParents = child.varType == VarType.BELIEF && parents.any { it.varType != VarType.BELIEF }
    val missingRequiredParent = explicitParentEvidence.any{(evParent, evChild) -> evChild == child && !parents.contains(evParent)}
    val symmetryViolation = explicitParentEvidence.any{(evParent, evChild) -> evParent == child && evChild in parents}

    return actionHasParents || beliefHasNonBeliefParents || missingRequiredParent || symmetryViolation
}

fun isAlive(logProbability: Double, aliveThresh: Double, logBest: Double) =
    logProbability > Math.log(aliveThresh) + logBest

fun minimalParentSet(X: RandomVariable, explicitParentEvidence: List<DirEdge>): Set<RandomVariable> {
    return explicitParentEvidence.filter { (_, child) -> child == X }.map { (parent, _) -> parent }.toSet()
}

fun revisedAliveList(aliveThresh: Double, aliveList: List<PSetNode>, pSetInfos: Map<PSet, ParentSetInfo>) : List<PSetNode> {
    val bestScore = pSetInfos.values.map { it.logProbability }.max()!!
    return aliveList.filter { isAlive(pSetInfos[it.item]!!.logProbability, aliveThresh, bestScore) }
}

fun reasonablePSets(aliveCandidates: List<PSetNode>): Set<PSet> {
    val remainingCandidates = LinkedList(aliveCandidates)
    val resultingReasonableParents = HashSet<PSet>()

    // A depth-first search of lattice structure starting from all alive parents
    /* TODO: This is not necessarily "all subsets of a reasonable parent set"
       rather, it is all subsets *within the constructed lattice*.
     */
    while (remainingCandidates.isNotEmpty()) {
        val candidate = remainingCandidates.pollFirst()
        if (candidate.item !in resultingReasonableParents) {
            resultingReasonableParents.add(candidate.item)
            for (subset in candidate.parents) {
                remainingCandidates.addFirst(subset)
            }
        }
    }
    return resultingReasonableParents
}
