import DNUnawareness.*
import Utils.currentTimeSecs
import Utils.project
import java.lang.IllegalStateException
import java.util.*

fun runSystem(trueDN: DecisionNetwork, agent: Agent, expert: Expert, numTrials: Int, truePolicy : Boolean): DecisionNetwork {
    val optimalActEUs = ArrayList<Double>()
    val actionTakenEUs = ArrayList<Double>()
    //val preComputedEUs = preComputeExpectedUtilities(trueDN)

    for (timeStep in 0 until numTrials) {
        val trialStartTime = currentTimeSecs()
        println(timeStep)
        println("Vocab Size ${agent.vocab.size}")

        // Generate sample and choose action
        val beliefAssignments = generateSample(trueDN.beliefNodes)

        val actionAssignments = if(!truePolicy)
            fillDefaults(
                eGreedy(agent.dn, agent.exploreProp, agent.betterActionInferences, project(beliefAssignments, agent.beliefs)),
                trueDN.actions
            )
        else
            eGreedy(trueDN, agent.exploreProp, emptyList(), beliefAssignments)

        val outcomeAssignments = generateSample(trueDN.outcomeNodes, beliefAssignments + actionAssignments)
        val allAssignments = beliefAssignments + actionAssignments + outcomeAssignments
        val reward = trueDN.rewardFunction.getValue(allAssignments)

        val newTrial = Trial(allAssignments, reward)

        // Calculating Whether optimal action was taken
        optimalActEUs.add(optimalAction(beliefAssignments, trueDN, trueDN.actions).second) //preComputedEUs[beliefAssignments]!!.optimalAction.second)
        actionTakenEUs.add(expectedUtility(beliefAssignments, actionAssignments, trueDN))

        ResultsLogger.logTimeStampedResult(timeStep, reward, "Rewards")

        // We don't need to bother with any of the learning stuff if we already know the true policy
        if(truePolicy){
            continue
        }

        // Update Agent/Expert Histories
        updateExpertTrials(expert, newTrial, timeStep)
        updateAgentTrials(agent, newTrial, timeStep)

        // Update DN Based on Trial Count
        agent.dn = updateDN(agent.trialHistory.last().first, agent, expert, timeStep)


        // Update Reward Function Based on Latest Trials and Advice
        var newRewardTable = unfactoredReward(agent.rewardScope, agent.betterActionKnowledge, agent.existsStateKnowledge)
        if(newRewardTable == null){
            //ResultsLogger.addDialogueMessage(String.format("Agent asks for more variables in reward domain.\n Current reward domain: %s\n", agent.rewardDomainKnowledge))
            val newRDomVar = askForRDomVar(expert, agent.dn.rewardFunction.scope.toSet(), timeStep)
            newRewardTable = unfactoredReward(agent.rewardScope + newRDomVar, agent.betterActionKnowledge, agent.existsStateKnowledge)!!
            if(newRDomVar !in agent.vocab){
                if(newRDomVar.varType == VarType.BELIEF){
                    agent.betterActionInferences.clear()
                }
                agent.vocab.add(newRDomVar)
                agent.dn = updateDN(setOf(newRDomVar), agent, newRewardTable, expert, timeStep)
            }
        }
        agent.dn = updateDN(agent, newRewardTable, expert,timeStep)


        // Expert Will Interject if Agent Performance is Sufficiently Poor
        if (expertWantsToInterject(expert, optimalActEUs, actionTakenEUs, timeStep, newTrial, trueDN)) {

            // Add monotonic / defeasible inferences of action advice and update DN if new vocab introduced
            val betterAction = betterActionAdvice(expert, newTrial, timeStep)
            val newActionVars = betterAction.keys - agent.actions
            if(newActionVars.isNotEmpty()){
                agent.vocab.addAll(newActionVars)
                updateHistoryWithNewAction(agent, newActionVars)
                agent.dn = updateDN(newActionVars, agent, agent.dn.rewardFunction, expert, timeStep)
            }
            val relevantState : ExistsState = agent.existsStateKnowledge.find { timeStep in it.domainTrialTimes }!!
            val worseAction = relevantState.assignment.filterKeys { it.varType == VarType.ACTION }
            val beliefContext = relevantState.assignment.filterKeys { it.varType == VarType.BELIEF }
            agent.betterActionKnowledge.add(ExistsBetterState(betterAction, beliefContext, relevantState.reward))
            agent.betterActionInferences.add(BetterActionInference(worseAction, betterAction, beliefContext, timeStep))
            ResultsLogger.logBetterActionAdv(timeStep, agent.betterActionInferences.last())


            // Better Action advice could conflict with previous advice
            val misunderstanding = checkForMisunderstandings(agent.betterActionInferences, agent.betterActionInferences.last())
            if(misunderstanding != null){
                val advice = resolveMisunderstanding(expert, misunderstanding.first, misunderstanding.second, timeStep)

                agent.vocab.add(advice.beliefVariable)
                agent.betterActionInferences.clear()

                agent.dn = updateDN(setOf(advice.beliefVariable), agent, agent.dn.rewardFunction, expert, timeStep)

                val olderSRKnowledge = agent.existsStateKnowledge.find { misunderstanding.first in it.domainTrialTimes }!!
                val newerSRKnowledge = agent.existsStateKnowledge.find { misunderstanding.second in it.domainTrialTimes }!!
                olderSRKnowledge.assignment = olderSRKnowledge.assignment + Pair(advice.beliefVariable, advice.newBeliefVal)
                newerSRKnowledge.assignment = newerSRKnowledge.assignment + Pair(advice.beliefVariable, advice.newBeliefVal)
            }
        }

        ResultsLogger.logTimeStampedResult(timeStep, currentTimeSecs() - trialStartTime, "TimeSecs")
        ResultsLogger.logTimeStampedResult(timeStep, agent.dn.actions.size.toDouble(), "ActionsSize")
        ResultsLogger.logTimeStampedResult(timeStep, agent.dn.chanceVars.size.toDouble(), "ChanceVarsSize")
        ResultsLogger.logParentEv(timeStep, agent.parenthoodKnowledge)
    }

    return agent.dn
}

fun expertWantsToInterject(expert : Expert, optActEUs: List<Double>, agentActEUs: List<Double>, timeStep: Int, newTrial : Trial, trueDN: DecisionNetwork) : Boolean {
    val beliefAssignment = newTrial.assignment.filterKeys{ it in trueDN.beliefs }
    val actionAssignment = newTrial.assignment.filterKeys { it in trueDN.actions }
    println("Opt/Agent difference in EU: ${optActEUs.last() - agentActEUs.last()}")
    return (
        optActEUs.last() - agentActEUs.last() > 1E-10 &&
        subOptimalBehaviour(optActEUs, agentActEUs, expert.subOptimalDecisionsThreshold, expert.timeLastAdviceGiven, timeStep) &&
        timeStep - expert.timeLastAdviceGiven > expert.minTimeBetweenMessages &&
        newTrial.reward <= expectedUtility(beliefAssignment, actionAssignment, trueDN)
    )
}

fun updateDN(trial : Trial, agent : Agent, expert: Expert, timeStep: Int) : DecisionNetwork {
    val dnUpdateResult = agent.dnUpdater.trialUpdate(agent.dn.rewardFunction, agent.trials, agent.parenthoodKnowledge, trial)
    return handleDNResult(dnUpdateResult, agent, agent.dn.rewardFunction, expert, timeStep)
}

fun updateDN(agent : Agent, rewardFunc: RewardTable, expert : Expert, timeStep : Int) : DecisionNetwork {
    val dnUpdateResult = agent.dnUpdater.update(rewardFunc, agent.trials, agent.parenthoodKnowledge)
    return handleDNResult(dnUpdateResult, agent, rewardFunc, expert, timeStep)
}

fun updateDN(newVars : Set<RandomVariable>, agent : Agent, rewardFunc: RewardTable, expert : Expert, timeStep : Int) : DecisionNetwork {
    val dnUpdateResult = agent.dnUpdater.addNewVars(agent.dn, newVars, rewardFunc, agent.trials, agent.parenthoodKnowledge)
    if(newVars.any{ it.varType != VarType.ACTION }){
        agent.trialHistory.clear()
    }
    return handleDNResult(dnUpdateResult, agent, rewardFunc, expert, timeStep)
}

fun handleDNResult(dnUpdateResult: DNUpdateResult, agent: Agent, rewardFunc: RewardTable, expert: Expert, timeStep: Int) : DecisionNetwork =
    when(dnUpdateResult){
        is UpdateResult.Success -> dnUpdateResult.result
        is UpdateResult.FailureWithReason -> {
            val whatVarDoesAdvice = whatDoesVarDo(expert, dnUpdateResult.reason.first(), timeStep)
            agent.parenthoodKnowledge.add(whatVarDoesAdvice)
            if(whatVarDoesAdvice.second !in agent.vocab) {
                agent.vocab.add(whatVarDoesAdvice.second)
                updateDN(setOf(whatVarDoesAdvice.second), agent, rewardFunc, expert, timeStep)
            }
            else{
                updateDN(agent, rewardFunc, expert, timeStep)
            }
        }
        is UpdateResult.FailureAmbiguous -> {
            // Find the most recently added variable that we don't know an effect of
            val potentialAdviceVars = agent
                .vocab
                .filter { rv -> rv !in agent.rewardScope  && !agent.parenthoodKnowledge.any{ (p, _) -> rv == p}}

            if(potentialAdviceVars.isEmpty()){
                updateDN(agent, rewardFunc, expert, timeStep)
                throw IllegalStateException("Agent can't construct DN, but there doesn't seem to be any advice expert can give?")
            }

            val mostRecentVar = potentialAdviceVars.last()

            val whatVarDoesAdvice = whatDoesVarDo(expert, mostRecentVar, timeStep)
            agent.parenthoodKnowledge.add(whatVarDoesAdvice)
            if(whatVarDoesAdvice.second !in agent.vocab) {
                agent.vocab.add(whatVarDoesAdvice.second)
                updateDN(setOf(whatVarDoesAdvice.second), agent, rewardFunc, expert, timeStep)
            }
            else{
                updateDN(agent, rewardFunc, expert, timeStep)
            }
        }
    }

fun preComputeExpectedUtilities(dn: DecisionNetwork): Map<RVAssignment, ActionUtilityResults> {
    val beliefAssignments = allAssignments(dn.beliefs)
    val actionAssignments = allAssignments(dn.actions)

    return beliefAssignments.associateWith { beliefAssignment ->
        ActionUtilityResults(actionAssignments.associateWith { expectedUtility(beliefAssignment, it, dn) })
    }
}

data class ActionUtilityResults(val actionAssignmentEUs: Map<RVAssignment, Double>){
    val optimalAction = actionAssignmentEUs.maxBy { it.value }!!.toPair()
}
