package DNUnawareness
import RandomVariable

import com.google.ortools.linearsolver.MPSolver
import com.google.ortools.sat.CpModel
import com.google.ortools.sat.CpSolver
import com.google.ortools.sat.CpSolverStatus
import com.google.ortools.sat.IntVar

import java.util.*
import kotlin.math.roundToLong

fun normAndExponentiate(pSetInfoMap: Map<RandomVariable, List<ParentSetInfo>>): Map<RandomVariable, Map<PSet, Double>> =
    pSetInfoMap.mapValues { (_ , pSetInfos) ->
        logNormPSetInfos(pSetInfos).associate { Pair(it.parentSet, Math.exp(it.logProbability)) }
    }

object DNStructSolver {
    init {
        System.loadLibrary("jniortools")
    }


    private val solver = MPSolver("ILP Struct Solver", MPSolver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING)
    //private val solver = CpSolver()

    /*
    fun solveDNILP(reasonableParents: Map<RandomVariable, List<ParentSetInfo>>, rDom: Set<RandomVariable>, enforceConncectivity: Boolean) : Map<RandomVariable, ParentSetInfo>?{
        val nonRewardDom = reasonableParents.keys - rDom
        val model = CpModel()
        val pSetVars = reasonableParents.mapValues { (rv, pInfos) ->
            pInfos.associateWith { model.newBoolVar("${it.parentSet} -> $rv") }
        }

        val pSetArray = ArrayList<IntVar>()
        val pSetProbLongArray = ArrayList<Long>()
        for((_, pInfMap) in pSetVars){
            for((pInfo, pSetVar) in pInfMap){
                pSetArray.add(pSetVar)
                // Conversion from floating point to longs necessarily loses some accuracy...
                pSetProbLongArray.add((pInfo.logProbability * 10).roundToLong())
            }
        }

        // Maximize structure score
        model.maximizeScalProd(pSetArray.toTypedArray(), pSetProbLongArray.toLongArray())

        // Each RV has exactly one parent set
        for((_,pMap) in pSetVars){
            model.addLinearSumEqual(pMap.values.toTypedArray(), 1)
        }

        // Graph forms a DAG
        val beliefs = reasonableParents.keys.filter { it.varType == VarType.BELIEF }
        val outcomes = reasonableParents.keys.filter { it.varType == VarType.OUTCOME }

        val beliefOrderVars = beliefs.associateWith { model.newIntVar(1, beliefs.size.toLong(), "${it.name}-order") }
        model.addAllDifferent(beliefOrderVars.values.toTypedArray())

        val outcomeOrderVars = outcomes.associateWith { model.newIntVar(1, outcomes.size.toLong(), "${it.name}-order") }
        model.addAllDifferent(outcomeOrderVars.values.toTypedArray())

        // Chosen parent sets must obey some belief order
        for (child in beliefs) {
            for (parent in beliefs.filter { it != child }) {
                val relevantPVars = pSetVars.getValue(child)
                    .filterKeys { parent in it.parentSet }
                    .values

                val b = model.newBoolVar("$parent -> $child ord")
                model.addLinearSumEqual(relevantPVars.toTypedArray(), 1).onlyEnforceIf(b)
                model.addLinearSumEqual(relevantPVars.toTypedArray(), 0).onlyEnforceIf(b.not())

                model
                    .addLessOrEqual(beliefOrderVars.getValue(parent), beliefOrderVars.getValue(child))
                    .onlyEnforceIf(b)
            }
        }

        // Chosen parent sets must obey some outcome order
        for (child in outcomes) {
            for (parent in outcomes.filter { it != child }) {
                val relevantPVars = pSetVars.getValue(child)
                    .filterKeys { parent in it.parentSet }
                    .values

                val b = model.newBoolVar("$parent -> $child ord")
                model.addLinearSumEqual(relevantPVars.toTypedArray(), 1).onlyEnforceIf(b)
                model.addLinearSumEqual(relevantPVars.toTypedArray(), 0).onlyEnforceIf(b.not())

                model
                    .addLessOrEqual(outcomeOrderVars.getValue(parent), outcomeOrderVars.getValue(child))
                    .onlyEnforceIf(b)
            }
        }

        // All non reward dom vars must have at least one child
        if (enforceConncectivity) {
            for (nonRewardRV in nonRewardDom) {
                val possibleChildVars = pSetVars
                    .values
                    .flatMap { pInfos ->
                        pInfos.filterKeys { nonRewardRV in it.parentSet }.values
                    }

                model.addBoolOr(possibleChildVars.toTypedArray())
            }
        }

        val status = solver.solve(model)
        if(status != CpSolverStatus.OPTIMAL){
            println(status)
            return null
        }

        /*
        // Logging result
        for ((rv, pMap) in pSetVars){
            for((pInfo, pVar) in pMap){
                if(cpSolver.value(pVar) != 0.toLong()){
                    println("$rv : ${pInfo.parentSet} - ${cpSolver.value(pVar)}")
                }
            }
        }
        */

        return pSetVars
            .mapValues { (_, pMap) ->
                pMap.entries
                    .find { (_, pVar) -> solver.value(pVar) == 1.toLong() }!!
                    .key
            }

    }
    */

    fun solveDNILP(reasonableParents: Map<RandomVariable, List<ParentSetInfo>>, rDom: Set<RandomVariable>, enforceConncectivity: Boolean): Map<RandomVariable, ParentSetInfo>? {

        val nonRewardDom = reasonableParents.keys - rDom
        solver.clear()

        val pSetVars = reasonableParents.mapValues { (rv, pInfos) ->
            pInfos.associateWith { solver.makeBoolVar("${it.parentSet} -> $rv") }
        }

        val objective = solver.objective()
        objective.setMaximization()
        for (pMap in pSetVars.values) {
            for ((pInfo, pVar) in pMap) {
                objective.setCoefficient(pVar, pInfo.logProbability)
            }
        }

        // Each rv has exactly one parent set
        for ((_, pMap) in pSetVars) {
            //cpModel.addLinearSumEqual(pMap.values.toTypedArray(), 1)
            val oneParentSet = solver.makeConstraint()
            oneParentSet.setBounds(1.0, 1.0)
            for (pVar in pMap.values) {
                oneParentSet.setCoefficient(pVar, 1.0)
            }
        }

        // Graph forms a DAG
        val beliefs = reasonableParents.keys.filter { it.varType == VarType.BELIEF }
        val outcomes = reasonableParents.keys.filter { it.varType == VarType.OUTCOME }

        val beliefOrderVars = beliefs.associateWith { solver.makeIntVar(1.0, beliefs.size.toDouble(), "${it.name}-order") }
        val outcomeOrderVars = outcomes.associateWith { solver.makeIntVar(1.0, outcomes.size.toDouble(), "${it.name}-order") }

        // Chosen parent sets must obey some belief order
        for (child in beliefs) {
            for (parent in beliefs.filter { it != child }) {
                val orderObeyed = solver.makeConstraint()
                orderObeyed.setCoefficient(beliefOrderVars[parent]!!, 1.0)
                orderObeyed.setCoefficient(beliefOrderVars[child]!!, -1.0)
                pSetVars[child]!!
                    .filterKeys { parent in it.parentSet }
                    .forEach { orderObeyed.setCoefficient(it.value, beliefs.size + 1.0) }
                orderObeyed.setUb(beliefs.size.toDouble())
            }
        }

        // Chosen parent sets must obey some outcome order
        for (child in outcomes) {
            for (parent in outcomes.filter { it != child }) {
                val orderObeyed = solver.makeConstraint()
                orderObeyed.setCoefficient(outcomeOrderVars.getValue(parent), 1.0)
                orderObeyed.setCoefficient(outcomeOrderVars.getValue(child), -1.0)
                // For all parent sets of this child which contain the current parent,
                pSetVars[child]!!
                    .filterKeys { parent in it.parentSet }
                    .forEach { orderObeyed.setCoefficient(it.value, outcomes.size + 1.0) }
                orderObeyed.setUb(outcomes.size.toDouble())
            }
        }


        // All non reward dom vars must have at least one child
        if (enforceConncectivity) {
            for (nonRewardRV in nonRewardDom) {
                val varConnected = solver.makeConstraint()
                varConnected.setLb(1.0)
                for ((_, pMap) in pSetVars.filterKeys { it != nonRewardRV }) {
                    val relevantPSets = pMap.filterKeys { nonRewardRV in it.parentSet }
                    for ((_, pVar) in relevantPSets) {
                        varConnected.setCoefficient(pVar, 1.0)
                    }
                }
            }
        }

        val resultStatus = solver.solve()
        if (resultStatus != MPSolver.ResultStatus.OPTIMAL) {
            return null
        }

        if (!solver.verifySolution(0.0001, true)) {
            throw IllegalStateException("The solver didnt actually obey the constraints? WHAT???")
        }

        return pSetVars
            .mapValues { (_, pMap) ->
                pMap.entries
                    .find { (_, pVar) -> pVar.solutionValue() == 1.0 }!!
                    .key
            }
    }
}
