package DNUnawareness

import DirEdge
import Factor
import com.google.common.collect.Sets

import java.util.*
import RandomVariable
import Trial
import Utils.logNormalize
import Utils.sumInnerLnProbs
import uniformJoint
import java.lang.IllegalArgumentException

typealias LogPrior<T> = (T) -> Double
data class PriorDependencyResult(val reasonablePSets: Map<RandomVariable, List<ParentSetInfo>>,
                                 val structPrior: Map<RandomVariable, LogPrior<PSet>>,
                                 val paramPrior : List<Factor>)

interface ParentExtensionStrategy {
    fun extendParents(oldStructPrior: Map<RandomVariable, LogPrior<PSet>>,
                      latestParams : List<Factor>,
                      additionalVocab : Set<RandomVariable>,
                      parentEvidence: List<DirEdge>,
                      sampleHistory: List<Trial>): PriorDependencyResult
}

class NonConservative(private val config: StructLearnConfig) : ParentExtensionStrategy {
    override fun extendParents(oldStructPrior: Map<RandomVariable, LogPrior<PSet>>,
                               latestParams: List<Factor>,
                               additionalVocab : Set<RandomVariable>,
                               parentEvidence: List<DirEdge>, sampleHistory: List<Trial>): PriorDependencyResult {

        if(additionalVocab.isEmpty()) throw IllegalArgumentException("No variables to extend with!")

        val allVocab = (oldStructPrior.keys + additionalVocab).toSet()
        val pSetPriors = allVocab.associateWith { minParentsPrior(it, allVocab, config.singleParentCost) }

        val uniformFactors = allVocab.map { Factor(listOf(it), uniformJoint(listOf(it))) }

        val reasonableParents = pSetPriors.mapValues { (x, pSetPrior) ->
            structUpdate(x, pSetPrior, allVocab, emptyList(), parentEvidence, uniformFactors, config)
        }

        return PriorDependencyResult(reasonableParents, pSetPriors, uniformFactors)
    }
}


class Conservative(private val config : StructLearnConfig, private val inReasonableMix: Double) : ParentExtensionStrategy {

    override fun extendParents(oldStructPrior: Map<RandomVariable, LogPrior<PSet>>,
                               latestParams : List<Factor>,
                               additionalVocab : Set<RandomVariable>,
                               parentEvidence: List<DirEdge>,
                               sampleHistory: List<Trial>): PriorDependencyResult {

        //Find out what the new variable is in the extended vocabulary
        if(additionalVocab.isEmpty())
            throw IllegalArgumentException("No variables to extend with!")

        val oldVocab = oldStructPrior.keys
        val allVocab = oldVocab + additionalVocab

        val newLogPriorAll = HashMap<RandomVariable, LogPrior<PSet>>()
        val newReasonableParents = HashMap<RandomVariable, List<ParentSetInfo>>()
        val explicitEvidenceOldVocab = parentEvidence.filter { (parent, child) -> parent in oldVocab && child in oldVocab}

        val newPriorParams = allVocab.map { rv ->
            latestParams
                .find { it.scope.first() == rv }
                ?: Factor(listOf(rv), uniformJoint(listOf(rv)))
        }

        for (x in oldVocab) {
            //Case 1: Old Variable
            //We are about to lose all the old samples, so we might as well squeeze all we can out of them with a final structural update
            val reasonableParents = structUpdate(x, oldStructPrior.getValue(x), oldVocab, sampleHistory, explicitEvidenceOldVocab, newPriorParams, config)

            val expandedPriorMap : Map<PSet, Double> = reasonableParents
                .asSequence()
                .flatMap { pInfo ->
                    Sets.powerSet(additionalVocab.toSet())
                        .asSequence()
                        .map { novelVarSubset ->
                            val parentSetPlusNewVars = pInfo.parentSet + novelVarSubset
                            val extraVarsCost = novelVarSubset.size * Math.log(config.singleParentCost) +
                                (additionalVocab.size - novelVarSubset.size) * Math.log(1 - config.singleParentCost)
                            Pair(parentSetPlusNewVars, extraVarsCost + pInfo.logProbability)
                        }
                }
                .filter { !isParentSetProvablyFalse(x, it.first, parentEvidence) }
                .filter {  it.first.size <=  config.maxParents }
                .associate { it }

            val expandedPrior = mapPrior(logNormalize(expandedPriorMap))
            val minPrior = minParentsPrior(x, allVocab.toSet(), config.singleParentCost)
            val priorMix = mixedPrior(inReasonableMix, expandedPrior, minPrior)

            newLogPriorAll[x] = priorMix
            newReasonableParents[x] = structUpdate(x, priorMix, allVocab, emptyList(), parentEvidence, newPriorParams, config)
        }

        for(x in additionalVocab){
            newLogPriorAll[x] = minParentsPrior(x, allVocab.toSet(), config.singleParentCost)
            newReasonableParents[x] = structUpdate(x, newLogPriorAll[x]!!, allVocab, emptyList(), parentEvidence, newPriorParams, config)
        }

        return PriorDependencyResult(newReasonableParents, newLogPriorAll, newPriorParams)
    }
}

fun logNormPSetInfos(parentSetInfos: List<ParentSetInfo>) : List<ParentSetInfo> {
    if(parentSetInfos.isEmpty()){
        return emptyList()
    }
    val logTotal = sumInnerLnProbs(parentSetInfos.map { it.logProbability })
    return parentSetInfos.map { p -> p.copy(logProbability = p.logProbability - logTotal)}
}


fun minParentsPrior(child : RandomVariable, vocab : Set<RandomVariable>, extraParentCost : Double) : LogPrior<PSet> = { pSet ->
    if(isParentSetProvablyFalse(child, pSet, emptyList()))
        Math.log(0.0)
    else
        (vocab - child).sumByDouble { rv ->
            Math.log(if(rv in pSet) extraParentCost else (1.0 - extraParentCost))
        }
}

fun mapPrior(priorMap : Map<PSet, Double>) : LogPrior<PSet> = { pSet : PSet ->
    if(pSet in priorMap) priorMap[pSet]!! else Math.log(0.0)
}


fun mixedPrior(inMix : Double, inPrior : LogPrior<PSet>, outPrior : LogPrior<PSet>) : LogPrior<PSet> = { pSet : PSet ->
    val inVal = Math.log(inMix) + inPrior(pSet)
    val outVal = Math.log(1 - inMix) + outPrior(pSet)
    sumInnerLnProbs(listOf(inVal, outVal))
}
