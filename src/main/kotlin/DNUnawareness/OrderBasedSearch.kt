package DNUnawareness

import DirEdge
import Factor
import Utils.swapped
import java.util.*
import RandomVariable
import Trial
import topologicalSort
import PlainNode

typealias OrderSearchResult = UpdateResult<Map<RandomVariable, ParentSetInfo>, List<RandomVariable>>

fun getParentSetInfos(parentSets : Map<RandomVariable, Set<RandomVariable>>,
                      reasonablePSets: Map<RandomVariable, List<ParentSetInfo>>,
                      cachedExtraParents : Map<RandomVariable, MutableList<ParentSetInfo>>,
                      trials: List<Trial>,
                      prior: Map<RandomVariable, LogPrior<PSet>>,
                      paramPrior : List<Factor>,
                      alphaStrength : Double) : Map<RandomVariable, ParentSetInfo>{
    val finalParentSetInfos = HashMap<RandomVariable, ParentSetInfo>()
    for((child, parentSet) in parentSets){
        val parentSetInfo = reasonablePSets[child]!!.find { it.parentSet == parentSet } ?: cachedExtraParents[child]!!.find { it.parentSet == parentSet }
        if(parentSetInfo != null){
            finalParentSetInfos[child] = parentSetInfo
        }
        else{
            val newPSetInfo = createPSetInfo(child, parentSet, trials, prior[child]!!, paramPrior, alphaStrength, emptyList())
            finalParentSetInfos[child] = newPSetInfo
            cachedExtraParents[child]!!.add(newPSetInfo)
        }
    }

    return finalParentSetInfos
}


fun bestDNStruct(reasonablePSets: Map<RandomVariable, List<ParentSetInfo>>,
                 parentEvidence: List<DirEdge>,
                 rewardDomain: Set<RandomVariable>,
                 enforceConnectivity : Boolean): OrderSearchResult{
    if(enforceConnectivity){
        val possibleChildren = possibleChildren(reasonablePSets, parentEvidence)

        val noChildren = possibleChildren.filter { it.key !in rewardDomain && it.value.isEmpty() }
        if(noChildren.isNotEmpty()){
            return UpdateResult.FailureWithReason(noChildren.keys.toList())
        }

        val orderExistsResult = validOrderExists(possibleChildren, rewardDomain)
        if(orderExistsResult is SOFail){
            return UpdateResult.FailureWithReason(orderExistsResult.culprits)
        }
    }

    val bestStruct = DNStructSolver.solveDNILP(reasonablePSets, rewardDomain, enforceConnectivity)

    return if(bestStruct != null){
        UpdateResult.Success(bestStruct)
    }
    else{
        UpdateResult.FailureAmbiguous()
    }
}

/*
fun bestParentsOrderSearch(reasonableParentsMap: Map<RandomVariable, List<ParentSetInfo>>,
                           vocab: Set<RandomVariable>,
                           rewardDomain: Set<RandomVariable>,
                           explicitParentEvidence: List<DirEdge>,
                           sampleHistory : List<Trial>,
                           prior : Map<RandomVariable, LogPrior<PSet>>,
                           paramPrior : List<Factor>,
                           alphaStrength: Double
): OrderSearchResult {

    if(reasonableParentsMap.any{it.value.any{ (_, _, logProb) -> logProb.isInfinite()}}){
        print("Uh oh, how can it be reasonable if it is impossible?")
    }

    val cachedExtraParents = vocab.associate { Pair(it, ArrayList<ParentSetInfo>()) }


    //Set up a reasonable starting order
    val actions = vocab.filter {it.varType == VarType.ACTION }

    fun calcOrderInfo(order : List<RandomVariable>) : Either<OrderInfo, ILPFail> {
        val orderMap = orderToMap(actions, order)
        val orderObeyingParentSets = reasonableParentsMap.mapValues {removeOrderViolatingParentSets(it.key, it.value, orderMap)}
        val orderObeyingChildren = possibleChildren(orderObeyingParentSets)

        if(orderObeyingParentSets.values.any{it.isEmpty()}){
            return Error(ILPFail("No Parents Given Order", orderObeyingParentSets, orderObeyingChildren))
        }

        if(orderObeyingChildren.filter { it.value.isEmpty() && !rewardDomain.contains(it.key) }.isNotEmpty()){
            return Error(ILPFail("No Children Given Order", orderObeyingParentSets, orderObeyingChildren))
        }

        val ilpResult = DNStructSolver.relaxedSolve(vocab.toList(), rewardDomain, normAndExponentiate(orderObeyingParentSets), explicitParentEvidence) ?:
            return Error(ILPFail("Infeasible", orderObeyingParentSets, orderObeyingChildren))

        val ilpParentSetInfos = getParentSetInfos(ilpResult, reasonableParentsMap, cachedExtraParents, sampleHistory, prior, paramPrior,  alphaStrength)
        val orderProb = ilpParentSetInfos.values.fold(0.0) { acc, next -> acc + next.logProbability}

        return Result(OrderInfo(orderProb, ilpParentSetInfos, order))
    }

    val possibleChildren = possibleChildren(reasonableParentsMap)
    val possibleParents = possibleParents(reasonableParentsMap)
    val noChildrenVars = possibleChildren.filter{it.value.isEmpty() && !rewardDomain.contains(it.key)}.keys.toList()
    if(noChildrenVars.isNotEmpty()){
        return FailureWithCulprits(noChildrenVars)
    }

    //Tabu search
    val startOrderResult = validOrderExists(possibleParents, possibleChildren, explicitParentEvidence, rewardDomain)
    if(startOrderResult is SOFail){
        return FailureWithCulprits(startOrderResult.culprits)
    }
    val initialOrder = (startOrderResult as SOSuccess).startOrder

    val initialOrderInfo = calcOrderInfo(initialOrder)
    var globalBestOrderInfo = when(initialOrderInfo){
        is Result -> initialOrderInfo.result
        else -> throw IllegalStateException("Feasible starting order should have possible structure")
    }

    val tabuList = LinkedHashSet<List<RandomVariable>>()
    var timeSinceGlobalImprovement = 0

    var totalRounds = 0
    var prevOrder = globalBestOrderInfo.variableOrder
    while (timeSinceGlobalImprovement <= vocab.size) {
        totalRounds++
        tabuList.add(prevOrder)

        val neighboringOrders = neighborsWithPartition(prevOrder).filter { !tabuList.contains(it) && obeysPartialOrder(it, explicitParentEvidence)}
        if (neighboringOrders.isEmpty()){
            return Success(globalBestOrderInfo.reasonableParents)
        }
        val neighboringOrderInfos = neighboringOrders
            .map(::calcOrderInfo)
            .filter { it is Result }
            .map { (it as Result).result }

        if(neighboringOrderInfos.isEmpty()){
            return Success(globalBestOrderInfo.reasonableParents)
        }

        val localBestOrderInfo = neighboringOrderInfos.maxBy { it.bestParentsProb }!!

        if (localBestOrderInfo.bestParentsProb > globalBestOrderInfo.bestParentsProb) {
            globalBestOrderInfo = localBestOrderInfo
            timeSinceGlobalImprovement = 0
        }
        else {
            timeSinceGlobalImprovement += 1
        }

        prevOrder = localBestOrderInfo.variableOrder
    }

    return Success(globalBestOrderInfo.reasonableParents)
}


data class OrderInfo(val bestParentsProb: Double, val reasonableParents: Map<RandomVariable, ParentSetInfo>, val variableOrder: List<RandomVariable>)


fun obeysPartialOrder(order : List<RandomVariable>, parentEvidence: List<DirEdge>) : Boolean{
    val orderMap = order.withIndex().associate { Pair(it.value, it.index) }
    for((parentEv, childEv) in parentEvidence){
        val childPos = orderMap[childEv]
        val parentPos = orderMap[parentEv]
        if(childPos != null && parentPos != null && childPos < parentPos){
            return false
        }
    }
    return true
}

fun neighborsWithPartition(initialOrdering: List<RandomVariable>): List<List<RandomVariable>> {
    val beliefPartition = initialOrdering.filter{it.varType == VarType.BELIEF}
    val outcomePartition = initialOrdering.filter{it.varType == VarType.OUTCOME}

    val beliefNeighbors = initialAndNeighbors(beliefPartition)
    val outcomeNeighbors = initialAndNeighbors(outcomePartition)

    val neighboringOrders = ArrayList<List<RandomVariable>>()
    for (beliefNeighbor in beliefNeighbors) {
        for (outcomeNeighbor in outcomeNeighbors) {
            val neighboringOrder = beliefNeighbor + outcomeNeighbor
            if (neighboringOrder != initialOrdering) {
                neighboringOrders.add(neighboringOrder)
            }
        }
    }

    return neighboringOrders
}
*/

fun orderToMap(actions: List<RandomVariable>, ordering: List<RandomVariable>): Map<RandomVariable, Int> {
    val orderingMap = HashMap<RandomVariable, Int>()
    for (i in actions.indices) {
        orderingMap.put(actions[i], i)
    }

    for (i in ordering.indices) {
        orderingMap.put(ordering[i], i + actions.size)
    }

    return orderingMap
}

fun initialAndNeighbors(initialOrdering: List<RandomVariable>): List<List<RandomVariable>> {
    val neighborsAndInitial : List<List<RandomVariable>> =  (0..initialOrdering.size - 2).map { initialOrdering.swapped(it, it+1) }.plusElement(initialOrdering)
    return neighborsAndInitial
}

fun removeOrderViolatingParentSets(child: RandomVariable, parentSetInfos: List<ParentSetInfo>, orderMap: Map<RandomVariable, Int>): List<ParentSetInfo> {
    val orderObeyingParentSets = parentSetInfos.filter { it.parentSet.all { parent -> orderMap[parent]!! < orderMap[child]!! } }
    return orderObeyingParentSets
}


fun possibleChildren(reasonablePSetMap : Map<RandomVariable, List<ParentSetInfo>>, parentEv : List<DirEdge>) : Map<RandomVariable, Set<RandomVariable>>{
    val possibleChildSets = reasonablePSetMap.keys.associateWith { HashSet<RandomVariable>() }
    for((child, parentSets) in reasonablePSetMap){
        parentSets
            .flatMap { it.parentSet }
            .forEach { possibleChildSets[it]!!.add(child) }
    }

    // If you have evidence that p is a *parent* of c, then you must disallow p as a *child* of c
    possibleChildSets
        .mapValues { (parent, children) -> children
            .filter { child -> !parentEv
                .any{ (pEv, cEv) -> pEv == child && cEv == parent }
            }
        }

    return possibleChildSets
}

fun possibleParents(reasonablePSetsInfos : Map<RandomVariable, List<ParentSetInfo>>) : Map<RandomVariable, Set<RandomVariable>>{
    return reasonablePSetsInfos
        .mapValues { (_, pInfs) ->  pInfs.flatMap { it.parentSet }.toSet()}
}

fun partialOrderingSort(vocab : List<RandomVariable>, parentEvidence: List<DirEdge>) : List<RandomVariable>{
    val parentEvInVocab = parentEvidence.filter { (p, c) -> p in vocab && c in vocab }
    val parentMap = vocab.associate { Pair(it, ArrayList<RandomVariable>()) }
    for((parentEv, childEv) in parentEvInVocab){
        parentMap[childEv]!!.add(parentEv)
    }
    return topologicalSort(parentMap.map { PlainNode(it.key, it.value) }).map{ it.item }
}
