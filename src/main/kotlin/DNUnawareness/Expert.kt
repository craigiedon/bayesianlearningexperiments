package DNUnawareness
import java.util.*
import RandomVariable
import Trial
import DirEdge
import RVAssignment
import Utils.powerSet

typealias TimeStamp = Int

data class Expert(val decisionNetwork: DecisionNetwork,
                  val minTimeBetweenMessages : Int,
                  val subOptimalDecisionsThreshold : Double) {
    var timeLastAdviceGiven: Int = -1
    val knownAgentVocab = HashSet<RandomVariable>()

    val trialHistory = ArrayList<Pair<Trial, TimeStamp>>()
    val effectAdviceGiven = ArrayList<DirEdge>()
    val actionAdviceHistory = HashMap<TimeStamp, RVAssignment>()

    init {
        if (minTimeBetweenMessages <= 0) {
            throw IllegalArgumentException("Minimum time between messages must be greater than 0")
        }
    }
}

fun askForRDomVar(expert : Expert, knownDomain : Set<RandomVariable>, currentTime: Int): RandomVariable {
    expert.timeLastAdviceGiven = currentTime

    val rewardDomain = expert.decisionNetwork.rewardFunction.scope.toSet()
    val unknownDomain = rewardDomain - knownDomain

    if(unknownDomain.isEmpty()){
        throw IllegalArgumentException("Asking for reward domain variables when agent already knows entire reward domain: $knownDomain")
    }

    return unknownDomain.random()
    //ResultsLogger.addDialogueMessage(String.format("Expert responds with %s\n", newRewardVariables))
}

fun whatDoesVarDo(expert : Expert, rv : RandomVariable, currentTime : Int): DirEdge {
    expert.timeLastAdviceGiven = currentTime

    val (knownChildren, unknownChildren) = children(expert.decisionNetwork, rv)
        .filter { DirEdge(rv, it) !in expert.effectAdviceGiven } // Don't repeat yourself
        .partition { it in expert.knownAgentVocab }

    // Favour explanation of action effects in terms of already known variables
    if(knownChildren.isNotEmpty()){
        val advice = DirEdge(rv, knownChildren.first())
        expert.effectAdviceGiven.add(advice)
        return advice
    }

    if(unknownChildren.isNotEmpty()){
        val advice = DirEdge(rv, unknownChildren.first())
        expert.knownAgentVocab.add(unknownChildren.first())
        expert.effectAdviceGiven.add(advice)
        //ResultsLogger.logParentEv(advice.first, advice.second)
        return advice
    }

    throw IllegalArgumentException("Have asked for effects of $rv, but either all effects are known, or $rv has no children")
    //ResultsLogger.addDialogueMessage(String.format("Expert gave agent advice:\n %s\n", advice))
    //ResultsLogger.logParentEv(advice.first, advice.second)
}

fun updateExpertTrials(expert : Expert, trial: Trial, timeStamp : TimeStamp){
    expert.trialHistory.add(Pair(trial, timeStamp))
    expert.knownAgentVocab += trial.assignment.filter{ it.value != 0 && it.key.varType == VarType.ACTION }.keys
}

fun betterActionAdvice(expert : Expert, trial : Trial, trialTime: TimeStamp) : RVAssignment {
    val dn = expert.decisionNetwork
    val trialBeliefs = trial.assignment.filterKeys { it.varType == VarType.BELIEF }
    val trialAction = trial.assignment.filterKeys { it.varType == VarType.ACTION }
    val knownActions = expert.knownAgentVocab.filter { it.varType == VarType.ACTION }

    val newVars = dn.actions - knownActions

    // Find the best action you can with the lowest number of additional variables
    fun findBetterAction(numExtra : Int) : RVAssignment {
        if(numExtra > newVars.size){
            throw IllegalArgumentException("Agent already took the best action! : $trial")
        }
        val betterAction = powerSet(newVars, numExtra, numExtra)
            .map { optimalAction(trialBeliefs, dn, knownActions + it) }
            .maxBy { it.second }!!.first
        return if(fillDefaults(betterAction, dn.actions) != trialAction)
            betterAction
        else
            findBetterAction(numExtra + 1)
    }

    val bestAction = findBetterAction(0)

    expert.knownAgentVocab.addAll(bestAction.keys)

    expert.timeLastAdviceGiven = trialTime
    expert.actionAdviceHistory[trialTime] = bestAction
    //ResultsLogger.addDialogueMessage(String.format("Expert gives better action advice at timestep: %s\n Advice: %s\n", this.timeLastAdviceGiven, betterActionMessage))

    return bestAction
}

fun subOptimalBehaviour(optimalActionEUs: List<Double>, agentActionEUs: List<Double>, threshold: Double, lastAdvice: Int, currentTime: Int): Boolean {
    if(optimalActionEUs.size != agentActionEUs.size)
        throw IllegalArgumentException("Opt action EUs and Agent action EUs are different sizes (${optimalActionEUs.size} vs ${agentActionEUs.size})")

    if(currentTime >= agentActionEUs.size)
        throw java.lang.IllegalArgumentException("Current time $currentTime greater than num actions taken (${agentActionEUs.size}")


    val expectedRewardLoss = optimalActionEUs
        .drop(lastAdvice + 1)
        .zip(agentActionEUs.drop(lastAdvice + 1))
        .sumByDouble{(opt, actual) -> (opt - actual) / (currentTime - lastAdvice)}

    return expectedRewardLoss > threshold
}

data class ResolveMisunderstanding(val beliefVariable: RandomVariable, val oldBeliefVal: Int, val newBeliefVal: Int)

fun resolveMisunderstanding(expert : Expert, oldTrialTime: TimeStamp, newTrialTime: TimeStamp, currentTime: Int): ResolveMisunderstanding {
    val oldTrial = expert.trialHistory.find { it.second == oldTrialTime }!!.first
    val newTrial = expert.trialHistory.find { it.second == newTrialTime }!!.first

    println("Old Trial:  ${oldTrial.assignment}")
    println("New Trial:  ${newTrial.assignment}")

    val unknownBeliefs = expert.decisionNetwork.beliefs.filter { it !in expert.knownAgentVocab }

    for (beliefVar in unknownBeliefs) {
        val oldBeliefVal = oldTrial.assignment[beliefVar]!!
        val newBeliefVal = newTrial.assignment[beliefVar]!!
        if (oldBeliefVal != newBeliefVal) {
            expert.timeLastAdviceGiven = currentTime
            expert.knownAgentVocab.add(beliefVar)
            return ResolveMisunderstanding(beliefVar, oldBeliefVal, newBeliefVal)
        }
    }

    throw IllegalArgumentException("Unable to differentiate between $oldTrialTime and $newTrialTime")
}
