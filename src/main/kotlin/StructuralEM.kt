import Utils.randomParams
import java.util.*

fun structuralEM(startingBN : BayesNet, data : List<RVAssignment>, EMIterations: Int, mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(), parentSizeRestriction : Int) : BayesNet{
    var bestBN = batchEM(startingBN, data, EMIterations)
    var prevStruct : BNStruct
    var iterationsSoFar = 0
    do{
        iterationsSoFar++

        prevStruct = rawStructure(bestBN)

        val completedData = completeData(bestBN, data)
        val adTree = buildADTree(startingBN.vocab.toList(), completedData, ADBuildParams(25, 500, 250, 0.5, parentSizeRestriction + 1))

        val bestStruct = greedyStructureSearch(prevStruct, adTree, 1.0, mustHaveParents = mustHaveParents, parentSizeRestriction = parentSizeRestriction)

        val roughBN = MStep(bestStruct, sufficientStats(listOf(bestStruct), adTree, emptyMap()), 1.0)
        bestBN = batchEM(roughBN, data, EMIterations)

    } while(bestStruct != prevStruct && iterationsSoFar < 50)
    //println("Total SEM iterations: $iterationsSoFar")

    return bestBN
}

fun structuralEMNoAD(startingBN : BayesNet, data : List<RVAssignment>, EMIterations : Int, mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(), parentSizeRestriction: Int) : BayesNet{
    var bestBN = batchEM(startingBN, data, EMIterations)
    var prevStruct : BNStruct
    var iterationsSoFar = 0
    do{
        iterationsSoFar++
        prevStruct = rawStructure(bestBN)

        val validNeighbours = validNeighbors(prevStruct, parentSizeRestriction).map { it.bnStruct }.filter { hasRequiredParents(it, mustHaveParents) }
        val availableStats = EStep(bestBN, data, uniqueFamilies(validNeighbours))
        val bestStruct = statConstrainedStructureSearch(prevStruct, availableStats, 1.0, mustHaveParents, parentSizeRestriction)
        val roughBN = MStep(bestStruct, availableStats, 1.0)

        bestBN = batchEM(roughBN, data, EMIterations)

    } while(bestStruct != prevStruct && iterationsSoFar < 50)
    println("Total SEM iterations: $iterationsSoFar")

    return bestBN
}

data class IncStructureSearchResult(val bn : BayesNet, val topNeighbours : List<BNStruct>, val relevantStats : SufficientStats)

fun incrementalStructuralStep(prevBN: BayesNet, availableStats: SufficientStats, data : List<RVAssignment>, EMIterations : Int,
                              mustHaveParents: Map<RandomVariable, Set<RandomVariable>> = emptyMap(), parentSizeRestriction: Int, numTopNeighbours : Int) : IncStructureSearchResult {
    // Do we want to use the un-updated versions of old neighbour stats? Maybe we should update them before we take a walk...
    // Use batch revision of stats / params before searching
    val revisedBN = batchEM(prevBN, data, EMIterations)
    val revisedStats = EStep(revisedBN, data, availableStats.keys.toList())

    val bestStruct = statConstrainedStructureSearch(rawStructure(revisedBN), revisedStats, 1.0, mustHaveParents, parentSizeRestriction)
    val bestBN = batchEM(MStep(bestStruct, revisedStats, 1.0), data, EMIterations)

    val neighbours = validNeighbors(bestStruct, parentSizeRestriction).map{it.bnStruct}.filter { hasRequiredParents(it, mustHaveParents) }
    println("Number of new neighbours: ${neighbours.size}")

    // Is this still a bottleneck? Is laziness working?
    val bestBNStats = EStep(bestBN, data, uniqueFamilies(listOf(bestStruct)))
    val neighbourStats = EStep(bestBN, data, uniqueFamilies(neighbours))
    val cachedScores = updateScoreCache(emptyMap(), bestStruct, bestBNStats + neighbourStats, 1.0)
    val neighbourScores = neighbours.map { Pair(it, BDeuScore(it, cachedScores, neighbourStats, 1.0))}

    val topNeighbours = neighbourScores.sortedByDescending { it.second }.take(numTopNeighbours).map { it.first }
    val topNeighbourFamilies = uniqueFamilies(topNeighbours).toSet()
    val topNeighbourStats = neighbourStats.filterKeys { topNeighbourFamilies.contains(it) }

    return IncStructureSearchResult(bestBN, topNeighbours, topNeighbourStats + bestBNStats)
}

fun imputeUniformally(bn : BayesNet, data : List<RVAssignment>) =
    data.flatMap { imputeUniformally(bn, it) }

fun imputeUniformally(bn : BayesNet, trial : RVAssignment) : List<WeightedAssignment>{
    val missingVars = (bn.vocab - trial.keys).toList()

    return allAssignments(missingVars).map{ missingAssignment ->
        WeightedAssignment(trial + missingAssignment, 1.0 / numAssignments(missingVars))
    }
}

fun completeData(bn : BayesNet, data : List<RVAssignment>) =
    data.flatMap { completeTrial(bn, it) }

fun completeTrial(bn : BayesNet, trial: RVAssignment) : List<WeightedAssignment>{
    val missingVars = (bn.vocab - trial.keys).toList()
    val completionProbabilities = variableEliminationQuery(missingVars, trial, bn)

    return  allAssignments(missingVars).map { missingAssignment ->
        val assgnIndex = assignmentToIndex(missingAssignment, completionProbabilities.scope)
        val probability = completionProbabilities.values[assgnIndex]
        WeightedAssignment(trial + missingAssignment, probability)
    }
}
