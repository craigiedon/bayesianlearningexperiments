import Utils.lnToDifferentBase
import Utils.logOfBase
import Utils.project

fun logLikelihood(data : List<RVAssignment>, bn : BayesNet) =
    data.sumByDouble { Math.log(variableEliminationQuery(it, bn)) }


fun KLDivFastCached(trueBN: BayesNet, learnedBN: BayesNet, testData : List<RVAssignment>, trueLnProbs : List<Double>) : Double{
    if(trueBN.vocab.size < learnedBN.vocab.size){
        throw IllegalArgumentException("True BN has smaller vocab than learned one")
    }

    if(testData.isEmpty()){
        throw IllegalArgumentException("Need at least one data point to do empirical eval")
    }

    val knownDistVars = trueBN.vocab.filter { learnedBN.vocab.contains(it) }
    val unknownDistVars = trueBN.vocab - knownDistVars
    val lnUnknownVocabProb = -lnNumAssignments(unknownDistVars)

    val totalKL : Double = testData
        .asSequence()
        .withIndex()
        .sumByDouble{(i, assignment) ->
            val lnTrueProb = trueLnProbs[i]
            val lnLearnedProb = simpleLnQueryFull(project(assignment, learnedBN.vocab), learnedBN) + lnUnknownVocabProb
            lnTrueProb - lnLearnedProb
        }

    return totalKL / testData.size
}

fun KLDivEmpirical(distributionVars: List<RandomVariable>, trueBN: BayesNet, learnedBN: BayesNet, testData: List<RVAssignment>) : Double{
    var totalKL = 0.0

    val knownDistVars = distributionVars.filter { learnedBN.vocab.contains(it) }
    val unknownDistVars = distributionVars - knownDistVars
    val lnUnknownVocabProb = -lnNumAssignments(unknownDistVars)

    if(testData.isEmpty()){
        throw IllegalArgumentException("Need a least one data point to do empirical evaluation")
    }

    for(assignment in testData){
        val lnTrueProb = Math.log(variableEliminationQuery(assignment.filterKeys { distributionVars.contains(it) }, trueBN))
        val lnLearnedProb = Math.log(variableEliminationQuery(assignment.filterKeys { knownDistVars.contains(it) }, learnedBN)) + lnUnknownVocabProb

        if(lnTrueProb.isInfinite() || lnLearnedProb.isInfinite() || lnTrueProb.isNaN() || lnLearnedProb.isNaN()){
            throw IllegalStateException("What happened? How can we get a zero probability sample in the first place?")
        }

        totalKL += lnToDifferentBase(lnTrueProb - lnLearnedProb, 2.0)
    }

    return totalKL / testData.size
}

fun KLDivCached(distributionVars : List<RandomVariable>, cachedTrueLnProbs : Map<RVAssignment, Double>, learnedBN : BayesNet, testData : List<RVAssignment>) : Double{
    var totalKL = 0.0

    val knownDistVars = distributionVars.filter { learnedBN.vocab.contains(it) }
    val unknownDistVars = distributionVars - knownDistVars
    val lnUnknownVocabProb = -lnNumAssignments(unknownDistVars)

    if(testData.isEmpty())
        throw IllegalArgumentException("Need a least one data point to do empirical evaluation")
    if(cachedTrueLnProbs.isEmpty())
        throw IllegalArgumentException("No cached probs provided. Why did you choose this option then?")

    for(assignment in testData){
        val lnTrueProb = cachedTrueLnProbs[assignment]!!
        val lnLearnedProb = Math.log(variableEliminationQuery(assignment.filterKeys { knownDistVars.contains(it) }, learnedBN)) + lnUnknownVocabProb

        if(!lnTrueProb.isFinite() || !lnLearnedProb.isFinite()){
            throw IllegalStateException("What happened? How can we get a zero probability sample in the first place?")
        }

        totalKL += lnToDifferentBase(lnTrueProb - lnLearnedProb, 2.0)
    }

    return totalKL / testData.size
}

fun KLDivExact(distributionVars: List<RandomVariable>, trueBN : BayesNet, learnedBN: BayesNet) : Double{
    val knownVocabSubset = distributionVars.filter { learnedBN.vocab.contains(it) }
    var klDivergence = 0.0
    val trueProbs = variableEliminationQuery(distributionVars, emptyMap(), trueBN)
    val learnedProbsSubVocab = variableEliminationQuery(knownVocabSubset, emptyMap(), learnedBN)
    val unknownVocab = distributionVars - knownVocabSubset
    val logUnknownVocabProb = -logOfBase(numAssignments(unknownVocab).toDouble(), 2.0)
    for(i in trueProbs.values.indices){
        val subVocabAssgnIndex = convertIndex(i, distributionVars, knownVocabSubset)
        if(learnedProbsSubVocab.values[subVocabAssgnIndex] != 0.0 && trueProbs.values[i] != 0.0){
            val trueProb = trueProbs.values[i]
            val logTrueProb = logOfBase(trueProb, 2.0)
            val logLearnedProb = logOfBase(learnedProbsSubVocab.values[subVocabAssgnIndex], 2.0) + logUnknownVocabProb
            klDivergence += trueProb * (logTrueProb - logLearnedProb)
        }
        if(klDivergence.isNaN()){
            throw IllegalStateException("KL is Not a Number")
        }
    }
    return klDivergence
}

fun KLDivEmpirical(trueBN: BayesNet, learnedBN : BayesNet, testData : List<RVAssignment>) =
    KLDivEmpirical(trueBN.vocab.toList(), trueBN, learnedBN, testData)

fun KLDivExact(trueBN : BayesNet, learnedBN : BayesNet) =
    KLDivExact(trueBN.vocab.toList(), trueBN, learnedBN)
