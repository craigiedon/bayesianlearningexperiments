import java.util.*

fun ADTreeFullEMRun(bn : BayesNet, data : List<RVAssignment>, orderedVocab : List<RandomVariable>, buildParams : ADBuildParams, miniBatchSize : Int, stepDeprecationPower: Double, iterations : Int) : Pair<ADTree, BayesNet>{
    val adTree = buildADTree(orderedVocab, emptyList<RVAssignment>(), buildParams)
    var currentParams = bn
    val bnStructure = rawStructure(bn)
    val shuffledData = ArrayList(data)
    val numMiniBatches = data.size / miniBatchSize
    for(i in 0..(iterations - 1)){
        Collections.shuffle(shuffledData)
        for(t in 0..(numMiniBatches - 1)){
            val priorStrength = 1.0 / (i * data.size + (t + 1) * miniBatchSize)
            val miniBatch = shuffledData.subList(t * miniBatchSize, (t+1) * miniBatchSize)
            ADTreeStepwiseEM(currentParams, adTree, miniBatch, (i * numMiniBatches) + t, stepDeprecationPower)
            currentParams = MStep(bnStructure, sufficientStats(listOf(bnStructure), adTree, emptyMap()), priorStrength)
        }
    }

    return Pair(adTree, currentParams)
}

fun ADTreeStepwiseEM(bn : BayesNet, currentADT : ADTree, miniBatch : List<RVAssignment>, influenceStep : Int, stepDeprecationPower : Double){
    val newMix = if (influenceStep == 0) 1.0 else (1.0 * Math.pow(influenceStep + 1.0, -stepDeprecationPower))
    val interpolatedWeightedData = completeData(bn, miniBatch).map { WeightedAssignment(it.assignment, it.weight * newMix) }

    multiplyADTCounts(1 - newMix, currentADT.rootNode)
    updateADTree(currentADT, interpolatedWeightedData)
}
