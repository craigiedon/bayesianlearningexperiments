import Utils.*
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*

fun main(args: Array<String>){
    learnWithIncreasingVocabAndHiddenVariables("graphGeneration/insurance.json", 6500, 250, 10)
}

data class UnstableVocabLearner(val windowSize : Int,
                                val EMIterations : Int,
                                val initialStepSize : Double,
                                val topNeighbourAmount : Int,
                                var learnedBN: BayesNet,
                                @get:JsonIgnore val stepFunc : (List<RVAssignment>, Int, UnstableVocabLearner) -> Int,
                                @get:JsonIgnore val newVariableIntegrationFunc: (BayesNet, RandomVariable, Map<RandomVariable, Set<RandomVariable>>) -> BayesNet,
                                val lastSEMTime : Int = 0,
                                @get:JsonIgnore val topNeighbours : List<BNStruct> = emptyList(),
                                @get:JsonIgnore val suffStats: SufficientStats = emptyMap()
)

data class AgentStats(
    val klDivs : List<Double> = emptyList(),
    val relativeKLDivs : List<Double> = emptyList(),
    val structuralUpdateTimes : List<Int> = emptyList(),
    val vocabUpdateTimes: List<Int> = emptyList(),
    val trialRunTimes : List<Long> = emptyList(),
    val SHDs : List<Int> = emptyList(),

    @get:JsonIgnore val testData : List<RVAssignment>,
    @get:JsonIgnore val cachedTestDataProbs : Map<RVAssignment, Double> = emptyMap()
)

fun updateAgentStats(agentStats : AgentStats, trueBN: BayesNet, learnedBN: BayesNet, structuralUpdateTriggered : Boolean, vocabUpdateTriggered : Boolean, trialStartTime : Long) : AgentStats{
    val trialTimeTaken = System.currentTimeMillis() - trialStartTime

    val cachedTestDataLnProbs = if(agentStats.cachedTestDataProbs.isEmpty())
        agentStats.testData
            .associate {
                val observedAssignment = it.filterKeys { trueBN.observableVars.contains(it) }
                Pair(it, Math.log(variableEliminationQuery(observedAssignment, trueBN)))
            }
    else
        agentStats.cachedTestDataProbs

    val klDiv = KLDivCached(trueBN.observableVars, cachedTestDataLnProbs, learnedBN, agentStats.testData)
    //val relativeKLDiv = KLDivEmpirical(learnedBN.observableVars, trueBN, learnedBN, agentStats.testData)
    println(klDiv)

    val structUpdateTimes = plusIf(agentStats.structuralUpdateTimes, agentStats.klDivs.size, structuralUpdateTriggered)
    val vocabUpdateTimes = plusIf(agentStats.vocabUpdateTimes, agentStats.klDivs.size, vocabUpdateTriggered)
    val SHDs = agentStats.SHDs + structuralHammingDistance(trueBN, learnedBN)

    return AgentStats(
        agentStats.klDivs + klDiv,
        agentStats.relativeKLDivs,// + relativeKLDiv,
        structUpdateTimes,
        vocabUpdateTimes,
        agentStats.trialRunTimes + trialTimeTaken,
        SHDs,
        agentStats.testData,
        cachedTestDataLnProbs
    )
}

fun graftNewVar(oldBN : BayesNet, newVar : RandomVariable, mandatoryParentRels : Map<RandomVariable, Set<RandomVariable>>) : BNStruct{
    val newVarChild = mandatoryParentRels.keys.find { rv -> mandatoryParentRels[rv]!!.contains(newVar) } ?:
        throw IllegalArgumentException("Should have been given child of new var to anchor it")

    val oldStructure = rawStructure(oldBN)

    // For every previous parent of the anchor child, have the new variable be a parent to this as well
    val newStructure = oldStructure.mapValues { (rv, parents) ->
        if(rv == newVarChild || oldStructure[newVarChild]!!.contains(rv))
            parents + newVar
        else
            parents
    } + Pair(newVar, emptySet())

    // Do something about the max num parents constraint?
    return newStructure
}

fun keepUnaffectedCPTsAndRandomizeNewOnes(newStructure : BNStruct, oldBN : BayesNet) : BayesNet{
    val nodeMap = HashMap<RandomVariable, BNode>()
    for((rv, parents) in newStructure){
        val oldNode = oldBN.nodes[rv]
        if(oldNode != null && oldNode.parents.toSet() == parents){
            nodeMap[rv] = oldNode
        }
        else{
            val orderedParents = parents.toList()
            nodeMap[rv] = BNode(rv, orderedParents, randomCPT(rv, orderedParents))
        }
    }
    return BayesNet(nodeMap)
}

fun allIndependentRandomParams(oldBN: BayesNet, newVar: RandomVariable, mandatoryParentRels: Map<RandomVariable, Set<RandomVariable>>) =
    randomParams(initialStructure(oldBN.vocab + newVar, mandatoryParentRels), 2)

fun OverConnectNewAndPreserveMarginals(oldBN: BayesNet, newVar: RandomVariable, mandatoryParentRels: Map<RandomVariable, Set<RandomVariable>>) : BayesNet{
    val graftedStruct = graftNewVar(oldBN, newVar, mandatoryParentRels)
    return PreserveOldMarginals(graftedStruct, oldBN)
}

fun PreserveOldMarginals(newStructure: BNStruct, oldBN : BayesNet) : BayesNet{
    val newNodeMap = HashMap<RandomVariable, BNode>()
    for((rv, parents) in newStructure){
        val oldNode = oldBN.nodes[rv]
        // New Vocab
        if(oldNode == null){
            val orderedParents = parents.toList()
            newNodeMap[rv] = BNode(rv, orderedParents, uniformCPT(rv, orderedParents))
        }
        // Old Vocab, but new parents
        else if(oldNode.parents.toSet() != parents){
            val newParents = (parents - oldNode.parents).toList()
            val newCPT = repeatList(oldNode.cpt, numAssignments(newParents))
            newNodeMap[rv] = BNode(rv, oldNode.parents + newParents, newCPT)
        }
        // Old Vocab, old parents
        else{
            newNodeMap[rv] = oldNode
        }
    }

    return BayesNet(newNodeMap)
}

fun timeSinceLastVocabIntro(data: List<RVAssignment>, fullVocabTimeStep: Int) =
    data.size - fullVocabTimeStep
fun distanceFromWindowBack(data : List<RVAssignment>, fullVocabTimestep: Int, agent : UnstableVocabLearner) : Int {
    val windowBack = windowBack(data, agent.windowSize, fullVocabTimestep)
    return data.size - windowBack
}

fun updateAgentNewTrial(agent : UnstableVocabLearner, data: List<RVAssignment>, timeStep: Int,
                        monotonicParentInfo : Map<RandomVariable, Set<RandomVariable>>,
                        newMiniBatch: List<RVAssignment>, fullVocabTimeStep : Int) : Pair<UnstableVocabLearner, Boolean>{

    val windowBack = windowBack(data, agent.windowSize, fullVocabTimeStep)

    val agentStepResult = agent.stepFunc(data, fullVocabTimeStep, agent)
    val stepParamResult = agentStepResult / newMiniBatch.size * agent.EMIterations

    val learnedStats = stepWiseE(agent.learnedBN, agent.suffStats, newMiniBatch, agent.initialStepSize,
        stepParamResult, 0.6)
    val updatedParamsBN = MStep(rawStructure(agent.learnedBN), learnedStats, 1.0)

    if(!structuralUpdateNeeded(updatedParamsBN, learnedStats, agent.topNeighbours, agent.lastSEMTime, timeStep, monotonicParentInfo)){
        return Pair(agent.copy(suffStats = learnedStats, learnedBN = updatedParamsBN), false)
    }

    val (updatedStructureBN, newTopNeighbours, newStats) = incrementalStructuralStep(updatedParamsBN, learnedStats, data.subList(windowBack, data.size), agent.EMIterations, monotonicParentInfo, 5, agent.topNeighbourAmount)

    return Pair(agent.copy(suffStats = newStats, learnedBN = updatedStructureBN, lastSEMTime = timeStep, topNeighbours = newTopNeighbours), true)
}

fun updateAgentNewVar(agent : UnstableVocabLearner, newVar : RandomVariable, data: List<RVAssignment>, monotonicParentInfo: Map<RandomVariable, Set<RandomVariable>>, fullVocabTimeStep : Int) : UnstableVocabLearner{
    val newBN = agent.newVariableIntegrationFunc(agent.learnedBN, newVar, monotonicParentInfo)
    val dataWindow = data.subList(windowBack(data, agent.windowSize, fullVocabTimeStep), data.size)
    val newBNStats = EStep(newBN, data)
    val (newNeighbours, topNeighbourStats) = topKNeighbours(newBN, dataWindow, agent.topNeighbourAmount)

    return agent.copy(learnedBN = newBN, topNeighbours = newNeighbours, suffStats = (newBNStats + topNeighbourStats))
}

fun learnWithIncreasingVocabAndHiddenVariables(fileLocation: String, numTrials: Int, expertVocabIntroInterval : Int, miniBatchSize : Int){
    val loadedBN = loadJsonBN(fileLocation)
    val trueBN = hideNodes(loadedBN, chooseHiddenVarsProportion(loadedBN, 0.25))
    val trueStruct = rawStructure(trueBN)

    val initialBN = randomParams(initialStructure(leafNodes(trueBN.vocab, rawStructure(trueBN))), 2)
    val unmentionedVocab = ArrayList(trueBN.vocab - initialBN.vocab)

    val monotonicParentInfo = HashMap<RandomVariable, Set<RandomVariable>>()
    val observedData = ArrayList<RVAssignment>()
    val fullData = ArrayList<RVAssignment>()
    val testData = hideVars(generateSamples(trueBN, 2500), trueBN.hiddenVars)
    var fullVocabTimeStep = 0

    var agents = mapOf(
        "w0, grafter" to UnstableVocabLearner(0, 50, 0.5, 50, initialBN, ::distanceFromWindowBack, ::OverConnectNewAndPreserveMarginals),
        "w100, grafter" to UnstableVocabLearner(100, 50, 0.5, 50, initialBN, ::distanceFromWindowBack, ::OverConnectNewAndPreserveMarginals)
    )

    val agentStats = HashMap(agents.mapValues { AgentStats(testData = testData) })

    for(i in 0..numTrials step miniBatchSize){
        println()
        println("Trial $i")
        // Print out the true substructure of the hidden vars so far versus what youve got for it
        val knownHiddenVars = trueBN.hiddenVars - unmentionedVocab
        println("Hidden Vars: $knownHiddenVars")
        println("True Hidden Var Substructure: ${parentChildSubstructure(trueStruct, knownHiddenVars)}")
        println("Montonic Info: $monotonicParentInfo")

        for((agentName, agent) in agents){
            println("$agentName hidden pcStruct: ${parentChildSubstructure(rawStructure(agent.learnedBN), knownHiddenVars)}")
        }

        val miniBatch = generateSamples(trueBN, miniBatchSize)
        val observedMiniBatch = hideVars(miniBatch, unmentionedVocab + trueBN.hiddenVars)

        fullData.addAll(miniBatch)
        observedData.addAll(observedMiniBatch)

        agents = agents.mapValues{ (agentName, oldAgent) ->
            val trialStartTime = System.currentTimeMillis()
            val (newAgent, SEMUpdateTriggered) = updateAgentNewTrial(oldAgent, observedData, i, monotonicParentInfo, observedMiniBatch, fullVocabTimeStep)
            agentStats[agentName] = updateAgentStats(agentStats[agentName]!!, trueBN, newAgent.learnedBN, SEMUpdateTriggered, false, trialStartTime)
            newAgent
        }

        // Expert Interjections of New Vocab
        if(i != 0 && i % expertVocabIntroInterval == 0 && unmentionedVocab.isNotEmpty()){
            println("New variable at $i")

            // Gross expert interaction that I can't figure out how to refactor
            val newVar = expertChooseNewVar(trueBN, trueBN.vocab.toList() - unmentionedVocab)
            val childOfNewVar = whatDoesThisAffect(newVar, trueBN, trueBN.vocab.toList() - unmentionedVocab)
            monotonicParentInfo[childOfNewVar] = (monotonicParentInfo[childOfNewVar] ?: emptySet()) + newVar
            unmentionedVocab.remove(newVar)
            fullVocabTimeStep = observedData.size

            // Update agents based on new variable
            agents = agents.mapValues{ (agentName, oldAgent) ->
                val startTime = System.currentTimeMillis()
                val newAgent = updateAgentNewVar(oldAgent, newVar, observedData, monotonicParentInfo, fullVocabTimeStep)
                agentStats[agentName] = updateAgentStats(agentStats[agentName]!!, trueBN, newAgent.learnedBN, true, true, startTime)
                newAgent
            }
        }
    }

    // Compare against full obs greedy batch learner
    val dataAmounts = listOf(100, 250, 1250, fullData.size)
    for(dataAmount in dataAmounts){
        val dataSubset = fullData.subList(0, dataAmount)
        val adParams = ADBuildParams(10, 200, 100, 0.6, 6)
        val adTree = buildADTree(trueBN.vocab.toList(), dataSubset, adParams)

        // How well can you fit *all* variables?
        val fullObsStruct = greedyStructureSearch(initialStructure(trueBN.vocab.toList(), monotonicParentInfo), adTree, 1.0, mustHaveParents = monotonicParentInfo, parentSizeRestriction = 5)
        val fullObsBN = learnParams(fullObsStruct, sufficientStats(fullObsStruct, adTree), 1.0)
        val fullObsKL = KLDivEmpirical(trueBN.vocab.toList(), trueBN, fullObsBN, testData)
        println("All vars fully observed - data size : $dataAmount, KLDiv : $fullObsKL")

        // What about if you just jettison the hidden variables completely?
        val noHiddenStruct = greedyStructureSearch(initialStructure(trueBN.observableVars), adTree, 1.0, parentSizeRestriction = 5)
        val noHiddenBN = learnParams(noHiddenStruct, sufficientStats(noHiddenStruct, adTree), 1.0)
        val noHiddenKL = KLDivEmpirical(trueBN.observableVars, trueBN, noHiddenBN, testData)
        println("Hidden exluded fully aware - data size : $dataAmount, KLDiv : $noHiddenKL")
    }

    // Full aware but hidden incremental learner
    var fullyAwareIncrementalAgent = UnstableVocabLearner(1000000, 50, 0.5, 50, randomParams(initialStructure(trueBN.vocab, monotonicParentInfo), 2), ::distanceFromWindowBack, ::OverConnectNewAndPreserveMarginals)
    agentStats["fully aware"] = AgentStats(testData = testData)
    for(trialIndex in fullData.indices step miniBatchSize){
        val miniBatch = hideVars(fullData.subList(trialIndex, trialIndex + miniBatchSize), trueBN.hiddenVars)
        val trialStartTime = System.currentTimeMillis()
        println(trialIndex)
        val dataSoFar = hideVars(fullData.subList(0, trialIndex + miniBatchSize), trueBN.hiddenVars)
        val (newAgent, SEMUpdateTriggered) = updateAgentNewTrial(fullyAwareIncrementalAgent, dataSoFar, trialIndex, monotonicParentInfo, miniBatch, fullVocabTimeStep)
        fullyAwareIncrementalAgent = newAgent
        agentStats["fully aware"] = updateAgentStats(agentStats["fully aware"]!!, trueBN, newAgent.learnedBN, SEMUpdateTriggered, false, trialStartTime)
    }

    saveToJson(agents, "graphGeneration", "unstableVocabLearningAgents")
    saveToJson(agentStats, "graphGeneration", "unstableVocabLearningStats")
}

fun chooseHiddenVarsProportion(trueBN : BayesNet, hiddenProportion : Double) =
    chooseHiddenVars(trueBN, (trueBN.vocab.size * hiddenProportion).toInt())

fun chooseHiddenVars(trueBN : BayesNet, numHiddenVars : Int) : List<RandomVariable> {
    val potentialHiddenVariables = trueBN.vocab.filter { !uselessHiddenVariable(rawStructure(trueBN), it) }

    if(potentialHiddenVariables.size < numHiddenVars)
        throw IllegalArgumentException("Not enough useful hidden variables: ${potentialHiddenVariables.size} v $numHiddenVars")

    return shuffle(potentialHiddenVariables).take(numHiddenVars)
}

fun uselessHiddenVariable(bnStruct : BNStruct, rv : RandomVariable) : Boolean{
    if(!bnStruct.keys.contains(rv))
        throw IllegalArgumentException("RV does not exist in given structure")
    // If the random variable has only a single parent and child, or no children, then it is useless
    val childrenOfRV = bnStruct.filter{ (_, parents) -> parents.contains(rv)}.map { (child, _) -> child }
    val parentsOfRV = bnStruct[rv]!!
    return childrenOfRV.size <= 1 || parentsOfRV.size == 1
}

fun structuralUpdateNeeded(currentBN : BayesNet, currentSuffStats : SufficientStats, topNeighbours: List<BNStruct>, lastSEMTime: Int, timeStep : Int, mandatoryParentRels: Map<RandomVariable, Set<RandomVariable>>) : Boolean{
    if(topNeighbours.isEmpty() && timeStep >= 50){
        println("Empty neighbours: Update Needed")
        return true
    }

    if(topNeighbours.isEmpty()){
        return false
    }

    if(timeStep - lastSEMTime >= 1000){
        println("Time passed: Update Needed")
        return true
    }

    val topNeighbourScores = topNeighbours.map { BDeuScore(it, emptyMap(), currentSuffStats, 1.0, mustHaveParents = mandatoryParentRels) }
    val currentBNScore = BDeuScore(rawStructure(currentBN), emptyMap(), currentSuffStats, 1.0, mustHaveParents = mandatoryParentRels)
    // Add a small epsilon to ensure we don't update on structures with roughly equivalent scores. (Difference could be due to rounding error anyways)
    val scoreDifference = topNeighbourScores.max()!! - currentBNScore
    if(scoreDifference > 1.0){
        println("Score beaten. Difference: $scoreDifference")
        return true
    }

    return false
}

fun topKNeighbours(currentBN : BayesNet, data : List<RVAssignment>, topNeighbourAmount : Int) : Pair<List<BNStruct>, SufficientStats>{
    val neighbours = validNeighbors(rawStructure(currentBN), 5).map { it.bnStruct }
    val neighbourStats = EStep(currentBN, data, uniqueFamilies(neighbours + rawStructure(currentBN)))
    val neighbourScores = neighbours.associate { bnStruct ->  Pair(bnStruct, BDeuScore(bnStruct, emptyMap(), neighbourStats, 1.0))}

    val topNeighbours = neighbourScores.entries.sortedByDescending { it.value }.map { it.key }.take(topNeighbourAmount)

    val newSuffStats = neighbourStats.filterKeys { uniqueFamilies(topNeighbours + rawStructure(currentBN)).contains(it)}
    return Pair(topNeighbours , newSuffStats)
}

fun uniqueFamilies(structs : List<BNStruct>) =
    structs.flatMap {
        struct -> struct.entries.map { (child, parent) -> parent + child }
    }.toSet().toList()

fun expertChooseNewVar(trueBN: BayesNet, knownVocab : List<RandomVariable>) : RandomVariable{
    val unknownVocab = trueBN.vocab - knownVocab
    val trueStruct = rawStructure(trueBN)
    // Want to pick a variable such that one of the unknown variables is a parent to some variable we already know about
    val possibleSuggestions = unknownVocab.filter{ newRV -> knownVocab.any { knownRV -> trueStruct[knownRV]!!.contains(newRV)}}
    return possibleSuggestions.random()
}

fun whatDoesThisAffect(rv : RandomVariable, trueBN : BayesNet, knownVocab : List<RandomVariable>) : RandomVariable{
    val trueStruct = rawStructure(trueBN)
    val possibleAnswers = knownVocab.filter{  knownVar -> trueStruct[knownVar]!!.contains(rv)}
    return possibleAnswers.random()
}

fun windowBack(data: List<RVAssignment>, windowSize: Int, fullVocabTimeStep: Int) =
    minOf(fullVocabTimeStep, maxOf(0, data.size - windowSize))

fun leafNodes(vocab: Collection<RandomVariable>, bnStruct : BNStruct) : List<RandomVariable>{
    return vocab.filter { rv ->
            !bnStruct.any { (_, parents) ->
                parents.contains(rv)
            }
    }
}
