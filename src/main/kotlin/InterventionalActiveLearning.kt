import Utils.logOfBase
import Utils.sumInnerLnProbs
import java.util.*

typealias RVAssignment = Map<RandomVariable, Int>
class InterventionalSample(val observedSample : RVAssignment, val clampedNodes : Set<RandomVariable>)
data class GraphStructInfo(val structure : BNStruct, val bestParams : BayesNet, val posteriorProb : Double)

fun main(args : Array<String>){
    val trueBN = loadJsonBN("graphGeneration/asia.json")
    val trueStruct = rawStructure(trueBN)

    val l1Results = HashMap<String, MutableList<MutableList<Double>>>()

    l1Results["active"] = ArrayList<MutableList<Double>>()
    l1Results["passive"] = ArrayList<MutableList<Double>>()
    l1Results["Utils.random"] = ArrayList<MutableList<Double>>()

    for(t in 1..10){
        val experimentalData = ArrayList<InterventionalSample>()
        val activeErrors = ArrayList<Double>()
        println("Active Learner")
        for(i in 1..100){
            val graphPost = approximateGraphStructPosterior(trueBN.vocab.toList(), emptyList(), experimentalData, 1000, 100)
            val optAction = optimalQueryIntervention(trueBN.vocab.toList(), graphPost, emptyList(), experimentalData)
            experimentalData.add(InterventionalSample(generateSampleWithIntervention(trueBN, optAction), optAction.keys))
            val l1Error = L1EdgeError(trueStruct, graphPost.mapValues { it.value.posteriorProb })
            println(l1Error)
            activeErrors.add(l1Error)
        }
        println()

        println("Observational Learner")
        val observedData = ArrayList<RVAssignment>()
        val passiveErrors = ArrayList<Double>()
        for(i in 1..100){
            val graphPost = approximateGraphStructPosterior(trueBN.vocab.toList(), observedData, emptyList(), 1000, 100)
            observedData.addAll(generateSamples(trueBN, 1))
            val l1Error = L1EdgeError(trueStruct, graphPost.mapValues { it.value.posteriorProb })
            println(l1Error)
            passiveErrors.add(l1Error)
        }

        println("Random Interventions Learner")
        experimentalData.clear()
        val randomErrors = ArrayList<Double>()
        for(i in 1..100){
            val graphPost = approximateGraphStructPosterior(trueBN.vocab.toList(), emptyList(), experimentalData, 1000, 100)
            val optAction = randomInterventionAction(trueBN.vocab.toList())
            experimentalData.add(InterventionalSample(generateSampleWithIntervention(trueBN, optAction), optAction.keys))
            val l1Error = L1EdgeError(trueStruct, graphPost.mapValues { it.value.posteriorProb })
            println(l1Error)
            randomErrors.add(l1Error)
        }

        l1Results["active"]!!.add(activeErrors)
        l1Results["passive"]!!.add(passiveErrors)
        l1Results["Utils.random"]!!.add(randomErrors)
    }

    saveToJson(l1Results, "graphGeneration", "activeLearningExp")
}

fun approximateGraphStructPosterior(vocab : List<RandomVariable>, observedData : List<RVAssignment>, interventionalData: List<InterventionalSample>, burnIn : Int, numSamples : Int) : Map<BNStruct, GraphStructInfo>{
    val graphStructs = graphStructMCMC(initialStructure(vocab), burnIn, numSamples, observedData, interventionalData)
    val totalProbMass : Double = graphStructs.values.sum().toDouble()
    val structInfos = HashMap<BNStruct, GraphStructInfo>()
    for((graphStruct, count) in graphStructs) {
        val sufficientStats = sufficientStatsInterventional(graphStruct, observedData, interventionalData)
        val bn = MStep(graphStruct, sufficientStats, 1.0)
        structInfos[graphStruct] = GraphStructInfo(graphStruct, bn, count.toDouble() / totalProbMass)
    }
    return structInfos
}

fun randomInterventionAction(vocab : List<RandomVariable>) : RVAssignment{
    val actions = ArrayList<RVAssignment>()
    actions.add(emptyMap())
    for(rv in vocab){
        actions.addAll(allAssignments(listOf(rv)))
    }
    return actions[(Math.random() * actions.size).toInt()]
}

fun optimalQueryIntervention(vocab: List<RandomVariable>, structInfos : Map<BNStruct, GraphStructInfo>, observedData : List<RVAssignment>, interventionalData: List<InterventionalSample>) : RVAssignment{
    val actions = ArrayList<RVAssignment>()
    actions.add(emptyMap())
    for(rv in vocab){
        actions.addAll(allAssignments(listOf(rv)))
    }

    val actionVal = HashMap<RVAssignment, Double>()
    val buildParams = ADBuildParams(5, 200, 100, 0.5, 5 + 1)
    //val adTree = buildADTree(vocab, observedData, buildParams)


    for(action in actions){
        actionVal[action] = 0.0
        val possibleOutcomes = allAssignments(vocab - action.keys)
        for(possibleOutcome in possibleOutcomes){
            val fullOutcome = possibleOutcome + action

            var outcomeMarginal = 0.0
            for((_, bestParamBN, graphPostProb) in structInfos.values) {
                val outcomePosterior = outcomeProbClampedVals(fullOutcome, bestParamBN, action)
                outcomeMarginal += graphPostProb * outcomePosterior
            }

            for((_, bestParamBN, graphPostProb) in structInfos.values) {
                val outcomePosterior = outcomeProbClampedVals(fullOutcome, bestParamBN, action)
                val graphProbGivenActionOutcome = (graphPostProb * outcomePosterior) / outcomeMarginal
                actionVal[action] = actionVal[action]!! + outcomePosterior * graphPostProb * logOfBase(graphProbGivenActionOutcome, 2.0)
            }
        }
    }

    return actionVal.maxBy { it.value }!!.key
}

fun graphStructMCMC(initialStruct : BNStruct, burnIn : Int, numSamples : Int, observationalData : List<RVAssignment>, interventionalData: List<InterventionalSample>) : Map<BNStruct, Int>{
    val sampledStructures = HashMap<BNStruct, Int>()
    var currentStruct = initialStruct
    for(t in 1..(burnIn + numSamples)){
        val neighbours = validNeighbors(currentStruct, 5)
        val randNeighbourIndex = (Math.random() * neighbours.size).toInt()
        val candidateNeighbour = neighbours[randNeighbourIndex]
        val newNeighbours = validNeighbors(candidateNeighbour.bnStruct, 5)

        var candNodeScore = 0.0
        var oldNodeScore = 0.0
        for((rv, differentParents) in candidateNeighbour.differingNodes){
            val candCounts = makeCountTableWithInterventional(rv, differentParents, observationalData, interventionalData)
            val oldCounts = makeCountTableWithInterventional(rv, currentStruct[rv]!!, observationalData, interventionalData)
            candNodeScore += BDeuNodeScore(rv, differentParents, candCounts, 1.0)
            oldNodeScore += BDeuNodeScore(rv, currentStruct[rv]!!, oldCounts, 1.0)
        }

        val rejectionThreshold = Math.exp(candNodeScore - oldNodeScore) * (neighbours.size.toDouble() / newNeighbours.size.toDouble())
        if(Math.random() < minOf(1.0, rejectionThreshold)){
            //Accept proposal
            currentStruct = candidateNeighbour.bnStruct
        }

        if(t > burnIn){
            sampledStructures[currentStruct] = (sampledStructures[currentStruct] ?: 0) + 1
        }
    }

    if(sampledStructures.values.sum() != numSamples){
        throw IllegalStateException("You don't have the amount of samples that you asked for")
    }

    return sampledStructures
}

fun outcomeProbClampedVals(fullOutcome : RVAssignment, bn : BayesNet, clampedAssignments: RVAssignment) : Double{
    if(HashSet(bn.vocab) != fullOutcome.keys){
        throw IllegalArgumentException("Outcome must contain assignment for full vocabulary")
    }
    val clampedVars = clampedAssignments.keys

    // Outcome assignment must match on all clamped vars, otherwise it is impossible
    val actualRestrictedAssignments = fullOutcome.filterKeys { clampedVars.contains(it)}
    if(actualRestrictedAssignments != clampedAssignments){
        return 0.0
    }

    val unclampedVars = bn.vocab - clampedVars
    return unclampedVars.fold(1.0, { productSoFar, rv ->
        val node = bn.nodes[rv]!!
        productSoFar * node.cpt[assignmentToIndex(fullOutcome, listOf(rv) + node.parents)]
    })
}

fun L1EdgeError(trueStruct : BNStruct, graphPosterior : Map<BNStruct, Double>) : Double{
    var l1Error = 0.0
    val vocab = trueStruct.keys.toList()

    for((i, rv1) in vocab.withIndex()){
        for(rv2 in vocab.subList(i + 1, vocab.size)){
            if(rv1 == rv2) throw IllegalStateException("Comparing two identical RVs for dependency")
            val featureProb : Double

            // rv1 -> rv2
            if(trueStruct[rv2]!!.contains(rv1)){
                featureProb = structFeatureProbability(graphPosterior, EdgeRelation.DirectedEdge(rv1, rv2))
            }
            // rv2 -> rv1
            else if(trueStruct[rv1]!!.contains(rv2)){
                featureProb = structFeatureProbability(graphPosterior, EdgeRelation.DirectedEdge(rv2, rv1))
            }
            // rv1 _|_ rv2
            else{
                featureProb = structFeatureProbability(graphPosterior, EdgeRelation.NoEdge(rv1, rv2))
            }
            l1Error += (1 - featureProb)
        }
    }

    return l1Error
}

sealed class EdgeRelation(val r1 : RandomVariable, val r2 : RandomVariable){
    class DirectedEdge(val parent : RandomVariable, val child : RandomVariable) : EdgeRelation(parent, child){
        override fun toString() = "$parent -> $child"
    }
    class NoEdge(r1: RandomVariable, r2 : RandomVariable) : EdgeRelation(r1, r2){
        override fun toString() = "$r1 -/- $r2"
    }
}
fun structFeatureProbability(graphPosterior : Map<BNStruct, Double>, edgeRelation : EdgeRelation) : Double{
    var featureProb = 0.0
    for((graphStruct, posteriorProb) in graphPosterior){
        if(edgeRelation is EdgeRelation.DirectedEdge && graphStruct[edgeRelation.child]!!.contains(edgeRelation.parent)){
            featureProb += posteriorProb
        }
        else if(edgeRelation is EdgeRelation.NoEdge && !graphStruct[edgeRelation.r1]!!.contains(edgeRelation.r2) && !graphStruct[edgeRelation.r2]!!.contains(edgeRelation.r1)){
            featureProb += posteriorProb
        }
    }

    return featureProb
}

fun structurePredictionError(trainingADTree : ADTree, testData : List<RVAssignment>, graphPosterior: Map<BNStruct, Double>) : Double{
    var totalError = 0.0
    val bestParams = graphPosterior.mapValues { MStep(it.key, sufficientStats(it.key, trainingADTree), 1.0) }
    for(testSample in testData) {
        val lnProbs = ArrayList<Double>()
        for((struct , posteriorProb) in graphPosterior){
            lnProbs.add(Math.log(posteriorProb) + simpleLnQueryFull(testSample, bestParams[struct]!!))
        }
        val sampleError = -sumInnerLnProbs(lnProbs)
        totalError += sampleError
    }
    return totalError / testData.size
}
