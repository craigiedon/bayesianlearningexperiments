import json
import sys

fp = open(sys.argv[1])
bn = json.load(fp)
fp.close()

for rv, node in bn.items():
    if len(node["parents"]) == 0:
        parents = "$\\emptyset$"
    else:
        parents = ", ".join(str(x) for x in node["parents"])

    values = ", ".join("{:.2f}".format(float(x)) for x in node["cpt"])
    print("{0} & {1} & {2} \\\\".format(rv, parents, values))
