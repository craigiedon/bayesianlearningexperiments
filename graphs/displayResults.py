import sys
import os

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

"""
def combine_results(data_frame_paths):
    combined_df = reduce(lambda d1, d2: sum_two_dfs(d1, d2), data_frames )
    combined_df[""]



def sum_two_dfs(d1, d2, group_key):
    concat_df = pd.concat([d1, d2])
    return concat_df.groupby(group_key).sum()
"""

def combine_rewards(experiment_name):
    folder_names = [folder for folder in os.listdir("graphs") if folder.startswith(experiment_name) and "agentFinalDN.json" in [os.lisdir("graphs/" + folder)]]
    print(len(folder_names))
    total_data = None
    for folder_name in folder_names:
        rewards_data = pd.read_csv("graphs/" + folder_name + "/Rewards.txt", header=None, names=["ts", "reward"])
        if total_data is not None:
            total_data = pd.concat([total_data, rewards_data])
            total_data = total_data.groupby("reward").agg({"ts" : "min", "reward" : "sum"})
        else:
            total_data = rewards_data

    avg_data = total_data.assign(reward=lambda x: x.reward / len(folder_names))
    return avg_data

def combine_policy_errors(experiment_name):
    folder_names = [folder for folder in os.listdir("graphs") if folder.startswith(experiment_name) and "agentFinalDN.json" in [os.listdir("graphs/" + folder)]]
    print(len(folder_names))
    total_data = None
    for folder_name in folder_names:
        policy_data = pd.read_csv("graphs/" + folder_name + "/PolicyError.txt", header=None, names=["ts", "policy_error"]).apply(policy_error=lambda x : int(x.policy_error / 20) * 20)
        if total_data is not None:
            total_data = pd.concat([total_data, policy_data])
            total_data = total_data.groupby("reward").agg({"ts" : "min", "policy_error" : "sum"})
        else:
            total_data = policy_error

    avg_data = total_data.assign(policy_error=lambda x: x.policy_error / len(folder_names))
    return avg_data

sns.set_style("white")

policy_data = combine_policy_errors("defaultConfig")
policy_data.plot(x="ts", y="policy_error")
plt.show()

reward_data = combine_rewards("defaultConfig")
reward_data = reward_data.groupby(reward_data["ts"].apply(lambda x : int(x / 150)))
reward_data = reward_data.agg({"ts" : "min", "reward" : "mean"})
reward_data.plot(x="ts",y="reward")

plt.legend(fontsize = "medium")
plt.tight_layout()
plt.show()
