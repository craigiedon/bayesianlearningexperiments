import numpy as np
import pandas as pd
import Utils.random
from IPython.display import display

data = []
rvs = ["dysp", "xray", "bronc", "either", "lung", "tub", "smoke", "asia"]
for numFills in range(1, len(rvs) + 1):
    for i in range(3):
        result = [Utils.random.randint(0,1) for x in range(numFills)]
        result.extend(["?"] * (len(rvs) - numFills))
        labelled_result = dict(zip(rvs, result))
        data.append(labelled_result)

s = pd.DataFrame(data, columns = rvs)
s.to_html("exampleData.html")
