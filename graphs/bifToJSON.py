import sys
import re
import json

def bif_to_json(file_name):
    bif_file = open(file_name, 'r')
    bif_lines = bif_file.readlines()
    bif_file.close()

    var_info = {}
    i = 0
    while i < len(bif_lines):
        current_line = bif_lines[i]
        var_match = re.match(r"variable ([^\s]*)", current_line)
        probability_match = re.match(r"probability \( (.*?)(?: \| (.*?))? \)", current_line)
        if var_match:
            var_name = var_match.group(1)
            i += 1
            content_line = bif_lines[i]
            domain_size = int(re.search(r"\[ (\d) \]", content_line).group(1))
            var_info[var_name] = {"name": var_name, "domainSize" : domain_size, "parents": [], "cpt": []}
        elif probability_match:
            var_name = probability_match.group(1)
            if probability_match.group(2):
                parents = probability_match.group(2).split(", ")
                var_info[var_name]["parents"] = parents

            i += 1
            while not re.match(r"}", bif_lines[i]):
                cpt_line = bif_lines[i]
                cpt_vals = re.findall(r"\d+\.\d+", cpt_line)
                var_info[var_name]["cpt"].extend(cpt_vals)
                i +=1
        i += 1
    return json.dumps(var_info,sort_keys= True, indent=4, separators=(',', ': ') )


if len(sys.argv) != 3:
    print("Incorrect arguments. Try: python bifToJSON.py <source_bif_file> <destination_name>")
    sys.exit()

json_string = bif_to_json(sys.argv[1])

with open(sys.argv[2], 'w') as outfile:
    outfile.write(json_string)
