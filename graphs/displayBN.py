import json
from graphviz import Digraph
import sys


def extractFinalBNs(result_file_name):
    with open(result_file_name) as result_file:
        json_result = json.load(result_file)
        return json_result["finalBNStructs"]


def display_directed_graph(parent_map):
    dg = Digraph('G')
    for node, parents in parent_map.iteritems():
        if len(parents) == 0:
            dg.node(node)
            
        for parent in parents:
            if type(parent) is dict:
                dg.edge(parent["name"], node)
            else:
                dg.edge(parent, node)
    dg.view()


def raw_structure(bn):
    return {rv: node["parents"] for (rv, node) in bn.iteritems()}


def raw_structure_kt(bn):
    return {rv: [x["name"] for x in node["parents"]] for (rv, node) in bn["nodes"].iteritems()}


def display_bn(bn):
    display_directed_graph(raw_structure_kt(bn))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Please specify json file to load")
        sys.exit()
    with open(sys.argv[1], 'r') as json_file:
        bayes_net = json.load(json_file)
        display_bn(bayes_net)
