from graphviz import Digraph
import json
import itertools
import copy
from graphviz import Graph
from displayBN import display_directed_graph
import Utils.random
import time
import numpy as np
import sys
from collections import defaultdict, deque
from itertools import chain

def generate_dag(num_nodes, iterations):
    nodes = ["X" + str(i) for i in range(num_nodes)]
    parent_map = defaultdict(list)
    child_map = defaultdict(list)

    parent_map[nodes[0]] = []
    for i in range(1, num_nodes):
        add_edge(nodes[i-1], nodes[i], parent_map, child_map)

    for i in range(iterations):
        (node_A, node_B) = Utils.random.sample(nodes, 2)
        if node_A in parent_map[node_B]:
                remove_edge(node_A, node_B, parent_map, child_map)
                if not is_connected(nodes, nodes[0], parent_map, child_map):
                    add_edge(node_A, node_B, parent_map, child_map)
        else:
            if not path_exists(node_B, node_A, child_map):
                add_edge(node_A, node_B, parent_map, child_map)


    return parent_map


def generate_dag_advanced(num_nodes, iterations, ar_prob, a_or_r_prob, max_induced_width, max_parents, max_children):
    nodes = ["X" + str(i) for i in range(num_nodes)]
    parent_map = defaultdict(list)
    child_map = defaultdict(list)

    parent_map[nodes[0]] = []
    for i in range(1, num_nodes):
        add_edge(nodes[i-1], nodes[i], parent_map, child_map)

    for i in range(iterations):
        old_parent_map = copy.deepcopy(parent_map)
        old_child_map = copy.deepcopy(child_map)
        rejected = False
        random_val = Utils.random.Utils.random()
        if is_polytree(nodes, parent_map, child_map):
            if random_val < ar_prob:
                add_or_remove(parent_map, child_map, nodes)
            elif random_val < (ar_prob + a_or_r_prob):
                sequence_add_or_remove(parent_map, child_map, nodes)
            else:
                add_and_remove(parent_map, child_map, nodes)
        else:
            if random_val < (ar_prob + a_or_r_prob):
                add_or_remove(parent_map, child_map, nodes)
            else:
                sequence_add_or_remove(parent_map, child_map, nodes)

            if is_polytree(nodes, parent_map, child_map) and Utils.random.Utils.random() > 0.5:
                rejected = True

        if (has_cycles(nodes, child_map)
            or not is_connected(nodes, nodes[0], parent_map, child_map)
            or tree_width(parent_map) > max_induced_width
            or any([len(parent_map[x]) > max_parents for x in parent_map.keys()])
            or any([len(child_map[x]) > max_children for x in child_map.keys()])
        ):
            rejected = True

        if rejected:
            parent_map = old_parent_map
            child_map = old_child_map

    return parent_map


def add_or_remove(parent_map, child_map, vocabulary):
    (parent, child) = Utils.random.sample(vocabulary, 2)
    if parent in parent_map[child]:
        remove_edge(parent, child, parent_map, child_map)
    else:
        add_edge(parent, child, parent_map, child_map)


def add_and_remove(parent_map, child_map, vocabulary):
    (parent, child) = Utils.random.sample(vocabulary, 2)
    if parent not in parent_map[child]:
        node_path = path_from(parent, child)
        predecessor_node = node_path[-2]
        if predecessor_node in parent_map[child]:
            remove_edge(predecessor_node, child, parent_map, child_map)
        else:
            remove_edge(child, predecessor_node, parent_map, child_map)

        if Utils.random.Utils.random() > 0.5:
            (parent, child) = (child, parent)
        parent_map[child] = parent

def is_polytree(vocab, parent_map, child_map):
    return is_connected(vocab, vocab[0], parent_map, child_map) and not has_undirected_cycles(vocab, parent_map, child_map)


def has_undirected_cycles(vocab, parent_map, child_map):
    undirected_map = {}
    for rv in vocab:
        undirected_map[rv] = list(set(parent_map[rv] + child_map[rv]))
    return has_cycles(vocab, undirected_map)

def path_from(from_node, to_node, parent_map, child_map, visited):
    edges = set(parent_map[from_node] + child_map[from_node])
    if to_node in edges:
        return [from_node, to_node]
    else:
        paths = [path_from(n, to_node, parent_map, child_map, visited.union(edges)) for n in edges.difference(visited)]
        paths = [n for n in paths if n is not None]

        if len(paths) == 0:
            return None

        return [from_node] + paths[0]


def sequence_add_or_remove(parent_map, child_map, vocabulary):
    if is_polytree(vocabulary, parent_map, child_map):
        (parent, child) = Utils.random.sample(vocabulary, 2)
        if parent not in parent_map[child]:
            add_edge(parent, child, parent_map, child_map)
    if not is_polytree(vocabulary, parent_map, child_map):
        (parent, child) = Utils.random.sample(vocabulary, 2)
        if parent in parent_map[child]:
            remove_edge(parent, child, parent_map, child_map)


def has_cycles(vocab, child_map):
    return any([path_exists(rv, rv, child_map) for rv in vocab])

def is_connected(vocab, start, parent_map, child_map):
    visited = set([])
    search_queue = deque()
    search_queue.extend(child_map[start] + parent_map[start])
    while search_queue:
        current_node = search_queue.popleft()
        if current_node not in visited:
            visited.add(current_node)
            search_queue.extend(child_map[current_node] + parent_map[current_node])
    return set(vocab) == visited

def add_edge(parent, child, parent_map, child_map):
    parent_map[child].append(parent)
    child_map[parent].append(child)


def remove_edge(parent, child, parent_map, child_map):
    parent_map[child].remove(parent)
    child_map[parent].remove(child)

def path_exists(start, finish, child_map):
    visited = set([start])
    search_queue = deque()
    search_queue.extend(child_map[start])
    while search_queue:
        current_node = search_queue.popleft()
        if current_node == finish:
            return True
        if current_node not in visited:
            visited.add(current_node)
            search_queue.extend(child_map[current_node])
    return False

def generate_cpt(node, parents):
    false_probs = np.Utils.random.beta(0.95, 0.95, 2**len(parents))
    cpt_tuples = [[x, 1-x] for x in false_probs]
    return list(chain.from_iterable(cpt_tuples))


def create_bn(num_vars, max_induced_width, max_parents, max_children):
    dag = generate_dag_advanced(num_vars, 5000, 0.3, 0.3, max_induced_width, max_parents, max_children)
    display_directed_graph(dag)
    bayes_net = {}
    for node in dag.keys():
        bayes_net[node] = {"name": node, "domainSize": 2, "parents": dag[node], "cpt": generate_cpt(node, dag[node])}
    return bayes_net


def tree_width(parent_map):
    moralized_graph = moralize(parent_map)
    tri_graph, elim_ordering = triangulate(moralized_graph)
    return len(maximum_clique(tri_graph, elim_ordering)) - 1


def triangulate(graph):
    elimination_ordering = {}
    triangulated_graph = {}
    for node, neighbors in graph.iteritems():
        triangulated_graph[node] = set(neighbors)
    for i in range(len(graph)):
        min_length = sys.maxsize
        min_node = None
        for node in [n for n in graph if n not in elimination_ordering]:
            later_neighbors = [neighbor for neighbor in graph[node] if neighbor not in elimination_ordering]
            if len(later_neighbors) < min_length:
                min_length = len(later_neighbors)
                min_later_neighbors = later_neighbors
                min_node = node

        elimination_ordering[min_node] = i

        for (u, v) in itertools.combinations(min_later_neighbors, 2):
            triangulated_graph[u].add(v)
            triangulated_graph[v].add(u)

    return triangulated_graph, elimination_ordering


def moralize(parent_map):
    edges = []
    for node, parents in parent_map.iteritems():
        for i in range(len(parents)):
            edges.append((node, parents[i]))
            moralized_edges = [(parents[i], other_parent) for other_parent in parents[(i+1):]]
            edges.extend(moralized_edges)

    node_neighbors_map = defaultdict(set)
    for (u, v) in edges:
        node_neighbors_map[u].add(v)
        node_neighbors_map[v].add(u)
    return node_neighbors_map


def maximum_clique(triangulated_graph, elimination_ordering):
    max_length = 0
    max_clique = None
    for node in triangulated_graph:
        clique = [node] + [n for n in triangulated_graph[node] if elimination_ordering[n] > elimination_ordering[node]]
        if len(clique) > max_length:
            max_clique = clique
            max_length = len(clique)
    return max_clique


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Incorrect number of arguments. Try: python generateBN <fileName> <num_vars> <max_induced_width> <max_parents> <max_children>")
        sys.exit()

    num_vars = int(sys.argv[2])
    max_induced_width = int(sys.argv[3])
    max_parents = int(sys.argv[4])
    max_children = int(sys.argv[5])
    bn = create_bn(num_vars, max_induced_width, max_parents, max_children)

    with open(sys.argv[1] + '.json', 'w') as outfile:
        json.dump(bn, outfile, sort_keys=True, indent=4, separators=(',', ': '))
