import json
import sys

fp = open(sys.argv[1])
dn = json.load(fp)
fp.close()

for cpt in dn["cpts"]:
    rv = cpt["name"]
    if len(cpt["parents"]) == 0:
        parents = "$\\emptyset$"
    else:
        parents = ", ".join(str(x) for x in cpt["parents"])

    values = ", ".join("{:.2f}".format(x) for x in cpt["values"])
    print(rv + " & " + parents + " & " + values + " \\\\")

for utility in dn["utilities"]:
    scope = ", ".join(str(x) for x in utility["scope"])
    rewards = ", ".join("{:.2f}".format(x) for x in utility["rewards"])
    print(scope + " & " + rewards)
