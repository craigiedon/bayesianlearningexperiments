import json
from graphviz import Digraph
import sys


def display_dn(dn):
    parent_map = raw_structure(dn)
    type_map = {rv["name"]: rv["varType"] for rv in dn["rvs"]}
    display_directed_graph(parent_map, type_map, dn["utilities"])


def raw_structure(dn):
    return {cpt["name"]: cpt["parents"] for cpt in dn["cpts"]}


def display_directed_graph(parent_map, type_map, utilities):
    dg = Digraph('G')
    dg.attr("graph", margin="0", nodesep="0.1", ranksep="0.2")
    dg.attr("node", fontsize="12", margin="0.05", height="0.02")

    for node, n_type in type_map.iteritems():
        node_label = node.replace(' ', '\n')
        if n_type == "ACTION":
            dg.node(node, shape="rectangle", label=node_label)
        else:
            dg.node(node, shape="oval", label=node_label)

    dg.node("R", shape="diamond")

    for node, parents in parent_map.iteritems():
        for parent in parents:
            dg.edge(parent, node)

    for utility in utilities:
        for rv in utility["scope"]:
            dg.edge(rv, "R")

    dg.view()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage : python displayDN.py <json_file>")
        sys.exit()
    with open(sys.argv[1], 'r') as json_file:
        decision_net = json.load(json_file)
        display_dn(decision_net)
