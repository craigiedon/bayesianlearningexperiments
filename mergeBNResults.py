import mergeResults as mrg
import sys
from os.path import join
import os

if len(sys.argv) != 2:
    print("Usage: python mergeResults.py <results_folder>")
    sys.exit(1)

results_folder = sys.argv[1]
merged_folder = join(results_folder, os.pardir, "merged")

print("Merging experiment repetitions")
for metric in ["klDiv", "shd", "bdScore"]:
    mrg.merge_results(results_folder, metric)

print("Adding Action and Chance Size Results")
mrg.add_action_and_chance(merged_folder)
