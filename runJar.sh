#!/bin/sh
#$ -cwd
#$ -l h_rt=15:00:00
### #$ -pe sharedmem 4
### #$ -R y
#$ -l h_vmem=8G

# Load modules command?
. /etc/profile.d/modules.sh

# Use java module
module load java

# Run the program
# $1 - JAR name
# $2 - Task Config File Path
# $3 - Experiment Config File Path

java -Xmx4G -jar -Djava.library.path=$1/lib $1/lib/$1.jar $SGE_TASK_ID $2 $3
