import chartResults as ch
import sys

merged_results_folder = sys.argv[1]
time_cutoff = int(sys.argv[2])

test_groups = [["default", "nonCon", "nonRelevant", "truePolicy", "random"], ["default", "Tolerance"]]
comparisons = [("klDiv", "KL Divergence"), ("shd", "Structural Hamming Distance"), ("bdScore", "BayesianDirichletScore"), ("TimeSecs", "Time (secs)")]
for t_group in test_groups:
    for comparison, y_label in comparisons:
        comparison_format = ["{0}*{1}*{2}*".format(merged_results_folder, x, comparison) for x in t_group]
        ch.load_and_compare(comparison_format, "t", y_label, time_cutoff)
