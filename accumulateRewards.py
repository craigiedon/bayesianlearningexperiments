import chartResults as ch
import os
from os.path import join
import sys
import re


def accumulate_results(results_folder, discount_factor):
    for experiment_folder in os.listdir(results_folder):
        abs_exp_folder = join(results_folder, experiment_folder)
        experiment_files = os.listdir(abs_exp_folder)

        load_files = [join(abs_exp_folder, f) for f in experiment_files if re.match(r'.*-Rewards.txt', f)]

        for load_file in load_files:
            save_file = load_file.replace("Rewards", "discountedCumReward")
            ch.discounted_accumulate_and_save(load_file, save_file, discount_factor)


if __name__ == "__main__":
    # List dir from results folder
    if len(sys.argv) != 2:
        print("Usage: python mergeResults.py <results_folder>")
        sys.exit(1)
    accumulate_results(sys.argv[1], 0.99)
