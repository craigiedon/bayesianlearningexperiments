#!/bin/bash
set -euo pipefail

tar_path="build/distributions/BayesianLearningExperiments.tar"
echo $tar_path

tar_name=$(basename $tar_path)
echo $tar_name

raw_name="${tar_name%.*}"
echo $raw_name

eddie_loc="s0929508@eddie3.ecdf.ed.ac.uk"
config_folder="eddieConfigs"
task_config_folder="eddieTaskConfigs"
graph_folder="graphs"
start_graph_folder="graphStarts"

echo "Removing old files from eddie"
ssh $eddie_loc "rm -rf configs ; rm -rf taskConfigs ; rm -rf graphs ; rm -rf graphStarts rm -rf OutputLogs ; rm -rf logs ; rm submitAllConfigs.py ; rm runJar.sh ; ls"

echo "Copying tar and scripts"
scp -r $tar_path submitAllConfigs.py runJar.sh "$eddie_loc:./"

echo "Copying configs"
scp -r $config_folder "$eddie_loc:./configs"

echo "Copying task configs"
scp -r $task_config_folder "$eddie_loc:./taskConfigs"

echo "Copying graphs"
scp -r $graph_folder "$eddie_loc:./graphs"

echo "Copying graph starts"
scp -r $start_graph_folder "$eddie_loc:./graphStarts"

echo "Unzipping and Submitting Jobs"
ssh $eddie_loc "tar -xvf $tar_name ; rm $tar_name ; python submitAllConfigs.py $raw_name 50"
