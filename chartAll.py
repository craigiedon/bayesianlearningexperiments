import chartResults as ch
import sys

merged_results_folder = sys.argv[1]
time_cutoff = int(sys.argv[2])

test_groups = [["default", "truePolicy", "random"], ["default", "nonCon", "nonRelevant"], ["default", "Tolerance"]]
#test_groups = [["default", "noPruning", "uniformPrior"]]
metrics = [
    ("Reward", "Cumulative Reward"),
    ("ChanceVarsSize", "Num Belief Variables"),
    ("ActionsSize", "Num Actions"),
    ("VocabSize", "Vocab Size"),
    ("TimeSecs", "Total Time (Secs)")
]

for t_group in test_groups:
    for (metric, y_label) in metrics:
        comparison_format = ["{0}*{1}*{2}*".format(merged_results_folder, x, metric) for x in t_group]
        ch.load_and_compare(comparison_format, "t", y_label, time_cutoff)

for t_group in test_groups:
    reward_comparison = ["{0}*{1}*TimeSecs*".format(merged_results_folder, x) for x in t_group]
    ch.load_discounted_rewards_and_compare(reward_comparison, "t", "Total Time (Secs)", time_cutoff, 1.0)
