import matplotlib.pyplot as plt
import json
import sys
import numpy as np
from collections import defaultdict

def active_learn_comparison(file_name):
    results_file = open(file_name, 'r')
    results = json.load(results_file)
    results_file.close()

    num_samples_results = results["NumSamples"]
    num_advice_results = results["adviceAmount"]

    l1_averages = []
    prediction_averages = []
    for num_samples, trials in num_advice_results.items():
        l1_averages.append(average_dicts([trial["L1"] for trial in trials]))
        prediction_averages.append(average_dicts([trial["Prediction"] for trial in trials]))

    models = l1_averages[0].keys()
    print(models)

    for model in models:
        model_l1s = [l1_avg[model] for l1_avg in l1_averages]
        #model_prediction_avgs = [pred_avg[model] for pred_avg in prediction_averages]
        plt.plot(model_l1s, label=model)

    plt.legend(loc="best")
    plt.show()

def average_dicts(dicts):
    total_dict = defaultdict(list)
    for d in dicts:
        for k, v in d.items():
            total_dict[k].append(v)

    return {k : sum(v) / len(v) for k,v in total_dict.items()}

def display_timings(file_name):
    results_file = open(file_name, 'r')
    results_file = open(file_name, 'r')
    results = json.load(results_file)
    results_file.close()

    timing_results = results["timings"]
    points_to_fill = 10000
    for experiment_key in timing_results:
        plt.plot(np.linspace(0,points_to_fill, len(timing_results[experiment_key])), [t / 1000.0 for t in timing_results[experiment_key]], label=experiment_key)
    plt.legend(loc="best")
    plt.ylabel("Time (seconds)")
    plt.xlabel("Num Data Points")
    plt.tight_layout()
    plt.show()

def display_timing_total(file_name):
    results_file = open(file_name, 'r')
    results_file = open(file_name, 'r')
    results = json.load(results_file)
    results_file.close()

    timing_results = results["timings"]
    timing_totals = sorted([(exp_name, sum(timings) / 1000.0) for (exp_name, timings) in timing_results.iteritems()], key=lambda t:t[1])
    xs = np.arange(len(timing_totals))
    plt.bar(xs, [timings for (exp_name, timings) in timing_totals], align='center')
    plt.xticks(xs, [exp_name for (exp_name, timings) in timing_totals])
    plt.show()

def display_timing_final(file_name):
    results_file = open(file_name, 'r')
    results_file = open(file_name, 'r')
    results = json.load(results_file)
    results_file.close()

    timing_results = results["timings"]
    final_timings = []
    for (exp_name, timings) in timing_results.iteritems():
        if "stepwise" in exp_name:
            final_timings.append((exp_name, sum(timings) / 1000.0))
        else:
            final_timings.append((exp_name, timings[-1] / 1000.0))

    final_timings.sort(key=lambda t:t[1])
    xs = np.arange(len(final_timings))
    plt.bar(xs, [timings for (exp_name, timings) in final_timings], align='center')
    plt.xticks(xs, [exp_name for (exp_name, timings) in final_timings])
    plt.show()


def display_SHDS(file_name):
    results_file = open(file_name, 'r')
    results = json.load(results_file)
    results_file.close()

    shd_results = results["shds"]
    points_to_fill = 10000
    for experiment_key in shd_results:
        plt.plot(np.linspace(0,points_to_fill, len(shd_results[experiment_key])), shd_results[experiment_key], label=experiment_key)
    plt.legend()
    plt.show()

def display_results(file_name, string_filters = [], log_scale = True):
    results_file = open(file_name, 'r')
    results = json.load(results_file)
    results_file.close()

    cm = plt.get_cmap('gist_rainbow')
    fig = plt.figure()

    kl_results = {k : v for (k,v) in results["kls"].iteritems() if any(sf in k for sf in string_filters) or not string_filters}
    if("fallbackRevisions" in results):
        fallback_revisions = {k : v for (k,v) in results["fallbackRevisions"].iteritems() if any(sf in k for sf in string_filters) or not string_filters}
    else:
        fallback_revisions = {}

    ax = fig.add_subplot(111)
    #num_colors = len(kl_results)
    #ax.set_color_cycle([cm(1.*i/num_colors) for i in range(num_colors)])

    points_to_fill = 10000

    print(fallback_revisions)
    for experiment_key in kl_results:
        if log_scale:
            plt.yscale('log')
        plt.plot(np.linspace(0,points_to_fill, len(kl_results[experiment_key])), kl_results[experiment_key], label=experiment_key)
        if(experiment_key in fallback_revisions):
            fallback_times = np.array(fallback_revisions[experiment_key])
            plt.plot(100 * fallback_times, np.array(kl_results[experiment_key])[fallback_times], "bx")
        plt.legend()

    plt.ylabel("KL Divergence")
    plt.xlabel("Num Data Points")
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    file_name = sys.argv[1]
    string_filters = sys.argv[2:]
    display_results(file_name, string_filters)
