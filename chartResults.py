# Import matplotlib etc.
import numpy as np
from itertools import cycle
import matplotlib.pyplot as plt
import pandas as pd
import sys
from glob import glob
import functools as fn
import seaborn as sns
from os.path import basename, splitext


def plot_reward_results(df):
    plt.plot(df.index.get_values(), df["discounted_cum_reward"])
    plt.show()


def compare_plots(dfs):
    for df in dfs:
        plt.plot(df.index.get_values(), df["discounted_cum_reward"])
    plt.show()


def load_rewards_and_accumulate(file_path, discount):
    df = pd.read_csv(file_path, names=["time_stamp", "value", "standard_error"])
    df["value"] = discounted_accumulate(df["value"], discount)
    return df


def load_time_stamped_val(file_path):
    df = pd.read_csv(file_path, names=["time_stamp", "value", "standard_error"])
    return df


def load_accumulated_rewards(file_path):
    return pd.read_csv(file_path, names=["time_stamp", "reward", "discounted_cum_reward"])


def add_results(wild_paths):
    file_paths = [file_name for wild_path in wild_paths for file_name in glob(wild_path)]
    dfs = [pd.read_csv(fp, names=["time_stamp", "value"]) for fp in file_paths]
    plus_indices = ["value"]
    combined_df = fn.reduce(lambda df1, df2: df1[plus_indices] + df2[plus_indices], dfs)
    return combined_df


def combine_results(wild_paths):
    file_paths = [file_name for wild_path in wild_paths for file_name in glob(wild_path)]
    dfs = [pd.read_csv(fp, names=["time_stamp", "value"]) for fp in file_paths]

    combined_df = pd.concat(dfs).groupby("time_stamp")

    avg_df = combined_df.mean()
    avg_df["sem"] = combined_df.sem()["value"]

    return avg_df


def merge_and_save(load_file_paths, save_file_path, combine_func):
    combined_df = combine_func(load_file_paths)
    combined_df.to_csv(save_file_path, header=False)


def scale_and_save(load_file_path, save_file_path, scale_factor):
    df = pd.read_csv(load_file_path, names=["time_stamp", "value"])
    df["value"] = df["value"] * scale_factor
    df.to_csv(save_file_path, header=False)


def load_discounted_rewards_and_compare(wild_paths, x_label, y_label, time_cutoff=None, discount=1.0):
    file_paths = [file_name for wild_path in wild_paths for file_name in glob(wild_path)]
    dfs_with_names = [(splitext(basename(file_path))[0].split("-")[-2],
                       load_rewards_and_accumulate(file_path, discount))
                      for file_path in file_paths]

    compare_results(dfs_with_names, x_label, y_label, time_cutoff)


def load_and_compare(wild_paths, x_label, y_label, time_cutoff=None):
    file_paths = [file_name for wild_path in wild_paths for file_name in glob(wild_path)]
    dfs_with_names = [(splitext(basename(file_path))[0].split("-")[-2],
                       load_time_stamped_val(file_path))
                      for file_path in file_paths]

    compare_results(dfs_with_names, x_label, y_label, time_cutoff)


def compare_results(dfs_with_names, x_label, y_label, time_cutoff=None):
    font_size = 16
    plt.rc('legend', fontsize=font_size)
    plt.rc('axes', titlesize=font_size)
    plt.rc('axes', labelsize=font_size)
    # plt.rcParams["figure.figsize"] = (4.0 * 0.9, 3.0 * 0.9)

    for df_name, df in dfs_with_names:
        df["Agent"] = df_name

    dfs = [df for (df_name, df) in dfs_with_names]

    if time_cutoff is not None:
        dfs = [df[df.time_stamp <= time_cutoff] for df in dfs]

    marker = cycle(('d', '^', '.', 'o', '*'))
    everyN = 1
    for df in dfs:
        print(df["Agent"])
        plt.plot(df["time_stamp"][0::everyN], df["value"][0::everyN],
                 # marker=marker.next(), markevery=10,
                 linewidth=1.0, label=df["Agent"][0])
        if "standard_error" in df.columns:
            plt.fill_between(df["time_stamp"][0::everyN], df["value"][0::everyN] + df["standard_error"][0::everyN], df["value"][0::everyN] - df["standard_error"][0::everyN], alpha=0.15)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.legend(loc="lower right")
    plt.tight_layout()
    plt.show()


def compare_vocab_size(wild_paths, time_cutoff=None):
    compare_results(wild_paths, "t", r'$|X^t|$', time_cutoff)


def compare_actions_size(wild_paths, time_cutoff=None):
    compare_results(wild_paths, "t", r'$|A^t|$', time_cutoff)


def discounted_accumulate(nums, discount):
    prev_sum = 0
    discounted_cum_sums = []
    for num in nums:
        new_sum = prev_sum * discount + num
        discounted_cum_sums.append(new_sum)
        prev_sum = new_sum
    return discounted_cum_sums


def discounted_accumulate_and_save(load_file_path, save_file_path, discount_factor):
    df = pd.read_csv(load_file_path, names=["time_stamp", "value"])
    df["value"] = discounted_accumulate(df["value"], discount_factor)
    df.to_csv(save_file_path, header=False)


if __name__ == "__main__":
    print("Undefined lone script behaviour")
